module controller(
clk, 
reset,
writedone, 
mux0sel,
muxdemux1demux0sel,
en_ark,
en_msb,
en_sr,
en_mc,
en_shiftregout,
sel_shiftregout,
keysc_sel,
keysc_en,
man_reset
);

input clk, reset, writedone;
output reg 
mux0sel = 0, 
muxdemux1demux0sel = 0,  
en_ark = 0, 
en_msb = 0, 
en_sr = 0, 
en_mc = 0, 
en_shiftregout = 0,
man_reset = 0,
keysc_en = 0;
output reg [3:0] keysc_sel = 0, sel_shiftregout = 0;
reg [31:0] i;
reg [4:0] j=0;

always @(posedge clk)
begin
	if(reset) i = 0;
	else if(writedone) i = i+1;
end

always @(i)
begin
	if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==40)) begin
		en_ark = 1;
		keysc_en = 1;
	end
	else begin
		en_ark = 0;
		keysc_en = 0;
	end
	if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)) begin
		en_msb <= 1;
	end
	else begin
		en_msb <= 0;
	end
	if((i==3)||(i==7)||(i==11)||(i==15)||(i==19)||(i==23)||(i==27)||(i==31)||(i==35)||(i==39)) begin
		if(i==39) keysc_sel <= keysc_sel+1;
		en_sr <= 1;
	end
	else begin
		en_sr <= 0;
	end
	if((i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)) begin
		en_mc <= 1;
		keysc_sel <= keysc_sel+1;
	end
	else begin
		en_mc <= 0;
	end
	if(i>40) begin
		en_shiftregout <= 1;
		sel_shiftregout = j;
		j = j+1;	
	end
	else begin
		en_shiftregout <= 0;
	end
	if(i>3) mux0sel <= 1;
	else mux0sel <= 0;
	if(i>38) muxdemux1demux0sel <= 1;
	else muxdemux1demux0sel <= 0;
//	if(i==56) begin 
//		man_reset <= 1;
//		j = 0;
//	end
//	else man_reset <= 0;
end
endmodule
