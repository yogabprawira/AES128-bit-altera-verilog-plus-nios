module shift_register_in (in, inkey, out, outkey, write, clk);

input [31:0] in, inkey;
output reg [127:0] out, outkey;
input clk, write;
reg [127:0] outreg, outkeyreg;

always @(posedge clk)
begin
	outreg <= outreg>>32;
	outkeyreg <= outkeyreg>>32;
	outreg[127:96] <= in;
	outkeyreg[127:96] <= inkey;
	out <= outreg;
	outkey <= outkeyreg;
end
endmodule
