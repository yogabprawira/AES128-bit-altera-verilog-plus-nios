module demultiplexer (in, out0, out1, sel, clk);

input [127:0] in;
output wire [127:0] out0, out1;
reg [127:0] out0_reg, out1_reg;
input sel, clk;

assign out0 = (sel==0)? out0_reg:128'dz;
assign out1 = (sel==1)? out1_reg:128'dz;

always @(clk)
begin
	out0_reg <= in;
	out1_reg <= in;
end
endmodule
