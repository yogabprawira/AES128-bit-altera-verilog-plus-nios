module mixcolumn(in, out, enable, clk);

input [127:0] in;
output wire [127:0] out;
input clk, enable;
wire [127:0] temp;

assign temp = (enable==1)? in : 128'dz;

mixcolumnperword mixc3(temp[127:96], out[127:96], clk);
mixcolumnperword mixc2(temp[95:64], out[95:64], clk);
mixcolumnperword mixc1(temp[63:32], out[63:32], clk);
mixcolumnperword mixc0(temp[31:0], out[31:0], clk);

endmodule
