module keyschedule (
inkey,
outkey,
sel,
enable,
clk
);

input [127:0] inkey;
output reg [127:0] outkey;
input [3:0] sel;
input clk, enable;
wire [127:0] outmux0, outkeyex;
reg mux0sel;

multiplexer mux0(inkey, outkey, outmux0, mux0sel, clk);
keyexpansion keyex0(outmux0, outkeyex, sel, clk); 

always @(posedge clk)
begin
	if(enable) outkey <= outkeyex;
	if(sel==0) mux0sel <= 0;
	else mux0sel <= 1;
end
endmodule
