module shift_reg_in2 (in, inkey, out, outkey, write, writedone, reset, clk);

input [7:0] in, inkey;
output reg [127:0] out, outkey;
input clk, write, reset;
output reg writedone;
reg [4:0] i;

always @(posedge clk)
begin
	if(reset) i <= 0;
	else begin
		if(write) begin
			out <= out>>8;
			outkey <= out>>8;
			out[127:120] <= in;
			outkey[127:120] <= inkey;
			i <= i+1;
		end
	end
end

always @(i)
begin
	if(i==16) writedone <= 1;
	else writedone <= 0;
end
endmodule
