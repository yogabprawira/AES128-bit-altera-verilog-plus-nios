/*
 * Program C untuk Prosesor NIOS II
 * Algoritma Advanced Encryption Standard (AES)
 * oleh Yoga Budhi Prawira
 * Universitas Gadjah Mada
*/

// Header Standard IO
#include <stdio.h>
#include <unistd.h>

// Header Pin NIOS II
#include "altera_avalon_pio_regs.h"
#include "system.h"

// Header Timestamp (Pengukur waktu eksekusi)
#include "sys/alt_timestamp.h"
#include "alt_types.h"

unsigned int i, j, k, n, counter;
unsigned char h;
unsigned char temp, total;
unsigned char a[4][4], cipherKeyUpdated[4][4], cipherText[4][4],
     	 	  rotWord[4], sementara[4][4], cipherKeySave[11][4][4],
     	 	  stateDecrypted[4][4];

unsigned char state[4][4] =
{ 0x32, 0x88, 0x31, 0xe0,
  0x43, 0x5a, 0x31, 0x37,
  0xf6, 0x30, 0x98, 0x07,
  0xa8, 0x8d, 0xa2, 0x34
};

unsigned char cipherKey[4][4] =
{ 0x2b, 0x28, 0xab, 0x09,
  0x7e, 0xae, 0xf7, 0xcf,
  0x15, 0xd2, 0x15, 0x4f,
  0x16, 0xa6, 0x88, 0x3c
};

unsigned char sBox[16][16] =
{ 0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, // 0
  0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, // 1
  0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, // 2
  0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, // 3
  0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, // 4
  0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, // 5
  0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, // 6
  0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, // 7
  0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, // 8
  0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, // 9
  0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, // a
  0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, // b
  0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, // c
  0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, // d
  0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, // e
  0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16  // f
};

unsigned char rCon[10] =
{ 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36 };

/*************************************************************************
 * Fungsi perkalian GF(2^8) **************************************************************************
 */
unsigned char mult2(unsigned char num) {
  h = (unsigned char)((signed char) num >> 7);
  num = num << 1;
  num ^= 0x1b & h;
  return num;
}
unsigned char mult3(unsigned char num) {
  num ^= mult2(num);
  return num;
}

/*************************************************************************
 * Fungsi pengubah key
**************************************************************************
 */
void keySchedule(void) {
  for (i = 0; i < 4; i++) {
    rotWord[i] = cipherKeyUpdated[(i + 1) % 4][3];
    rotWord[i] = sBox[rotWord[i] / 0x10][rotWord[i] % 0x10];
  }
  cipherKeyUpdated[0][0] ^= rotWord[0] ^ rCon[counter];
  counter++;
  for (i = 1; i < 4; i++) {
    cipherKeyUpdated[i][0] ^= rotWord[i];
  }
  for (j = 1; j < 4; j++) {
    for (i = 0; i < 4; i++) {
      cipherKeyUpdated[i][j] ^= cipherKeyUpdated[i][j - 1];
    }
  }
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      cipherKeySave[counter][i][j] = cipherKeyUpdated[i][j];
    }
  }
}

/*************************************************************************
 * Blok untuk Enkripsi
**************************************************************************
 */
void addRoundKey(void) {
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      sementara[i][j] ^= cipherKeyUpdated[i][j];
    }
  }
}

void subBytes(void) {
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      sementara[i][j] = sBox[sementara[i][j] / 0x10][sementara[i][j] % 0x10];
    }
  }
}

void shiftRows(void) {
  for (i = 1; i < 4; i++) {
    for (k = 0; k < i; k++) {
      temp = sementara[i][0];
      for (j = 0; j < 3; j++) {
        sementara[i][j] = sementara[i][(j + 1) % 4];
      }
      sementara[i][3] = temp;
    }
  }
}

void mixColumns(void) {
  /* Referensi: http://en.wikipedia.org/wiki/Rijndael_mix_columns
   * r = sementara
   */
  for (j = 0; j < 4; j++) {
    for (i = 0; i < 4; i++) {
      a[i][j] = sementara[i][j];
    }
  }
  for (j = 0; j < 4; j++) {
    for (i = 0; i < 4; i++) {
      sementara[i][j] = mult3(a[(1 + i) % 4][j]) ^ mult2(a[i][j]) ^ a[(2 + i) % 4][j] ^ a[(3 + i) % 4][j];
    }
  }
}

/*************************************************************************
 * Fungsi Enkripsi dengan Nios II
**************************************************************************
 */
void encryptAES128(void) {
  counter = 0;

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      sementara[i][j] = state[i][j];
    }
  }

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      cipherKeyUpdated[i][j] = cipherKey[i][j];
      cipherKeySave[0][i][j] = cipherKey[i][j];
    }
  }

  printf("\n");
  printf("State : ");
  printf("\n");
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      printf("%X ",sementara[i][j]);
    }
    printf("\n");
  }

  printf("\n");
  printf("Cipher Key : ");
  printf("\n");
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      printf("%X ",cipherKeyUpdated[i][j]);
    }
    printf("\n");
  }

  if (alt_timestamp_start()< 0)
  	  printf("\n\nNo timestamp device is available.\n\n");
  alt_u32 waktustart = alt_timestamp();

  addRoundKey();

  for (n = 1; n < 11; n++) {
    subBytes();
    shiftRows();
    if (n < 10) mixColumns();
    keySchedule();
    addRoundKey();
  }

  alt_u32 waktustop = alt_timestamp();

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      cipherText[i][j] = sementara[i][j];
    }
  }

  printf("\n");
  printf("Cipher Text :");
  printf("\n");
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      printf("%X ",cipherText[i][j]);
    }
    printf("\n");
  }

  printf("\nWaktu Eksekusi (clock): ");
  printf("\n%d", (waktustop - waktustart));
}

/*************************************************************************
 * Fungsi Enkripsi Menggunakan Modul AES
**************************************************************************
 */
void encryptAES128withEngine(void)
{
	alt_u32 waktustart, waktustop;
	IOWR_ALTERA_AVALON_PIO_DATA(WRITE_BASE, 0);

	unsigned int state_in0 = 0xa8f64332;
	unsigned int state_in1 = 0x8d305a88;
	unsigned int state_in2 = 0xa2983131;
	unsigned int state_in3 = 0x340737e0;

	printf("\nState: ");
	printf("\n%X", state_in0);
	printf("\n%X", state_in1);
	printf("\n%X", state_in2);
	printf("\n%X\n", state_in3);

	unsigned int key0 = 0x16157e2b;
	unsigned int key1 = 0xa6d2ae28;
	unsigned int key2 = 0x8815f7ab;
	unsigned int key3 = 0x3c4fcf09;

	printf("\nCipher Key: ");
	printf("\n%X", key0);
	printf("\n%X", key1);
	printf("\n%X", key2);
	printf("\n%X\n", key3);

	IOWR_ALTERA_AVALON_PIO_DATA(INPUT0_AES_BASE, state_in0);
	IOWR_ALTERA_AVALON_PIO_DATA(INPUT1_AES_BASE, state_in1);
	IOWR_ALTERA_AVALON_PIO_DATA(INPUT2_AES_BASE, state_in2);
	IOWR_ALTERA_AVALON_PIO_DATA(INPUT3_AES_BASE, state_in3);

	IOWR_ALTERA_AVALON_PIO_DATA(KEY0_AES_BASE, key0);
	IOWR_ALTERA_AVALON_PIO_DATA(KEY1_AES_BASE, key1);
	IOWR_ALTERA_AVALON_PIO_DATA(KEY2_AES_BASE, key2);
	IOWR_ALTERA_AVALON_PIO_DATA(KEY3_AES_BASE, key3);

	if (alt_timestamp_start()< 0)
		printf("\n\nNo timestamp device is available.\n\n");
	waktustart = alt_timestamp();

	IOWR_ALTERA_AVALON_PIO_DATA(WRITE_BASE, 1);
	while(!IORD_ALTERA_AVALON_PIO_DATA(DONE_BASE));

	waktustop = alt_timestamp();

	IOWR_ALTERA_AVALON_PIO_DATA(WRITE_BASE, 0);

	unsigned int out0 = IORD_ALTERA_AVALON_PIO_DATA(OUTPUT0_AES_BASE);
	unsigned int out1 = IORD_ALTERA_AVALON_PIO_DATA(OUTPUT1_AES_BASE);
	unsigned int out2 = IORD_ALTERA_AVALON_PIO_DATA(OUTPUT2_AES_BASE);
	unsigned int out3 = IORD_ALTERA_AVALON_PIO_DATA(OUTPUT3_AES_BASE);
	printf("\nCipher Text: ");
	printf("\n%X", out0);
	printf("\n%X", out1);
	printf("\n%X", out2);
	printf("\n%X\n", out3);

	printf("\nWaktu Eksekusi (clock): ");
	printf("\n%d", (waktustop - waktustart));
}

/*************************************************************************
 * Fungsi Utama
 **************************************************************************
 */
int main()
{ 
  //Jalankan salah satu fungsi di bawah ini
  encryptAES128(); // Enkripsi menggunakan Nios II
  //encryptAES128withEngine(); // Enkripsi menggunakan Modul AES
  return 0;
}
