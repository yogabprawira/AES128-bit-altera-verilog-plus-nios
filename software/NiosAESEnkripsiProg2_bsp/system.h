/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'niosProcessorAES' in SOPC Builder design 'niosForAES'
 * SOPC Builder design path: F:/Dropbox/Dropbox/Skripsi/NIOSAESALTERAENKRIPSI/niosForAES.sopcinfo
 *
 * Generated: Wed Dec 16 23:44:56 ICT 2015
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_qsys"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x0000c820
#define ALT_CPU_CPU_FREQ 50000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0x10
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00004020
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 50000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x10
#define ALT_CPU_NAME "niosProcessorAES"
#define ALT_CPU_RESET_ADDR 0x00004000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x0000c820
#define NIOS2_CPU_FREQ 50000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0x10
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00004020
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x10
#define NIOS2_RESET_ADDR 0x00004000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_TIMER
#define __ALTERA_NIOS2_QSYS


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Cyclone IV E"
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag_uart"
#define ALT_STDERR_BASE 0xd100
#define ALT_STDERR_DEV jtag_uart
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart"
#define ALT_STDIN_BASE 0xd100
#define ALT_STDIN_DEV jtag_uart
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart"
#define ALT_STDOUT_BASE 0xd100
#define ALT_STDOUT_DEV jtag_uart
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "niosForAES"


/*
 * done configuration
 *
 */

#define ALT_MODULE_CLASS_done altera_avalon_pio
#define DONE_BASE 0xd040
#define DONE_BIT_CLEARING_EDGE_REGISTER 0
#define DONE_BIT_MODIFYING_OUTPUT_REGISTER 0
#define DONE_CAPTURE 0
#define DONE_DATA_WIDTH 1
#define DONE_DO_TEST_BENCH_WIRING 0
#define DONE_DRIVEN_SIM_VALUE 0
#define DONE_EDGE_TYPE "NONE"
#define DONE_FREQ 50000000
#define DONE_HAS_IN 1
#define DONE_HAS_OUT 0
#define DONE_HAS_TRI 0
#define DONE_IRQ -1
#define DONE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DONE_IRQ_TYPE "NONE"
#define DONE_NAME "/dev/done"
#define DONE_RESET_VALUE 0
#define DONE_SPAN 16
#define DONE_TYPE "altera_avalon_pio"


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 4
#define ALT_SYS_CLK none
#define ALT_TIMESTAMP_CLK TIMER_AES


/*
 * input0_AES configuration
 *
 */

#define ALT_MODULE_CLASS_input0_AES altera_avalon_pio
#define INPUT0_AES_BASE 0xd030
#define INPUT0_AES_BIT_CLEARING_EDGE_REGISTER 0
#define INPUT0_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define INPUT0_AES_CAPTURE 0
#define INPUT0_AES_DATA_WIDTH 32
#define INPUT0_AES_DO_TEST_BENCH_WIRING 0
#define INPUT0_AES_DRIVEN_SIM_VALUE 0
#define INPUT0_AES_EDGE_TYPE "NONE"
#define INPUT0_AES_FREQ 50000000
#define INPUT0_AES_HAS_IN 0
#define INPUT0_AES_HAS_OUT 1
#define INPUT0_AES_HAS_TRI 0
#define INPUT0_AES_IRQ -1
#define INPUT0_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define INPUT0_AES_IRQ_TYPE "NONE"
#define INPUT0_AES_NAME "/dev/input0_AES"
#define INPUT0_AES_RESET_VALUE 0
#define INPUT0_AES_SPAN 16
#define INPUT0_AES_TYPE "altera_avalon_pio"


/*
 * input1_AES configuration
 *
 */

#define ALT_MODULE_CLASS_input1_AES altera_avalon_pio
#define INPUT1_AES_BASE 0xd0d0
#define INPUT1_AES_BIT_CLEARING_EDGE_REGISTER 0
#define INPUT1_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define INPUT1_AES_CAPTURE 0
#define INPUT1_AES_DATA_WIDTH 32
#define INPUT1_AES_DO_TEST_BENCH_WIRING 0
#define INPUT1_AES_DRIVEN_SIM_VALUE 0
#define INPUT1_AES_EDGE_TYPE "NONE"
#define INPUT1_AES_FREQ 50000000
#define INPUT1_AES_HAS_IN 0
#define INPUT1_AES_HAS_OUT 1
#define INPUT1_AES_HAS_TRI 0
#define INPUT1_AES_IRQ -1
#define INPUT1_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define INPUT1_AES_IRQ_TYPE "NONE"
#define INPUT1_AES_NAME "/dev/input1_AES"
#define INPUT1_AES_RESET_VALUE 0
#define INPUT1_AES_SPAN 16
#define INPUT1_AES_TYPE "altera_avalon_pio"


/*
 * input2_AES configuration
 *
 */

#define ALT_MODULE_CLASS_input2_AES altera_avalon_pio
#define INPUT2_AES_BASE 0xd0e0
#define INPUT2_AES_BIT_CLEARING_EDGE_REGISTER 0
#define INPUT2_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define INPUT2_AES_CAPTURE 0
#define INPUT2_AES_DATA_WIDTH 32
#define INPUT2_AES_DO_TEST_BENCH_WIRING 0
#define INPUT2_AES_DRIVEN_SIM_VALUE 0
#define INPUT2_AES_EDGE_TYPE "NONE"
#define INPUT2_AES_FREQ 50000000
#define INPUT2_AES_HAS_IN 0
#define INPUT2_AES_HAS_OUT 1
#define INPUT2_AES_HAS_TRI 0
#define INPUT2_AES_IRQ -1
#define INPUT2_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define INPUT2_AES_IRQ_TYPE "NONE"
#define INPUT2_AES_NAME "/dev/input2_AES"
#define INPUT2_AES_RESET_VALUE 0
#define INPUT2_AES_SPAN 16
#define INPUT2_AES_TYPE "altera_avalon_pio"


/*
 * input3_AES configuration
 *
 */

#define ALT_MODULE_CLASS_input3_AES altera_avalon_pio
#define INPUT3_AES_BASE 0xd0f0
#define INPUT3_AES_BIT_CLEARING_EDGE_REGISTER 0
#define INPUT3_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define INPUT3_AES_CAPTURE 0
#define INPUT3_AES_DATA_WIDTH 32
#define INPUT3_AES_DO_TEST_BENCH_WIRING 0
#define INPUT3_AES_DRIVEN_SIM_VALUE 0
#define INPUT3_AES_EDGE_TYPE "NONE"
#define INPUT3_AES_FREQ 50000000
#define INPUT3_AES_HAS_IN 0
#define INPUT3_AES_HAS_OUT 1
#define INPUT3_AES_HAS_TRI 0
#define INPUT3_AES_IRQ -1
#define INPUT3_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define INPUT3_AES_IRQ_TYPE "NONE"
#define INPUT3_AES_NAME "/dev/input3_AES"
#define INPUT3_AES_RESET_VALUE 0
#define INPUT3_AES_SPAN 16
#define INPUT3_AES_TYPE "altera_avalon_pio"


/*
 * jtag_uart configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart altera_avalon_jtag_uart
#define JTAG_UART_BASE 0xd100
#define JTAG_UART_IRQ -1
#define JTAG_UART_IRQ_INTERRUPT_CONTROLLER_ID -1
#define JTAG_UART_NAME "/dev/jtag_uart"
#define JTAG_UART_READ_DEPTH 64
#define JTAG_UART_READ_THRESHOLD 8
#define JTAG_UART_SPAN 8
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_WRITE_DEPTH 64
#define JTAG_UART_WRITE_THRESHOLD 8


/*
 * key0_AES configuration
 *
 */

#define ALT_MODULE_CLASS_key0_AES altera_avalon_pio
#define KEY0_AES_BASE 0xd090
#define KEY0_AES_BIT_CLEARING_EDGE_REGISTER 0
#define KEY0_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define KEY0_AES_CAPTURE 0
#define KEY0_AES_DATA_WIDTH 32
#define KEY0_AES_DO_TEST_BENCH_WIRING 0
#define KEY0_AES_DRIVEN_SIM_VALUE 0
#define KEY0_AES_EDGE_TYPE "NONE"
#define KEY0_AES_FREQ 50000000
#define KEY0_AES_HAS_IN 0
#define KEY0_AES_HAS_OUT 1
#define KEY0_AES_HAS_TRI 0
#define KEY0_AES_IRQ -1
#define KEY0_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define KEY0_AES_IRQ_TYPE "NONE"
#define KEY0_AES_NAME "/dev/key0_AES"
#define KEY0_AES_RESET_VALUE 0
#define KEY0_AES_SPAN 16
#define KEY0_AES_TYPE "altera_avalon_pio"


/*
 * key1_AES configuration
 *
 */

#define ALT_MODULE_CLASS_key1_AES altera_avalon_pio
#define KEY1_AES_BASE 0xd0a0
#define KEY1_AES_BIT_CLEARING_EDGE_REGISTER 0
#define KEY1_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define KEY1_AES_CAPTURE 0
#define KEY1_AES_DATA_WIDTH 32
#define KEY1_AES_DO_TEST_BENCH_WIRING 0
#define KEY1_AES_DRIVEN_SIM_VALUE 0
#define KEY1_AES_EDGE_TYPE "NONE"
#define KEY1_AES_FREQ 50000000
#define KEY1_AES_HAS_IN 0
#define KEY1_AES_HAS_OUT 1
#define KEY1_AES_HAS_TRI 0
#define KEY1_AES_IRQ -1
#define KEY1_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define KEY1_AES_IRQ_TYPE "NONE"
#define KEY1_AES_NAME "/dev/key1_AES"
#define KEY1_AES_RESET_VALUE 0
#define KEY1_AES_SPAN 16
#define KEY1_AES_TYPE "altera_avalon_pio"


/*
 * key2_AES configuration
 *
 */

#define ALT_MODULE_CLASS_key2_AES altera_avalon_pio
#define KEY2_AES_BASE 0xd0b0
#define KEY2_AES_BIT_CLEARING_EDGE_REGISTER 0
#define KEY2_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define KEY2_AES_CAPTURE 0
#define KEY2_AES_DATA_WIDTH 32
#define KEY2_AES_DO_TEST_BENCH_WIRING 0
#define KEY2_AES_DRIVEN_SIM_VALUE 0
#define KEY2_AES_EDGE_TYPE "NONE"
#define KEY2_AES_FREQ 50000000
#define KEY2_AES_HAS_IN 0
#define KEY2_AES_HAS_OUT 1
#define KEY2_AES_HAS_TRI 0
#define KEY2_AES_IRQ -1
#define KEY2_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define KEY2_AES_IRQ_TYPE "NONE"
#define KEY2_AES_NAME "/dev/key2_AES"
#define KEY2_AES_RESET_VALUE 0
#define KEY2_AES_SPAN 16
#define KEY2_AES_TYPE "altera_avalon_pio"


/*
 * key3_AES configuration
 *
 */

#define ALT_MODULE_CLASS_key3_AES altera_avalon_pio
#define KEY3_AES_BASE 0xd0c0
#define KEY3_AES_BIT_CLEARING_EDGE_REGISTER 0
#define KEY3_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define KEY3_AES_CAPTURE 0
#define KEY3_AES_DATA_WIDTH 32
#define KEY3_AES_DO_TEST_BENCH_WIRING 0
#define KEY3_AES_DRIVEN_SIM_VALUE 0
#define KEY3_AES_EDGE_TYPE "NONE"
#define KEY3_AES_FREQ 50000000
#define KEY3_AES_HAS_IN 0
#define KEY3_AES_HAS_OUT 1
#define KEY3_AES_HAS_TRI 0
#define KEY3_AES_IRQ -1
#define KEY3_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define KEY3_AES_IRQ_TYPE "NONE"
#define KEY3_AES_NAME "/dev/key3_AES"
#define KEY3_AES_RESET_VALUE 0
#define KEY3_AES_SPAN 16
#define KEY3_AES_TYPE "altera_avalon_pio"


/*
 * onchip_RAM configuration
 *
 */

#define ALT_MODULE_CLASS_onchip_RAM altera_avalon_onchip_memory2
#define ONCHIP_RAM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define ONCHIP_RAM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define ONCHIP_RAM_BASE 0x8000
#define ONCHIP_RAM_CONTENTS_INFO ""
#define ONCHIP_RAM_DUAL_PORT 0
#define ONCHIP_RAM_GUI_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_RAM_INIT_CONTENTS_FILE "niosForAES_onchip_RAM"
#define ONCHIP_RAM_INIT_MEM_CONTENT 1
#define ONCHIP_RAM_INSTANCE_ID "NONE"
#define ONCHIP_RAM_IRQ -1
#define ONCHIP_RAM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ONCHIP_RAM_NAME "/dev/onchip_RAM"
#define ONCHIP_RAM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define ONCHIP_RAM_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_RAM_READ_DURING_WRITE_MODE "DONT_CARE"
#define ONCHIP_RAM_SINGLE_CLOCK_OP 0
#define ONCHIP_RAM_SIZE_MULTIPLE 1
#define ONCHIP_RAM_SIZE_VALUE 16384
#define ONCHIP_RAM_SPAN 16384
#define ONCHIP_RAM_TYPE "altera_avalon_onchip_memory2"
#define ONCHIP_RAM_WRITABLE 1


/*
 * onchip_ROM configuration
 *
 */

#define ALT_MODULE_CLASS_onchip_ROM altera_avalon_onchip_memory2
#define ONCHIP_ROM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define ONCHIP_ROM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define ONCHIP_ROM_BASE 0x4000
#define ONCHIP_ROM_CONTENTS_INFO ""
#define ONCHIP_ROM_DUAL_PORT 0
#define ONCHIP_ROM_GUI_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_ROM_INIT_CONTENTS_FILE "niosForAES_onchip_ROM"
#define ONCHIP_ROM_INIT_MEM_CONTENT 1
#define ONCHIP_ROM_INSTANCE_ID "NONE"
#define ONCHIP_ROM_IRQ -1
#define ONCHIP_ROM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ONCHIP_ROM_NAME "/dev/onchip_ROM"
#define ONCHIP_ROM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define ONCHIP_ROM_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_ROM_READ_DURING_WRITE_MODE "DONT_CARE"
#define ONCHIP_ROM_SINGLE_CLOCK_OP 0
#define ONCHIP_ROM_SIZE_MULTIPLE 1
#define ONCHIP_ROM_SIZE_VALUE 16384
#define ONCHIP_ROM_SPAN 16384
#define ONCHIP_ROM_TYPE "altera_avalon_onchip_memory2"
#define ONCHIP_ROM_WRITABLE 0


/*
 * output0_AES configuration
 *
 */

#define ALT_MODULE_CLASS_output0_AES altera_avalon_pio
#define OUTPUT0_AES_BASE 0xd050
#define OUTPUT0_AES_BIT_CLEARING_EDGE_REGISTER 0
#define OUTPUT0_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define OUTPUT0_AES_CAPTURE 0
#define OUTPUT0_AES_DATA_WIDTH 32
#define OUTPUT0_AES_DO_TEST_BENCH_WIRING 0
#define OUTPUT0_AES_DRIVEN_SIM_VALUE 0
#define OUTPUT0_AES_EDGE_TYPE "NONE"
#define OUTPUT0_AES_FREQ 50000000
#define OUTPUT0_AES_HAS_IN 1
#define OUTPUT0_AES_HAS_OUT 0
#define OUTPUT0_AES_HAS_TRI 0
#define OUTPUT0_AES_IRQ -1
#define OUTPUT0_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define OUTPUT0_AES_IRQ_TYPE "NONE"
#define OUTPUT0_AES_NAME "/dev/output0_AES"
#define OUTPUT0_AES_RESET_VALUE 0
#define OUTPUT0_AES_SPAN 16
#define OUTPUT0_AES_TYPE "altera_avalon_pio"


/*
 * output1_AES configuration
 *
 */

#define ALT_MODULE_CLASS_output1_AES altera_avalon_pio
#define OUTPUT1_AES_BASE 0xd060
#define OUTPUT1_AES_BIT_CLEARING_EDGE_REGISTER 0
#define OUTPUT1_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define OUTPUT1_AES_CAPTURE 0
#define OUTPUT1_AES_DATA_WIDTH 32
#define OUTPUT1_AES_DO_TEST_BENCH_WIRING 0
#define OUTPUT1_AES_DRIVEN_SIM_VALUE 0
#define OUTPUT1_AES_EDGE_TYPE "NONE"
#define OUTPUT1_AES_FREQ 50000000
#define OUTPUT1_AES_HAS_IN 1
#define OUTPUT1_AES_HAS_OUT 0
#define OUTPUT1_AES_HAS_TRI 0
#define OUTPUT1_AES_IRQ -1
#define OUTPUT1_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define OUTPUT1_AES_IRQ_TYPE "NONE"
#define OUTPUT1_AES_NAME "/dev/output1_AES"
#define OUTPUT1_AES_RESET_VALUE 0
#define OUTPUT1_AES_SPAN 16
#define OUTPUT1_AES_TYPE "altera_avalon_pio"


/*
 * output2_AES configuration
 *
 */

#define ALT_MODULE_CLASS_output2_AES altera_avalon_pio
#define OUTPUT2_AES_BASE 0xd070
#define OUTPUT2_AES_BIT_CLEARING_EDGE_REGISTER 0
#define OUTPUT2_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define OUTPUT2_AES_CAPTURE 0
#define OUTPUT2_AES_DATA_WIDTH 32
#define OUTPUT2_AES_DO_TEST_BENCH_WIRING 0
#define OUTPUT2_AES_DRIVEN_SIM_VALUE 0
#define OUTPUT2_AES_EDGE_TYPE "NONE"
#define OUTPUT2_AES_FREQ 50000000
#define OUTPUT2_AES_HAS_IN 1
#define OUTPUT2_AES_HAS_OUT 0
#define OUTPUT2_AES_HAS_TRI 0
#define OUTPUT2_AES_IRQ -1
#define OUTPUT2_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define OUTPUT2_AES_IRQ_TYPE "NONE"
#define OUTPUT2_AES_NAME "/dev/output2_AES"
#define OUTPUT2_AES_RESET_VALUE 0
#define OUTPUT2_AES_SPAN 16
#define OUTPUT2_AES_TYPE "altera_avalon_pio"


/*
 * output3_AES configuration
 *
 */

#define ALT_MODULE_CLASS_output3_AES altera_avalon_pio
#define OUTPUT3_AES_BASE 0xd080
#define OUTPUT3_AES_BIT_CLEARING_EDGE_REGISTER 0
#define OUTPUT3_AES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define OUTPUT3_AES_CAPTURE 0
#define OUTPUT3_AES_DATA_WIDTH 32
#define OUTPUT3_AES_DO_TEST_BENCH_WIRING 0
#define OUTPUT3_AES_DRIVEN_SIM_VALUE 0
#define OUTPUT3_AES_EDGE_TYPE "NONE"
#define OUTPUT3_AES_FREQ 50000000
#define OUTPUT3_AES_HAS_IN 1
#define OUTPUT3_AES_HAS_OUT 0
#define OUTPUT3_AES_HAS_TRI 0
#define OUTPUT3_AES_IRQ -1
#define OUTPUT3_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define OUTPUT3_AES_IRQ_TYPE "NONE"
#define OUTPUT3_AES_NAME "/dev/output3_AES"
#define OUTPUT3_AES_RESET_VALUE 0
#define OUTPUT3_AES_SPAN 16
#define OUTPUT3_AES_TYPE "altera_avalon_pio"


/*
 * timer_AES configuration
 *
 */

#define ALT_MODULE_CLASS_timer_AES altera_avalon_timer
#define TIMER_AES_ALWAYS_RUN 0
#define TIMER_AES_BASE 0xd000
#define TIMER_AES_COUNTER_SIZE 32
#define TIMER_AES_FIXED_PERIOD 0
#define TIMER_AES_FREQ 50000000
#define TIMER_AES_IRQ -1
#define TIMER_AES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TIMER_AES_LOAD_VALUE 49999
#define TIMER_AES_MULT 0.0010
#define TIMER_AES_NAME "/dev/timer_AES"
#define TIMER_AES_PERIOD 1
#define TIMER_AES_PERIOD_UNITS "ms"
#define TIMER_AES_RESET_OUTPUT 0
#define TIMER_AES_SNAPSHOT 1
#define TIMER_AES_SPAN 32
#define TIMER_AES_TICKS_PER_SEC 1000.0
#define TIMER_AES_TIMEOUT_PULSE_OUTPUT 0
#define TIMER_AES_TYPE "altera_avalon_timer"


/*
 * write configuration
 *
 */

#define ALT_MODULE_CLASS_write altera_avalon_pio
#define WRITE_BASE 0xd020
#define WRITE_BIT_CLEARING_EDGE_REGISTER 0
#define WRITE_BIT_MODIFYING_OUTPUT_REGISTER 0
#define WRITE_CAPTURE 0
#define WRITE_DATA_WIDTH 1
#define WRITE_DO_TEST_BENCH_WIRING 0
#define WRITE_DRIVEN_SIM_VALUE 0
#define WRITE_EDGE_TYPE "NONE"
#define WRITE_FREQ 50000000
#define WRITE_HAS_IN 0
#define WRITE_HAS_OUT 1
#define WRITE_HAS_TRI 0
#define WRITE_IRQ -1
#define WRITE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define WRITE_IRQ_TYPE "NONE"
#define WRITE_NAME "/dev/write"
#define WRITE_RESET_VALUE 0
#define WRITE_SPAN 16
#define WRITE_TYPE "altera_avalon_pio"

#endif /* __SYSTEM_H_ */
