module aes_v(
in3, in2, in1, in0, 
key3, key2, key1, key0, 
out_reg3, out_reg2, out_reg1, out_reg0, 
write, done, reset, clk);

input [31:0] in3, in2, in1, in0, key3, key2, key1, key0;
output reg [31:0] out_reg3, out_reg2, out_reg1, out_reg0;
input clk, reset, write;
reg [127:0] 
outmux0_reg,
outark_reg,
outmsb_reg,
outsr_reg,
outmc_reg,
in_reg,
key_reg
;
reg [3:0] count = 0;
reg [5:0] iter;
reg mux0sel, demux0sel, mux1sel, demux1sel, enable, keys_en;
output reg done;
wire [127:0] 
outmux1,
outmux0,
outark,
outdemux0,
outmsb,
outsr,
outmc,
outdemux1_0,
outdemux1_1,
outkey,
out
;

keyexpansion_module keyexp(key_reg, outkey, count, keys_en, clk);
multiplexer mux0(in_reg, outmux1, outmux0, mux0sel, clk);
addroundkey ark(outmux0_reg, outkey, outark, enable, clk); 
demultiplexer demux0(outark_reg, outdemux0, out, demux0sel, clk);
multisubbytes msb(outdemux0, outmsb, enable, clk);
shiftrows sr(outmsb_reg, outsr, enable, clk);
demultiplexer demux1(outsr_reg, outdemux1_0, outdemux1_1, demux1sel, clk);
mixcolumn mc(outdemux1_0, outmc, enable, clk);
multiplexer mux1(outmc_reg, outdemux1_1, outmux1, mux1sel, clk);

always @(posedge clk)
begin
	in_reg[127:96] <= in3;
	in_reg[95:64] <= in2;
	in_reg[63:32] <= in1;
	in_reg[31:0] <= in0;
	key_reg[127:96] <= key3;
	key_reg[95:64] <= key2;
	key_reg[63:32] <= key1;
	key_reg[31:0] <= key0;
	outmux0_reg <= outmux0;
	outark_reg <= outark;
	outmsb_reg <= outmsb;
	outmc_reg <= outmc;
	if(reset==0||write==0) begin
		enable <= 0;
		iter <= 0;
		count <= 0;
	end
	else begin
		enable <= 1;
		if(count>10) count <= 0;
		else begin
			if(iter>7) begin
				iter <= 0;
				count <= count+1;
			end
			else begin
				iter <= iter+1;
			end
		end
	end
end

always @(count)
begin
	if(count==0) begin
		mux0sel <= 0;
		demux0sel <= 0;
		mux1sel <= 0;
		demux1sel <= 0;
		done <= 0;
	end
	if(count>0) begin 
		mux0sel <= 1;
	end
	if(count==10) begin
		mux1sel <= 1;
		demux1sel <= 1;
		if(iter>6) begin 
			demux0sel <= 1;
			done <= 1;
			out_reg3 <= out[127:96];
			out_reg2 <= out[95:64];
			out_reg1 <= out[63:32];
			out_reg0 <= out[31:0];
		end
	end
end

always @(iter)
begin
	if(iter == 4) begin
		keys_en <= 1;
	end
	else begin 
		keys_en <= 0;
	end
	if(iter == 3) outsr_reg <= outsr;
end

endmodule
