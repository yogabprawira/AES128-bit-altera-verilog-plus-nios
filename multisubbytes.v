module multisubbytes(in, out, enable, clk);

input [127:0] in;
output wire [127:0] out;
input clk, enable;

subbytes matriks00(in[7:0], out[7:0], enable, clk);
subbytes matriks10(in[15:8], out[15:8], enable, clk);
subbytes matriks20(in[23:16], out[23:16], enable, clk);
subbytes matriks30(in[31:24], out[31:24], enable, clk);
subbytes matriks01(in[39:32], out[39:32], enable, clk);
subbytes matriks11(in[47:40], out[47:40], enable, clk);
subbytes matriks21(in[55:48], out[55:48], enable, clk);
subbytes matriks31(in[63:56], out[63:56], enable, clk);
subbytes matriks02(in[71:64], out[71:64], enable, clk);
subbytes matriks12(in[79:72], out[79:72], enable, clk);
subbytes matriks22(in[87:80], out[87:80], enable, clk);
subbytes matriks32(in[95:88], out[95:88], enable, clk);
subbytes matriks03(in[103:96], out[103:96], enable, clk);
subbytes matriks13(in[111:104], out[111:104], enable, clk);
subbytes matriks23(in[119:112], out[119:112], enable, clk);
subbytes matriks33(in[127:120], out[127:120], enable, clk);

endmodule

