Projek ini merupakan implementasi engine AES 128-bit di FPGA Altera DE0-Nano menggunakan bahasa verilog dan program di prosesor NIOS II menggunakan bahasa C.
Program engine AES di FPGA hanya mendukung proses enkripsi dengan input 128-bit.
Prosesor NIOS II merupakan prosesor sederhana template dari altera yang bisa ditanamkan di FPGA bersama dengan engine AES yang telah dibuat sebelumnya. Prosesor ini dapat ditanami program menggunakan bahasa C.
Versi Altera Quartus yang dipakai adalah versi 13.1.