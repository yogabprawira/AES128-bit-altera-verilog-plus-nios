module mixcolumnperword (
in,
out, 
clk
);

input [31:0] in;
output wire [31:0] out;
wire [7:0] 
x02_3, x02_2, x02_1, x02_0,
x03_3, x03_2, x03_1, x03_0;
input clk;

multiply2 mod3(in[31:24], x02_3, clk);
multiply2 mod2(in[23:16], x02_2, clk);
multiply2 mod1(in[15:8], x02_1, clk);
multiply2 mod0(in[7:0], x02_0, clk);

assign x03_3 = x02_3 ^ in[31:24];
assign x03_2 = x02_2 ^ in[23:16];
assign x03_1 = x02_1 ^ in[15:8];
assign x03_0 = x02_0 ^ in[7:0];

assign out[7:0] = x02_0 ^ x03_1 ^ in[23:16] ^ in[31:24];
assign out[15:8] = in[7:0] ^ x02_1 ^ x03_2 ^ in[31:24];
assign out[23:16] = in[7:0] ^ in[15:8] ^ x02_2 ^ x03_3;
assign out[31:24] = x03_0 ^ in[15:8] ^ in[23:16] ^ x02_3;

endmodule
