module controller2(
clk, 
reset,
writedone, 
mux0sel,
muxdemux1demux0sel,
en_ark,
en_msb,
en_sr,
en_mc,
en_shiftregout,
sel_shiftregout,
keysc_sel,
keysc_en,
man_reset
);

input clk, reset, writedone;
output reg 
mux0sel, 
muxdemux1demux0sel,  
en_ark, 
en_msb, 
en_sr, 
en_mc, 
en_shiftregout,
man_reset,
keysc_en;
output reg [3:0] keysc_sel, sel_shiftregout;
reg [5:0] count=0;

always @(posedge clk)
begin
	if(reset) count = 0;
	else begin 
		count = count+1;
	end
end

always @(count)
begin
	case(count)
		0 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 0; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 0; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		1 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 0; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 0; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		2 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 0; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 0; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		3 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 0; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 0; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		4 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 1; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		5 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 1; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		6 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 1; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		7 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 1; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		8 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 2; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		9 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 2; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		10 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 2; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		11 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 2; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		12 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 3; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		13 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 3; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		14 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 3; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		15 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 3; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		16 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 4; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		17 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 4; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		18 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 4; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		19 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 4; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		20 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 5; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		21 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 5; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		22 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 5; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		23 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 5; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		24 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 6; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		25 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 6; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		26 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 6; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		27 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 6; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		28 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 7; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		29 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 7; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		30 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 7; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		31 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 7; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		32 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 8; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		33 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 8; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		34 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 8; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		35 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 8; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		36 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 9; en_msb <= 0; 
			en_sr <= 0; en_mc <= 1; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		37 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 9; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
		38 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 9; en_msb <= 1; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		39 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 1; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		40 : begin
			en_ark <= 1; keysc_en <= 1; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		41 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 0;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		42 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 1;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		43 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 2;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		44 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 3;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		45 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 4;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		46 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 5;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		47 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 6;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		48 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 7;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		48 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 8;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		49 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 9;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		50 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 10;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		51 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 11;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		52 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 12;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		53 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 13;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		54 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 14;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		55 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 1; sel_shiftregout <= 15;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 0;
		end
		56 : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 10; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 15;
			mux0sel <= 1; muxdemux1demux0sel <= 1; man_reset <= 1;
		end
		default : begin
			en_ark <= 0; keysc_en <= 0; keysc_sel <= 0; en_msb <= 0; 
			en_sr <= 0; en_mc <= 0; en_shiftregout <= 0; sel_shiftregout <= 0;
			mux0sel <= 0; muxdemux1demux0sel <= 0; man_reset <= 0;
		end
	endcase
end
endmodule
