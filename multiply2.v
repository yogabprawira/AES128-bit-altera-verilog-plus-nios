module multiply2 (
a,
b,
clk
);

input [7:0] a;
output reg [7:0] b;
input clk;

always @(posedge clk)
begin
	b[0] <= a[7];
	b[1] <= a[0] ^ a[7];
	b[2] <= a[1];
	b[3] <= a[2] ^ a[7];
	b[4] <= a[3] ^ a[7];
	b[5] <= a[4];
	b[6] <= a[5];
	b[7] <= a[6];
end
endmodule