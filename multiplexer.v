module multiplexer (in0, in1, out, sel, clk);

input [127:0] in0, in1;
output wire [127:0] out;
input sel, clk;
reg [127:0] regin;

always @(clk)
begin
	if(sel) regin <= in1;
	else regin <= in0;
end

assign out = regin;

endmodule
