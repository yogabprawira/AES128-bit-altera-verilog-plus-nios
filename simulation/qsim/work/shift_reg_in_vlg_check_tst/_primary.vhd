library verilog;
use verilog.vl_types.all;
entity shift_reg_in_vlg_check_tst is
    port(
        \out\           : in     vl_logic_vector(127 downto 0);
        writedone       : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end shift_reg_in_vlg_check_tst;
