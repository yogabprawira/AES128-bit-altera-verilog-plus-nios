library verilog;
use verilog.vl_types.all;
entity demultiplexer_vlg_check_tst is
    port(
        out0            : in     vl_logic_vector(127 downto 0);
        out1            : in     vl_logic_vector(127 downto 0);
        sampler_rx      : in     vl_logic
    );
end demultiplexer_vlg_check_tst;
