library verilog;
use verilog.vl_types.all;
entity demultiplexer_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        \in\            : in     vl_logic_vector(127 downto 0);
        sel             : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end demultiplexer_vlg_sample_tst;
