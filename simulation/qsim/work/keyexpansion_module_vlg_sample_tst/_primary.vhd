library verilog;
use verilog.vl_types.all;
entity keyexpansion_module_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        enable          : in     vl_logic;
        inkey           : in     vl_logic_vector(127 downto 0);
        sel             : in     vl_logic_vector(3 downto 0);
        sampler_tx      : out    vl_logic
    );
end keyexpansion_module_vlg_sample_tst;
