library verilog;
use verilog.vl_types.all;
entity keyschedule_vlg_check_tst is
    port(
        outkey          : in     vl_logic_vector(127 downto 0);
        sampler_rx      : in     vl_logic
    );
end keyschedule_vlg_check_tst;
