library verilog;
use verilog.vl_types.all;
entity shift_register_out is
    port(
        \in\            : in     vl_logic_vector(127 downto 0);
        \out\           : out    vl_logic_vector(31 downto 0);
        read            : in     vl_logic;
        clk             : in     vl_logic
    );
end shift_register_out;
