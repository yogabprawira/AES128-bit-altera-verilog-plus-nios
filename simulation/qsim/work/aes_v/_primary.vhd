library verilog;
use verilog.vl_types.all;
entity aes_v is
    port(
        in3             : in     vl_logic_vector(31 downto 0);
        in2             : in     vl_logic_vector(31 downto 0);
        in1             : in     vl_logic_vector(31 downto 0);
        in0             : in     vl_logic_vector(31 downto 0);
        key3            : in     vl_logic_vector(31 downto 0);
        key2            : in     vl_logic_vector(31 downto 0);
        key1            : in     vl_logic_vector(31 downto 0);
        key0            : in     vl_logic_vector(31 downto 0);
        out_reg3        : out    vl_logic_vector(31 downto 0);
        out_reg2        : out    vl_logic_vector(31 downto 0);
        out_reg1        : out    vl_logic_vector(31 downto 0);
        out_reg0        : out    vl_logic_vector(31 downto 0);
        write           : in     vl_logic;
        done            : out    vl_logic;
        reset           : in     vl_logic;
        clk             : in     vl_logic
    );
end aes_v;
