library verilog;
use verilog.vl_types.all;
entity aes_v_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        in0             : in     vl_logic_vector(31 downto 0);
        in1             : in     vl_logic_vector(31 downto 0);
        in2             : in     vl_logic_vector(31 downto 0);
        in3             : in     vl_logic_vector(31 downto 0);
        key0            : in     vl_logic_vector(31 downto 0);
        key1            : in     vl_logic_vector(31 downto 0);
        key2            : in     vl_logic_vector(31 downto 0);
        key3            : in     vl_logic_vector(31 downto 0);
        reset           : in     vl_logic;
        write           : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end aes_v_vlg_sample_tst;
