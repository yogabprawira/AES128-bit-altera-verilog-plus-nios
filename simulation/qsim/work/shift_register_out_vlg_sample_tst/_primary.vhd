library verilog;
use verilog.vl_types.all;
entity shift_register_out_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        \in\            : in     vl_logic_vector(127 downto 0);
        read            : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end shift_register_out_vlg_sample_tst;
