library verilog;
use verilog.vl_types.all;
entity shift_register_in is
    port(
        \in\            : in     vl_logic_vector(31 downto 0);
        inkey           : in     vl_logic_vector(31 downto 0);
        \out\           : out    vl_logic_vector(127 downto 0);
        outkey          : out    vl_logic_vector(127 downto 0);
        write           : in     vl_logic;
        clk             : in     vl_logic
    );
end shift_register_in;
