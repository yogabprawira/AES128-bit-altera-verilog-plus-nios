library verilog;
use verilog.vl_types.all;
entity aes_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        \in\            : in     vl_logic_vector(7 downto 0);
        key             : in     vl_logic_vector(7 downto 0);
        reset           : in     vl_logic;
        write           : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end aes_vlg_sample_tst;
