library verilog;
use verilog.vl_types.all;
entity shift_reg_in_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        \in\            : in     vl_logic_vector(7 downto 0);
        reset           : in     vl_logic;
        write           : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end shift_reg_in_vlg_sample_tst;
