library verilog;
use verilog.vl_types.all;
entity aes_vlg_check_tst is
    port(
        \out\           : in     vl_logic_vector(7 downto 0);
        writedone       : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end aes_vlg_check_tst;
