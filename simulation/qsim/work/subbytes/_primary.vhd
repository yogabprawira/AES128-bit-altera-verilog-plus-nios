library verilog;
use verilog.vl_types.all;
entity subbytes is
    port(
        inputSubBytes   : in     vl_logic_vector(7 downto 0);
        outputSubBytes  : out    vl_logic_vector(7 downto 0);
        enable          : in     vl_logic;
        clk             : in     vl_logic
    );
end subbytes;
