library verilog;
use verilog.vl_types.all;
entity shift_reg_in is
    port(
        \in\            : in     vl_logic_vector(7 downto 0);
        \out\           : out    vl_logic_vector(127 downto 0);
        write           : in     vl_logic;
        writedone       : out    vl_logic;
        reset           : in     vl_logic;
        clk             : in     vl_logic
    );
end shift_reg_in;
