library verilog;
use verilog.vl_types.all;
entity controller is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        writedone       : in     vl_logic;
        mux0sel         : out    vl_logic;
        muxdemux1demux0sel: out    vl_logic;
        en_ark          : out    vl_logic;
        en_msb          : out    vl_logic;
        en_sr           : out    vl_logic;
        en_mc           : out    vl_logic;
        en_shiftregout  : out    vl_logic;
        sel_shiftregout : out    vl_logic_vector(3 downto 0);
        keysc_sel       : out    vl_logic_vector(3 downto 0);
        keysc_en        : out    vl_logic;
        man_reset       : out    vl_logic
    );
end controller;
