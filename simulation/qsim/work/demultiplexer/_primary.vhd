library verilog;
use verilog.vl_types.all;
entity demultiplexer is
    port(
        \in\            : in     vl_logic_vector(127 downto 0);
        out0            : out    vl_logic_vector(127 downto 0);
        out1            : out    vl_logic_vector(127 downto 0);
        sel             : in     vl_logic;
        clk             : in     vl_logic
    );
end demultiplexer;
