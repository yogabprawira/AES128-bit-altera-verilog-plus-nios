library verilog;
use verilog.vl_types.all;
entity subbytes_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        enable          : in     vl_logic;
        inputSubBytes   : in     vl_logic_vector(7 downto 0);
        sampler_tx      : out    vl_logic
    );
end subbytes_vlg_sample_tst;
