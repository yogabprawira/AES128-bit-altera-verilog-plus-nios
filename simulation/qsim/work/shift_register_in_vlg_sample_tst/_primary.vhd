library verilog;
use verilog.vl_types.all;
entity shift_register_in_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        \in\            : in     vl_logic_vector(31 downto 0);
        inkey           : in     vl_logic_vector(31 downto 0);
        write           : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end shift_register_in_vlg_sample_tst;
