library verilog;
use verilog.vl_types.all;
entity keyexpansion_vlg_check_tst is
    port(
        outkey          : in     vl_logic_vector(127 downto 0);
        sampler_rx      : in     vl_logic
    );
end keyexpansion_vlg_check_tst;
