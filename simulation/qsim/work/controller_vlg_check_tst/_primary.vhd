library verilog;
use verilog.vl_types.all;
entity controller_vlg_check_tst is
    port(
        en_ark          : in     vl_logic;
        en_mc           : in     vl_logic;
        en_msb          : in     vl_logic;
        en_shiftregout  : in     vl_logic;
        en_sr           : in     vl_logic;
        keysc_en        : in     vl_logic;
        keysc_sel       : in     vl_logic_vector(3 downto 0);
        man_reset       : in     vl_logic;
        mux0sel         : in     vl_logic;
        muxdemux1demux0sel: in     vl_logic;
        sel_shiftregout : in     vl_logic_vector(3 downto 0);
        sampler_rx      : in     vl_logic
    );
end controller_vlg_check_tst;
