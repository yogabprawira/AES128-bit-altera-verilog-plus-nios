library verilog;
use verilog.vl_types.all;
entity shiftrows_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        enable          : in     vl_logic;
        \in\            : in     vl_logic_vector(127 downto 0);
        sampler_tx      : out    vl_logic
    );
end shiftrows_vlg_sample_tst;
