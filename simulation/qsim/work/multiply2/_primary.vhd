library verilog;
use verilog.vl_types.all;
entity multiply2 is
    port(
        a               : in     vl_logic_vector(7 downto 0);
        b               : out    vl_logic_vector(7 downto 0);
        clk             : in     vl_logic
    );
end multiply2;
