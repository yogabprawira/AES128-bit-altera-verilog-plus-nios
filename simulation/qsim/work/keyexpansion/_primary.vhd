library verilog;
use verilog.vl_types.all;
entity keyexpansion is
    port(
        inkey           : in     vl_logic_vector(127 downto 0);
        outkey          : out    vl_logic_vector(127 downto 0);
        sel             : in     vl_logic_vector(3 downto 0);
        clk             : in     vl_logic
    );
end keyexpansion;
