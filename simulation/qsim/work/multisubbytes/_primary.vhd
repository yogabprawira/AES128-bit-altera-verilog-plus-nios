library verilog;
use verilog.vl_types.all;
entity multisubbytes is
    port(
        \in\            : in     vl_logic_vector(127 downto 0);
        \out\           : out    vl_logic_vector(127 downto 0);
        enable          : in     vl_logic;
        clk             : in     vl_logic
    );
end multisubbytes;
