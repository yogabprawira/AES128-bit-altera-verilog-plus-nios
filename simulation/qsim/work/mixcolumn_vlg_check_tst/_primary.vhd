library verilog;
use verilog.vl_types.all;
entity mixcolumn_vlg_check_tst is
    port(
        \out\           : in     vl_logic_vector(127 downto 0);
        sampler_rx      : in     vl_logic
    );
end mixcolumn_vlg_check_tst;
