library verilog;
use verilog.vl_types.all;
entity aes_v_vlg_check_tst is
    port(
        done            : in     vl_logic;
        out_reg0        : in     vl_logic_vector(31 downto 0);
        out_reg1        : in     vl_logic_vector(31 downto 0);
        out_reg2        : in     vl_logic_vector(31 downto 0);
        out_reg3        : in     vl_logic_vector(31 downto 0);
        sampler_rx      : in     vl_logic
    );
end aes_v_vlg_check_tst;
