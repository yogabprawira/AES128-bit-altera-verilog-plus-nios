library verilog;
use verilog.vl_types.all;
entity keyschedule is
    port(
        inkey           : in     vl_logic_vector(127 downto 0);
        outkey          : out    vl_logic_vector(127 downto 0);
        sel             : in     vl_logic_vector(3 downto 0);
        enable          : in     vl_logic;
        clk             : in     vl_logic
    );
end keyschedule;
