library verilog;
use verilog.vl_types.all;
entity aes is
    port(
        writedone       : out    vl_logic;
        write           : in     vl_logic;
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        \in\            : in     vl_logic_vector(7 downto 0);
        key             : in     vl_logic_vector(7 downto 0);
        \out\           : out    vl_logic_vector(7 downto 0)
    );
end aes;
