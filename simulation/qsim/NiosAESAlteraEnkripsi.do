onerror {exit -code 1}
vlib work
vlog -work work NiosAESAlteraEnkripsi.vo
vlog -work work aes_v2.vwf.vt
vsim -novopt -c -t 1ps -L cycloneive_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.aes_v_vlg_vec_tst -voptargs="+acc"
vcd file -direction NiosAESAlteraEnkripsi.msim.vcd
vcd add -internal aes_v_vlg_vec_tst/*
vcd add -internal aes_v_vlg_vec_tst/i1/*
run -all
quit -f
