-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "11/13/2015 14:26:16"

-- 
-- Device: Altera EP4CE40F29C6 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	mixcolumn IS
    PORT (
	\in\ : IN std_logic_vector(127 DOWNTO 0);
	\out\ : OUT std_logic_vector(127 DOWNTO 0);
	enable : IN std_logic;
	clk : IN std_logic
	);
END mixcolumn;

-- Design Ports Information
-- out[0]	=>  Location: PIN_AC17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[1]	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[2]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[3]	=>  Location: PIN_A21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[4]	=>  Location: PIN_A22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[5]	=>  Location: PIN_J19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[6]	=>  Location: PIN_B25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[7]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[8]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[9]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[10]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[11]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[12]	=>  Location: PIN_C24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[13]	=>  Location: PIN_C26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[14]	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[15]	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[16]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[17]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[18]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[19]	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[20]	=>  Location: PIN_C22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[21]	=>  Location: PIN_A26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[22]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[23]	=>  Location: PIN_D20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[24]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[25]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[26]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[27]	=>  Location: PIN_C21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[28]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[29]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[30]	=>  Location: PIN_D25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[31]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[32]	=>  Location: PIN_U26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[33]	=>  Location: PIN_AD28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[34]	=>  Location: PIN_AB19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[35]	=>  Location: PIN_AB20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[36]	=>  Location: PIN_AE20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[37]	=>  Location: PIN_W26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[38]	=>  Location: PIN_AC21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[39]	=>  Location: PIN_AD19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[40]	=>  Location: PIN_AA23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[41]	=>  Location: PIN_U24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[42]	=>  Location: PIN_AE25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[43]	=>  Location: PIN_AH25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[44]	=>  Location: PIN_AB18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[45]	=>  Location: PIN_AE22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[46]	=>  Location: PIN_AC28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[47]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[48]	=>  Location: PIN_Y26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[49]	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[50]	=>  Location: PIN_AB24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[51]	=>  Location: PIN_AG23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[52]	=>  Location: PIN_AC19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[53]	=>  Location: PIN_AD22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[54]	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[55]	=>  Location: PIN_AC18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[56]	=>  Location: PIN_AD27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[57]	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[58]	=>  Location: PIN_AB23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[59]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[60]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[61]	=>  Location: PIN_AC26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[62]	=>  Location: PIN_AE23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[63]	=>  Location: PIN_AA17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[64]	=>  Location: PIN_T21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[65]	=>  Location: PIN_T22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[66]	=>  Location: PIN_AF25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[67]	=>  Location: PIN_AG19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[68]	=>  Location: PIN_Y13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[69]	=>  Location: PIN_AA12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[70]	=>  Location: PIN_AC12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[71]	=>  Location: PIN_Y14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[72]	=>  Location: PIN_R24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[73]	=>  Location: PIN_T26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[74]	=>  Location: PIN_AE21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[75]	=>  Location: PIN_AF20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[76]	=>  Location: PIN_AG8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[77]	=>  Location: PIN_AB13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[78]	=>  Location: PIN_AG6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[79]	=>  Location: PIN_AA10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[80]	=>  Location: PIN_U25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[81]	=>  Location: PIN_V20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[82]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[83]	=>  Location: PIN_AG22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[84]	=>  Location: PIN_AB11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[85]	=>  Location: PIN_AF6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[86]	=>  Location: PIN_AD8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[87]	=>  Location: PIN_AE10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[88]	=>  Location: PIN_R25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[89]	=>  Location: PIN_T25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[90]	=>  Location: PIN_AD18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[91]	=>  Location: PIN_AF19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[92]	=>  Location: PIN_AH7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[93]	=>  Location: PIN_W10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[94]	=>  Location: PIN_AH3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[95]	=>  Location: PIN_Y15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[96]	=>  Location: PIN_AD4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[97]	=>  Location: PIN_AD12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[98]	=>  Location: PIN_AF13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[99]	=>  Location: PIN_AG12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[100]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[101]	=>  Location: PIN_AE14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[102]	=>  Location: PIN_AF17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[103]	=>  Location: PIN_AH10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[104]	=>  Location: PIN_AB4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[105]	=>  Location: PIN_AB7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[106]	=>  Location: PIN_AF9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[107]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[108]	=>  Location: PIN_AD7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[109]	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[110]	=>  Location: PIN_AC15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[111]	=>  Location: PIN_AF7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[112]	=>  Location: PIN_AE3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[113]	=>  Location: PIN_AC11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[114]	=>  Location: PIN_AE9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[115]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[116]	=>  Location: PIN_AB14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[117]	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[118]	=>  Location: PIN_AH17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[119]	=>  Location: PIN_AF12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[120]	=>  Location: PIN_AB8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[121]	=>  Location: PIN_AE6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[122]	=>  Location: PIN_AH8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[123]	=>  Location: PIN_AE12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[124]	=>  Location: PIN_Y12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[125]	=>  Location: PIN_AD14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[126]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out[127]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- enable	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[8]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[16]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[24]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[9]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[17]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[25]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[10]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[18]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[26]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[11]	=>  Location: PIN_D21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[19]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[27]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[12]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[20]	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[28]	=>  Location: PIN_C25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[13]	=>  Location: PIN_D26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[21]	=>  Location: PIN_B26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[29]	=>  Location: PIN_A23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[14]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[22]	=>  Location: PIN_D23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[30]	=>  Location: PIN_C23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[15]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[23]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[31]	=>  Location: PIN_A25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[0]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[1]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[2]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[3]	=>  Location: PIN_B22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[4]	=>  Location: PIN_B23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[5]	=>  Location: PIN_D22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[6]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[7]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[40]	=>  Location: PIN_AA26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[48]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[56]	=>  Location: PIN_V22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[41]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[49]	=>  Location: PIN_Y22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[57]	=>  Location: PIN_AC27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[42]	=>  Location: PIN_AD21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[50]	=>  Location: PIN_AD25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[58]	=>  Location: PIN_AF27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[43]	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[51]	=>  Location: PIN_AF22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[59]	=>  Location: PIN_AE24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[44]	=>  Location: PIN_AF26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[52]	=>  Location: PIN_AG26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[60]	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[45]	=>  Location: PIN_AC24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[53]	=>  Location: PIN_AH26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[61]	=>  Location: PIN_AC22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[46]	=>  Location: PIN_AE27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[54]	=>  Location: PIN_AA21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[62]	=>  Location: PIN_AD24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[47]	=>  Location: PIN_AE28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[55]	=>  Location: PIN_AC25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[63]	=>  Location: PIN_AA24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[32]	=>  Location: PIN_V26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[33]	=>  Location: PIN_AD26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[34]	=>  Location: PIN_AF21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[35]	=>  Location: PIN_AF23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[36]	=>  Location: PIN_AE26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[37]	=>  Location: PIN_AB21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[38]	=>  Location: PIN_AG25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[39]	=>  Location: PIN_AB26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[72]	=>  Location: PIN_Y28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[80]	=>  Location: PIN_Y27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[88]	=>  Location: PIN_R26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[73]	=>  Location: PIN_V23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[81]	=>  Location: PIN_U22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[89]	=>  Location: PIN_U23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[74]	=>  Location: PIN_AG15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[82]	=>  Location: PIN_AH15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[90]	=>  Location: PIN_AA19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[75]	=>  Location: PIN_AF16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[83]	=>  Location: PIN_AH23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[91]	=>  Location: PIN_AH18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[76]	=>  Location: PIN_AC8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[84]	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[92]	=>  Location: PIN_AH6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[77]	=>  Location: PIN_AE5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[85]	=>  Location: PIN_AF5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[93]	=>  Location: PIN_AH4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[78]	=>  Location: PIN_AC10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[86]	=>  Location: PIN_AG7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[94]	=>  Location: PIN_AB9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[79]	=>  Location: PIN_V25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[87]	=>  Location: PIN_U27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[95]	=>  Location: PIN_AB25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[64]	=>  Location: PIN_R28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[65]	=>  Location: PIN_V24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[66]	=>  Location: PIN_AF24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[67]	=>  Location: PIN_AF18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[68]	=>  Location: PIN_AD10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[69]	=>  Location: PIN_AE8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[70]	=>  Location: PIN_AG3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[71]	=>  Location: PIN_V27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[104]	=>  Location: PIN_AD3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[112]	=>  Location: PIN_AD11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[120]	=>  Location: PIN_AG4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[105]	=>  Location: PIN_AC5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[113]	=>  Location: PIN_AF3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[121]	=>  Location: PIN_Y10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[106]	=>  Location: PIN_AH12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[114]	=>  Location: PIN_AF10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[122]	=>  Location: PIN_AB6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[107]	=>  Location: PIN_AE13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[115]	=>  Location: PIN_AH11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[123]	=>  Location: PIN_AE17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[108]	=>  Location: PIN_AG17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[116]	=>  Location: PIN_AF15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[124]	=>  Location: PIN_AD15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[109]	=>  Location: PIN_AB16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[117]	=>  Location: PIN_AA16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[125]	=>  Location: PIN_AE15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[110]	=>  Location: PIN_AC14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[118]	=>  Location: PIN_AG11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[126]	=>  Location: PIN_AE11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[111]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[119]	=>  Location: PIN_AE7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[127]	=>  Location: PIN_AD5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[96]	=>  Location: PIN_AC4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[97]	=>  Location: PIN_AE4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[98]	=>  Location: PIN_AF8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[99]	=>  Location: PIN_AG10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[100]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[101]	=>  Location: PIN_AE16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[102]	=>  Location: PIN_AF11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in[103]	=>  Location: PIN_AF4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF mixcolumn IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \ww_in\ : std_logic_vector(127 DOWNTO 0);
SIGNAL \ww_out\ : std_logic_vector(127 DOWNTO 0);
SIGNAL ww_enable : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \enable~input_o\ : std_logic;
SIGNAL \out[0]~output_o\ : std_logic;
SIGNAL \out[1]~output_o\ : std_logic;
SIGNAL \out[2]~output_o\ : std_logic;
SIGNAL \out[3]~output_o\ : std_logic;
SIGNAL \out[4]~output_o\ : std_logic;
SIGNAL \out[5]~output_o\ : std_logic;
SIGNAL \out[6]~output_o\ : std_logic;
SIGNAL \out[7]~output_o\ : std_logic;
SIGNAL \out[8]~output_o\ : std_logic;
SIGNAL \out[9]~output_o\ : std_logic;
SIGNAL \out[10]~output_o\ : std_logic;
SIGNAL \out[11]~output_o\ : std_logic;
SIGNAL \out[12]~output_o\ : std_logic;
SIGNAL \out[13]~output_o\ : std_logic;
SIGNAL \out[14]~output_o\ : std_logic;
SIGNAL \out[15]~output_o\ : std_logic;
SIGNAL \out[16]~output_o\ : std_logic;
SIGNAL \out[17]~output_o\ : std_logic;
SIGNAL \out[18]~output_o\ : std_logic;
SIGNAL \out[19]~output_o\ : std_logic;
SIGNAL \out[20]~output_o\ : std_logic;
SIGNAL \out[21]~output_o\ : std_logic;
SIGNAL \out[22]~output_o\ : std_logic;
SIGNAL \out[23]~output_o\ : std_logic;
SIGNAL \out[24]~output_o\ : std_logic;
SIGNAL \out[25]~output_o\ : std_logic;
SIGNAL \out[26]~output_o\ : std_logic;
SIGNAL \out[27]~output_o\ : std_logic;
SIGNAL \out[28]~output_o\ : std_logic;
SIGNAL \out[29]~output_o\ : std_logic;
SIGNAL \out[30]~output_o\ : std_logic;
SIGNAL \out[31]~output_o\ : std_logic;
SIGNAL \out[32]~output_o\ : std_logic;
SIGNAL \out[33]~output_o\ : std_logic;
SIGNAL \out[34]~output_o\ : std_logic;
SIGNAL \out[35]~output_o\ : std_logic;
SIGNAL \out[36]~output_o\ : std_logic;
SIGNAL \out[37]~output_o\ : std_logic;
SIGNAL \out[38]~output_o\ : std_logic;
SIGNAL \out[39]~output_o\ : std_logic;
SIGNAL \out[40]~output_o\ : std_logic;
SIGNAL \out[41]~output_o\ : std_logic;
SIGNAL \out[42]~output_o\ : std_logic;
SIGNAL \out[43]~output_o\ : std_logic;
SIGNAL \out[44]~output_o\ : std_logic;
SIGNAL \out[45]~output_o\ : std_logic;
SIGNAL \out[46]~output_o\ : std_logic;
SIGNAL \out[47]~output_o\ : std_logic;
SIGNAL \out[48]~output_o\ : std_logic;
SIGNAL \out[49]~output_o\ : std_logic;
SIGNAL \out[50]~output_o\ : std_logic;
SIGNAL \out[51]~output_o\ : std_logic;
SIGNAL \out[52]~output_o\ : std_logic;
SIGNAL \out[53]~output_o\ : std_logic;
SIGNAL \out[54]~output_o\ : std_logic;
SIGNAL \out[55]~output_o\ : std_logic;
SIGNAL \out[56]~output_o\ : std_logic;
SIGNAL \out[57]~output_o\ : std_logic;
SIGNAL \out[58]~output_o\ : std_logic;
SIGNAL \out[59]~output_o\ : std_logic;
SIGNAL \out[60]~output_o\ : std_logic;
SIGNAL \out[61]~output_o\ : std_logic;
SIGNAL \out[62]~output_o\ : std_logic;
SIGNAL \out[63]~output_o\ : std_logic;
SIGNAL \out[64]~output_o\ : std_logic;
SIGNAL \out[65]~output_o\ : std_logic;
SIGNAL \out[66]~output_o\ : std_logic;
SIGNAL \out[67]~output_o\ : std_logic;
SIGNAL \out[68]~output_o\ : std_logic;
SIGNAL \out[69]~output_o\ : std_logic;
SIGNAL \out[70]~output_o\ : std_logic;
SIGNAL \out[71]~output_o\ : std_logic;
SIGNAL \out[72]~output_o\ : std_logic;
SIGNAL \out[73]~output_o\ : std_logic;
SIGNAL \out[74]~output_o\ : std_logic;
SIGNAL \out[75]~output_o\ : std_logic;
SIGNAL \out[76]~output_o\ : std_logic;
SIGNAL \out[77]~output_o\ : std_logic;
SIGNAL \out[78]~output_o\ : std_logic;
SIGNAL \out[79]~output_o\ : std_logic;
SIGNAL \out[80]~output_o\ : std_logic;
SIGNAL \out[81]~output_o\ : std_logic;
SIGNAL \out[82]~output_o\ : std_logic;
SIGNAL \out[83]~output_o\ : std_logic;
SIGNAL \out[84]~output_o\ : std_logic;
SIGNAL \out[85]~output_o\ : std_logic;
SIGNAL \out[86]~output_o\ : std_logic;
SIGNAL \out[87]~output_o\ : std_logic;
SIGNAL \out[88]~output_o\ : std_logic;
SIGNAL \out[89]~output_o\ : std_logic;
SIGNAL \out[90]~output_o\ : std_logic;
SIGNAL \out[91]~output_o\ : std_logic;
SIGNAL \out[92]~output_o\ : std_logic;
SIGNAL \out[93]~output_o\ : std_logic;
SIGNAL \out[94]~output_o\ : std_logic;
SIGNAL \out[95]~output_o\ : std_logic;
SIGNAL \out[96]~output_o\ : std_logic;
SIGNAL \out[97]~output_o\ : std_logic;
SIGNAL \out[98]~output_o\ : std_logic;
SIGNAL \out[99]~output_o\ : std_logic;
SIGNAL \out[100]~output_o\ : std_logic;
SIGNAL \out[101]~output_o\ : std_logic;
SIGNAL \out[102]~output_o\ : std_logic;
SIGNAL \out[103]~output_o\ : std_logic;
SIGNAL \out[104]~output_o\ : std_logic;
SIGNAL \out[105]~output_o\ : std_logic;
SIGNAL \out[106]~output_o\ : std_logic;
SIGNAL \out[107]~output_o\ : std_logic;
SIGNAL \out[108]~output_o\ : std_logic;
SIGNAL \out[109]~output_o\ : std_logic;
SIGNAL \out[110]~output_o\ : std_logic;
SIGNAL \out[111]~output_o\ : std_logic;
SIGNAL \out[112]~output_o\ : std_logic;
SIGNAL \out[113]~output_o\ : std_logic;
SIGNAL \out[114]~output_o\ : std_logic;
SIGNAL \out[115]~output_o\ : std_logic;
SIGNAL \out[116]~output_o\ : std_logic;
SIGNAL \out[117]~output_o\ : std_logic;
SIGNAL \out[118]~output_o\ : std_logic;
SIGNAL \out[119]~output_o\ : std_logic;
SIGNAL \out[120]~output_o\ : std_logic;
SIGNAL \out[121]~output_o\ : std_logic;
SIGNAL \out[122]~output_o\ : std_logic;
SIGNAL \out[123]~output_o\ : std_logic;
SIGNAL \out[124]~output_o\ : std_logic;
SIGNAL \out[125]~output_o\ : std_logic;
SIGNAL \out[126]~output_o\ : std_logic;
SIGNAL \out[127]~output_o\ : std_logic;
SIGNAL \in[24]~input_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \in[15]~input_o\ : std_logic;
SIGNAL \in[16]~input_o\ : std_logic;
SIGNAL \mix3|out2[0]~0_combout\ : std_logic;
SIGNAL \in[7]~input_o\ : std_logic;
SIGNAL \in[8]~input_o\ : std_logic;
SIGNAL \in[17]~input_o\ : std_logic;
SIGNAL \mix3|pd2|modul1|b~0_combout\ : std_logic;
SIGNAL \in[25]~input_o\ : std_logic;
SIGNAL \mix3|out2[1]~1_combout\ : std_logic;
SIGNAL \in[9]~input_o\ : std_logic;
SIGNAL \in[0]~input_o\ : std_logic;
SIGNAL \mix3|pd3|modul1|b~0_combout\ : std_logic;
SIGNAL \in[1]~input_o\ : std_logic;
SIGNAL \in[10]~input_o\ : std_logic;
SIGNAL \in[26]~input_o\ : std_logic;
SIGNAL \in[18]~input_o\ : std_logic;
SIGNAL \mix3|out2[2]~2_combout\ : std_logic;
SIGNAL \in[19]~input_o\ : std_logic;
SIGNAL \mix3|pd2|modul1|b~1_combout\ : std_logic;
SIGNAL \in[27]~input_o\ : std_logic;
SIGNAL \mix3|out2[3]~3_combout\ : std_logic;
SIGNAL \in[2]~input_o\ : std_logic;
SIGNAL \mix3|pd3|modul1|b~1_combout\ : std_logic;
SIGNAL \in[11]~input_o\ : std_logic;
SIGNAL \in[28]~input_o\ : std_logic;
SIGNAL \in[20]~input_o\ : std_logic;
SIGNAL \mix3|pd2|modul1|b~2_combout\ : std_logic;
SIGNAL \mix3|out2[4]~4_combout\ : std_logic;
SIGNAL \in[3]~input_o\ : std_logic;
SIGNAL \mix3|pd3|modul1|b~2_combout\ : std_logic;
SIGNAL \in[12]~input_o\ : std_logic;
SIGNAL \in[29]~input_o\ : std_logic;
SIGNAL \in[21]~input_o\ : std_logic;
SIGNAL \mix3|out2[5]~5_combout\ : std_logic;
SIGNAL \in[13]~input_o\ : std_logic;
SIGNAL \in[4]~input_o\ : std_logic;
SIGNAL \in[5]~input_o\ : std_logic;
SIGNAL \in[22]~input_o\ : std_logic;
SIGNAL \in[30]~input_o\ : std_logic;
SIGNAL \mix3|out2[6]~6_combout\ : std_logic;
SIGNAL \in[14]~input_o\ : std_logic;
SIGNAL \in[6]~input_o\ : std_logic;
SIGNAL \in[23]~input_o\ : std_logic;
SIGNAL \in[31]~input_o\ : std_logic;
SIGNAL \mix3|out2[7]~7_combout\ : std_logic;
SIGNAL \mix3|pd1|modul1|b~0_combout\ : std_logic;
SIGNAL \mix3|pd1|modul1|b~1_combout\ : std_logic;
SIGNAL \mix3|pd1|modul1|b~2_combout\ : std_logic;
SIGNAL \mix3|out0[0]~0_combout\ : std_logic;
SIGNAL \mix3|pd0|modul1|b~0_combout\ : std_logic;
SIGNAL \mix3|out0[1]~1_combout\ : std_logic;
SIGNAL \mix3|out0[2]~2_combout\ : std_logic;
SIGNAL \mix3|pd0|modul1|b~1_combout\ : std_logic;
SIGNAL \mix3|out0[3]~3_combout\ : std_logic;
SIGNAL \mix3|pd0|modul1|b~2_combout\ : std_logic;
SIGNAL \mix3|out0[4]~4_combout\ : std_logic;
SIGNAL \mix3|out0[5]~5_combout\ : std_logic;
SIGNAL \mix3|out0[6]~6_combout\ : std_logic;
SIGNAL \mix3|out0[7]~7_combout\ : std_logic;
SIGNAL \in[56]~input_o\ : std_logic;
SIGNAL \in[47]~input_o\ : std_logic;
SIGNAL \in[48]~input_o\ : std_logic;
SIGNAL \mix2|out2[0]~0_combout\ : std_logic;
SIGNAL \in[39]~input_o\ : std_logic;
SIGNAL \in[40]~input_o\ : std_logic;
SIGNAL \in[32]~input_o\ : std_logic;
SIGNAL \mix2|pd3|modul1|b~0_combout\ : std_logic;
SIGNAL \in[57]~input_o\ : std_logic;
SIGNAL \in[49]~input_o\ : std_logic;
SIGNAL \mix2|pd2|modul1|b~0_combout\ : std_logic;
SIGNAL \mix2|out2[1]~1_combout\ : std_logic;
SIGNAL \in[41]~input_o\ : std_logic;
SIGNAL \in[58]~input_o\ : std_logic;
SIGNAL \in[50]~input_o\ : std_logic;
SIGNAL \mix2|out2[2]~2_combout\ : std_logic;
SIGNAL \in[42]~input_o\ : std_logic;
SIGNAL \in[33]~input_o\ : std_logic;
SIGNAL \in[59]~input_o\ : std_logic;
SIGNAL \in[51]~input_o\ : std_logic;
SIGNAL \mix2|pd2|modul1|b~1_combout\ : std_logic;
SIGNAL \mix2|out2[3]~3_combout\ : std_logic;
SIGNAL \in[34]~input_o\ : std_logic;
SIGNAL \mix2|pd3|modul1|b~1_combout\ : std_logic;
SIGNAL \in[43]~input_o\ : std_logic;
SIGNAL \mix2|pd2|modul1|b~2_combout\ : std_logic;
SIGNAL \in[52]~input_o\ : std_logic;
SIGNAL \in[60]~input_o\ : std_logic;
SIGNAL \mix2|out2[4]~4_combout\ : std_logic;
SIGNAL \in[44]~input_o\ : std_logic;
SIGNAL \in[35]~input_o\ : std_logic;
SIGNAL \mix2|pd3|modul1|b~2_combout\ : std_logic;
SIGNAL \in[36]~input_o\ : std_logic;
SIGNAL \in[45]~input_o\ : std_logic;
SIGNAL \in[61]~input_o\ : std_logic;
SIGNAL \in[53]~input_o\ : std_logic;
SIGNAL \mix2|out2[5]~5_combout\ : std_logic;
SIGNAL \in[37]~input_o\ : std_logic;
SIGNAL \in[54]~input_o\ : std_logic;
SIGNAL \in[62]~input_o\ : std_logic;
SIGNAL \mix2|out2[6]~6_combout\ : std_logic;
SIGNAL \in[46]~input_o\ : std_logic;
SIGNAL \in[55]~input_o\ : std_logic;
SIGNAL \in[63]~input_o\ : std_logic;
SIGNAL \mix2|out2[7]~7_combout\ : std_logic;
SIGNAL \in[38]~input_o\ : std_logic;
SIGNAL \mix2|pd1|modul1|b~0_combout\ : std_logic;
SIGNAL \mix2|pd1|modul1|b~1_combout\ : std_logic;
SIGNAL \mix2|pd1|modul1|b~2_combout\ : std_logic;
SIGNAL \mix2|out0[0]~0_combout\ : std_logic;
SIGNAL \mix2|pd0|modul1|b~0_combout\ : std_logic;
SIGNAL \mix2|out0[1]~1_combout\ : std_logic;
SIGNAL \mix2|out0[2]~2_combout\ : std_logic;
SIGNAL \mix2|pd0|modul1|b~1_combout\ : std_logic;
SIGNAL \mix2|out0[3]~3_combout\ : std_logic;
SIGNAL \mix2|pd0|modul1|b~2_combout\ : std_logic;
SIGNAL \mix2|out0[4]~4_combout\ : std_logic;
SIGNAL \mix2|out0[5]~5_combout\ : std_logic;
SIGNAL \mix2|out0[6]~6_combout\ : std_logic;
SIGNAL \mix2|out0[7]~7_combout\ : std_logic;
SIGNAL \in[80]~input_o\ : std_logic;
SIGNAL \in[79]~input_o\ : std_logic;
SIGNAL \in[88]~input_o\ : std_logic;
SIGNAL \mix1|out2[0]~0_combout\ : std_logic;
SIGNAL \in[71]~input_o\ : std_logic;
SIGNAL \in[72]~input_o\ : std_logic;
SIGNAL \in[64]~input_o\ : std_logic;
SIGNAL \mix1|pd3|modul1|b~0_combout\ : std_logic;
SIGNAL \in[89]~input_o\ : std_logic;
SIGNAL \in[81]~input_o\ : std_logic;
SIGNAL \mix1|pd2|modul1|b~0_combout\ : std_logic;
SIGNAL \mix1|out2[1]~1_combout\ : std_logic;
SIGNAL \in[73]~input_o\ : std_logic;
SIGNAL \in[82]~input_o\ : std_logic;
SIGNAL \in[90]~input_o\ : std_logic;
SIGNAL \mix1|out2[2]~2_combout\ : std_logic;
SIGNAL \in[65]~input_o\ : std_logic;
SIGNAL \in[74]~input_o\ : std_logic;
SIGNAL \in[91]~input_o\ : std_logic;
SIGNAL \in[83]~input_o\ : std_logic;
SIGNAL \mix1|pd2|modul1|b~1_combout\ : std_logic;
SIGNAL \mix1|out2[3]~3_combout\ : std_logic;
SIGNAL \in[75]~input_o\ : std_logic;
SIGNAL \in[66]~input_o\ : std_logic;
SIGNAL \mix1|pd3|modul1|b~1_combout\ : std_logic;
SIGNAL \in[92]~input_o\ : std_logic;
SIGNAL \in[84]~input_o\ : std_logic;
SIGNAL \mix1|pd2|modul1|b~2_combout\ : std_logic;
SIGNAL \mix1|out2[4]~4_combout\ : std_logic;
SIGNAL \in[67]~input_o\ : std_logic;
SIGNAL \mix1|pd3|modul1|b~2_combout\ : std_logic;
SIGNAL \in[76]~input_o\ : std_logic;
SIGNAL \in[68]~input_o\ : std_logic;
SIGNAL \in[77]~input_o\ : std_logic;
SIGNAL \in[85]~input_o\ : std_logic;
SIGNAL \in[93]~input_o\ : std_logic;
SIGNAL \mix1|out2[5]~5_combout\ : std_logic;
SIGNAL \in[94]~input_o\ : std_logic;
SIGNAL \in[86]~input_o\ : std_logic;
SIGNAL \mix1|out2[6]~6_combout\ : std_logic;
SIGNAL \in[78]~input_o\ : std_logic;
SIGNAL \in[69]~input_o\ : std_logic;
SIGNAL \in[95]~input_o\ : std_logic;
SIGNAL \in[87]~input_o\ : std_logic;
SIGNAL \mix1|out2[7]~7_combout\ : std_logic;
SIGNAL \in[70]~input_o\ : std_logic;
SIGNAL \mix1|pd1|modul1|b~0_combout\ : std_logic;
SIGNAL \mix1|pd1|modul1|b~1_combout\ : std_logic;
SIGNAL \mix1|pd1|modul1|b~2_combout\ : std_logic;
SIGNAL \mix1|out0[0]~0_combout\ : std_logic;
SIGNAL \mix1|pd0|modul1|b~0_combout\ : std_logic;
SIGNAL \mix1|out0[1]~1_combout\ : std_logic;
SIGNAL \mix1|out0[2]~2_combout\ : std_logic;
SIGNAL \mix1|pd0|modul1|b~1_combout\ : std_logic;
SIGNAL \mix1|out0[3]~3_combout\ : std_logic;
SIGNAL \mix1|pd0|modul1|b~2_combout\ : std_logic;
SIGNAL \mix1|out0[4]~4_combout\ : std_logic;
SIGNAL \mix1|out0[5]~5_combout\ : std_logic;
SIGNAL \mix1|out0[6]~6_combout\ : std_logic;
SIGNAL \mix1|out0[7]~7_combout\ : std_logic;
SIGNAL \in[112]~input_o\ : std_logic;
SIGNAL \in[111]~input_o\ : std_logic;
SIGNAL \in[120]~input_o\ : std_logic;
SIGNAL \mix0|out2[0]~0_combout\ : std_logic;
SIGNAL \in[104]~input_o\ : std_logic;
SIGNAL \in[103]~input_o\ : std_logic;
SIGNAL \in[105]~input_o\ : std_logic;
SIGNAL \in[113]~input_o\ : std_logic;
SIGNAL \mix0|pd2|modul1|b~0_combout\ : std_logic;
SIGNAL \in[121]~input_o\ : std_logic;
SIGNAL \mix0|out2[1]~1_combout\ : std_logic;
SIGNAL \in[96]~input_o\ : std_logic;
SIGNAL \mix0|pd3|modul1|b~0_combout\ : std_logic;
SIGNAL \in[122]~input_o\ : std_logic;
SIGNAL \in[114]~input_o\ : std_logic;
SIGNAL \mix0|out2[2]~2_combout\ : std_logic;
SIGNAL \in[106]~input_o\ : std_logic;
SIGNAL \in[97]~input_o\ : std_logic;
SIGNAL \in[107]~input_o\ : std_logic;
SIGNAL \in[98]~input_o\ : std_logic;
SIGNAL \mix0|pd3|modul1|b~1_combout\ : std_logic;
SIGNAL \mix0|pd2|modul1|b~1_combout\ : std_logic;
SIGNAL \in[115]~input_o\ : std_logic;
SIGNAL \in[123]~input_o\ : std_logic;
SIGNAL \mix0|out2[3]~3_combout\ : std_logic;
SIGNAL \in[99]~input_o\ : std_logic;
SIGNAL \mix0|pd3|modul1|b~2_combout\ : std_logic;
SIGNAL \in[108]~input_o\ : std_logic;
SIGNAL \mix0|pd2|modul1|b~2_combout\ : std_logic;
SIGNAL \in[116]~input_o\ : std_logic;
SIGNAL \in[124]~input_o\ : std_logic;
SIGNAL \mix0|out2[4]~4_combout\ : std_logic;
SIGNAL \in[117]~input_o\ : std_logic;
SIGNAL \in[125]~input_o\ : std_logic;
SIGNAL \mix0|out2[5]~5_combout\ : std_logic;
SIGNAL \in[109]~input_o\ : std_logic;
SIGNAL \in[100]~input_o\ : std_logic;
SIGNAL \in[101]~input_o\ : std_logic;
SIGNAL \in[110]~input_o\ : std_logic;
SIGNAL \in[118]~input_o\ : std_logic;
SIGNAL \in[126]~input_o\ : std_logic;
SIGNAL \mix0|out2[6]~6_combout\ : std_logic;
SIGNAL \in[127]~input_o\ : std_logic;
SIGNAL \in[119]~input_o\ : std_logic;
SIGNAL \mix0|out2[7]~7_combout\ : std_logic;
SIGNAL \in[102]~input_o\ : std_logic;
SIGNAL \mix0|pd1|modul1|b~0_combout\ : std_logic;
SIGNAL \mix0|pd1|modul1|b~1_combout\ : std_logic;
SIGNAL \mix0|pd1|modul1|b~2_combout\ : std_logic;
SIGNAL \mix0|out0[0]~0_combout\ : std_logic;
SIGNAL \mix0|pd0|modul1|b~0_combout\ : std_logic;
SIGNAL \mix0|out0[1]~1_combout\ : std_logic;
SIGNAL \mix0|out0[2]~2_combout\ : std_logic;
SIGNAL \mix0|pd0|modul1|b~1_combout\ : std_logic;
SIGNAL \mix0|out0[3]~3_combout\ : std_logic;
SIGNAL \mix0|pd0|modul1|b~2_combout\ : std_logic;
SIGNAL \mix0|out0[4]~4_combout\ : std_logic;
SIGNAL \mix0|out0[5]~5_combout\ : std_logic;
SIGNAL \mix0|out0[6]~6_combout\ : std_logic;
SIGNAL \mix0|out0[7]~7_combout\ : std_logic;
SIGNAL \mix0|out3\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|out3\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|out1\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|out2\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|out1\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix0|out0\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix0|pd1|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|out1\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|out2\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|pd2|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|pd1|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|out0\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|out3\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|pd3|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix0|pd3|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix0|out2\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|pd2|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|out0\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|pd2|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix0|pd0|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix0|out1\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|pd3|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix0|pd2|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|pd3|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|out3\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|out2\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|pd0|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|pd1|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix3|out0\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|pd1|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix2|pd0|modul1|b\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \mix1|pd0|modul1|b\ : std_logic_vector(7 DOWNTO 0);

BEGIN

\ww_in\ <= \in\;
\out\ <= \ww_out\;
ww_enable <= enable;
ww_clk <= clk;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);

-- Location: IOOBUF_X48_Y0_N9
\out[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(0),
	devoe => ww_devoe,
	o => \out[0]~output_o\);

-- Location: IOOBUF_X43_Y43_N16
\out[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(1),
	devoe => ww_devoe,
	o => \out[1]~output_o\);

-- Location: IOOBUF_X38_Y43_N9
\out[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(2),
	devoe => ww_devoe,
	o => \out[2]~output_o\);

-- Location: IOOBUF_X52_Y43_N9
\out[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(3),
	devoe => ww_devoe,
	o => \out[3]~output_o\);

-- Location: IOOBUF_X52_Y43_N2
\out[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(4),
	devoe => ww_devoe,
	o => \out[4]~output_o\);

-- Location: IOOBUF_X65_Y43_N30
\out[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(5),
	devoe => ww_devoe,
	o => \out[5]~output_o\);

-- Location: IOOBUF_X63_Y43_N16
\out[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(6),
	devoe => ww_devoe,
	o => \out[6]~output_o\);

-- Location: IOOBUF_X50_Y43_N23
\out[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out3\(7),
	devoe => ww_devoe,
	o => \out[7]~output_o\);

-- Location: IOOBUF_X48_Y43_N30
\out[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(0),
	devoe => ww_devoe,
	o => \out[8]~output_o\);

-- Location: IOOBUF_X41_Y43_N2
\out[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(1),
	devoe => ww_devoe,
	o => \out[9]~output_o\);

-- Location: IOOBUF_X43_Y43_N9
\out[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(2),
	devoe => ww_devoe,
	o => \out[10]~output_o\);

-- Location: IOOBUF_X52_Y43_N30
\out[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(3),
	devoe => ww_devoe,
	o => \out[11]~output_o\);

-- Location: IOOBUF_X56_Y43_N2
\out[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(4),
	devoe => ww_devoe,
	o => \out[12]~output_o\);

-- Location: IOOBUF_X67_Y41_N2
\out[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(5),
	devoe => ww_devoe,
	o => \out[13]~output_o\);

-- Location: IOOBUF_X63_Y43_N23
\out[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(6),
	devoe => ww_devoe,
	o => \out[14]~output_o\);

-- Location: IOOBUF_X50_Y43_N16
\out[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out2\(7),
	devoe => ww_devoe,
	o => \out[15]~output_o\);

-- Location: IOOBUF_X48_Y43_N9
\out[16]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(0),
	devoe => ww_devoe,
	o => \out[16]~output_o\);

-- Location: IOOBUF_X38_Y43_N16
\out[17]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(1),
	devoe => ww_devoe,
	o => \out[17]~output_o\);

-- Location: IOOBUF_X41_Y43_N16
\out[18]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(2),
	devoe => ww_devoe,
	o => \out[18]~output_o\);

-- Location: IOOBUF_X56_Y43_N16
\out[19]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(3),
	devoe => ww_devoe,
	o => \out[19]~output_o\);

-- Location: IOOBUF_X56_Y43_N30
\out[20]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(4),
	devoe => ww_devoe,
	o => \out[20]~output_o\);

-- Location: IOOBUF_X63_Y43_N2
\out[21]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(5),
	devoe => ww_devoe,
	o => \out[21]~output_o\);

-- Location: IOOBUF_X61_Y43_N30
\out[22]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(6),
	devoe => ww_devoe,
	o => \out[22]~output_o\);

-- Location: IOOBUF_X50_Y43_N30
\out[23]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out1\(7),
	devoe => ww_devoe,
	o => \out[23]~output_o\);

-- Location: IOOBUF_X48_Y43_N16
\out[24]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(0),
	devoe => ww_devoe,
	o => \out[24]~output_o\);

-- Location: IOOBUF_X43_Y43_N2
\out[25]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(1),
	devoe => ww_devoe,
	o => \out[25]~output_o\);

-- Location: IOOBUF_X41_Y43_N9
\out[26]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(2),
	devoe => ww_devoe,
	o => \out[26]~output_o\);

-- Location: IOOBUF_X54_Y43_N23
\out[27]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(3),
	devoe => ww_devoe,
	o => \out[27]~output_o\);

-- Location: IOOBUF_X54_Y43_N30
\out[28]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(4),
	devoe => ww_devoe,
	o => \out[28]~output_o\);

-- Location: IOOBUF_X65_Y43_N23
\out[29]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(5),
	devoe => ww_devoe,
	o => \out[29]~output_o\);

-- Location: IOOBUF_X67_Y41_N23
\out[30]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(6),
	devoe => ww_devoe,
	o => \out[30]~output_o\);

-- Location: IOOBUF_X48_Y43_N2
\out[31]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix3|out0\(7),
	devoe => ww_devoe,
	o => \out[31]~output_o\);

-- Location: IOOBUF_X67_Y10_N2
\out[32]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(0),
	devoe => ww_devoe,
	o => \out[32]~output_o\);

-- Location: IOOBUF_X67_Y7_N16
\out[33]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(1),
	devoe => ww_devoe,
	o => \out[33]~output_o\);

-- Location: IOOBUF_X56_Y0_N9
\out[34]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(2),
	devoe => ww_devoe,
	o => \out[34]~output_o\);

-- Location: IOOBUF_X61_Y0_N9
\out[35]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(3),
	devoe => ww_devoe,
	o => \out[35]~output_o\);

-- Location: IOOBUF_X56_Y0_N23
\out[36]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(4),
	devoe => ww_devoe,
	o => \out[36]~output_o\);

-- Location: IOOBUF_X67_Y12_N16
\out[37]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(5),
	devoe => ww_devoe,
	o => \out[37]~output_o\);

-- Location: IOOBUF_X59_Y0_N2
\out[38]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(6),
	devoe => ww_devoe,
	o => \out[38]~output_o\);

-- Location: IOOBUF_X56_Y0_N30
\out[39]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out3\(7),
	devoe => ww_devoe,
	o => \out[39]~output_o\);

-- Location: IOOBUF_X67_Y7_N23
\out[40]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(0),
	devoe => ww_devoe,
	o => \out[40]~output_o\);

-- Location: IOOBUF_X67_Y10_N23
\out[41]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(1),
	devoe => ww_devoe,
	o => \out[41]~output_o\);

-- Location: IOOBUF_X59_Y0_N9
\out[42]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(2),
	devoe => ww_devoe,
	o => \out[42]~output_o\);

-- Location: IOOBUF_X61_Y0_N16
\out[43]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(3),
	devoe => ww_devoe,
	o => \out[43]~output_o\);

-- Location: IOOBUF_X54_Y0_N9
\out[44]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(4),
	devoe => ww_devoe,
	o => \out[44]~output_o\);

-- Location: IOOBUF_X65_Y0_N23
\out[45]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(5),
	devoe => ww_devoe,
	o => \out[45]~output_o\);

-- Location: IOOBUF_X67_Y8_N2
\out[46]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(6),
	devoe => ww_devoe,
	o => \out[46]~output_o\);

-- Location: IOOBUF_X59_Y0_N30
\out[47]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out2\(7),
	devoe => ww_devoe,
	o => \out[47]~output_o\);

-- Location: IOOBUF_X67_Y11_N2
\out[48]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(0),
	devoe => ww_devoe,
	o => \out[48]~output_o\);

-- Location: IOOBUF_X67_Y11_N23
\out[49]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(1),
	devoe => ww_devoe,
	o => \out[49]~output_o\);

-- Location: IOOBUF_X67_Y2_N9
\out[50]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(2),
	devoe => ww_devoe,
	o => \out[50]~output_o\);

-- Location: IOOBUF_X61_Y0_N2
\out[51]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(3),
	devoe => ww_devoe,
	o => \out[51]~output_o\);

-- Location: IOOBUF_X56_Y0_N16
\out[52]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(4),
	devoe => ww_devoe,
	o => \out[52]~output_o\);

-- Location: IOOBUF_X65_Y0_N30
\out[53]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(5),
	devoe => ww_devoe,
	o => \out[53]~output_o\);

-- Location: IOOBUF_X67_Y8_N9
\out[54]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(6),
	devoe => ww_devoe,
	o => \out[54]~output_o\);

-- Location: IOOBUF_X54_Y0_N16
\out[55]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out1\(7),
	devoe => ww_devoe,
	o => \out[55]~output_o\);

-- Location: IOOBUF_X67_Y7_N2
\out[56]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(0),
	devoe => ww_devoe,
	o => \out[56]~output_o\);

-- Location: IOOBUF_X67_Y11_N9
\out[57]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(1),
	devoe => ww_devoe,
	o => \out[57]~output_o\);

-- Location: IOOBUF_X67_Y3_N16
\out[58]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(2),
	devoe => ww_devoe,
	o => \out[58]~output_o\);

-- Location: IOOBUF_X67_Y3_N23
\out[59]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(3),
	devoe => ww_devoe,
	o => \out[59]~output_o\);

-- Location: IOOBUF_X67_Y8_N16
\out[60]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(4),
	devoe => ww_devoe,
	o => \out[60]~output_o\);

-- Location: IOOBUF_X67_Y8_N23
\out[61]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(5),
	devoe => ww_devoe,
	o => \out[61]~output_o\);

-- Location: IOOBUF_X63_Y0_N9
\out[62]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(6),
	devoe => ww_devoe,
	o => \out[62]~output_o\);

-- Location: IOOBUF_X56_Y0_N2
\out[63]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix2|out0\(7),
	devoe => ww_devoe,
	o => \out[63]~output_o\);

-- Location: IOOBUF_X67_Y18_N16
\out[64]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(0),
	devoe => ww_devoe,
	o => \out[64]~output_o\);

-- Location: IOOBUF_X67_Y18_N23
\out[65]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(1),
	devoe => ww_devoe,
	o => \out[65]~output_o\);

-- Location: IOOBUF_X52_Y0_N16
\out[66]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(2),
	devoe => ww_devoe,
	o => \out[66]~output_o\);

-- Location: IOOBUF_X50_Y0_N16
\out[67]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(3),
	devoe => ww_devoe,
	o => \out[67]~output_o\);

-- Location: IOOBUF_X16_Y0_N16
\out[68]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(4),
	devoe => ww_devoe,
	o => \out[68]~output_o\);

-- Location: IOOBUF_X9_Y0_N23
\out[69]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(5),
	devoe => ww_devoe,
	o => \out[69]~output_o\);

-- Location: IOOBUF_X9_Y0_N16
\out[70]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(6),
	devoe => ww_devoe,
	o => \out[70]~output_o\);

-- Location: IOOBUF_X18_Y0_N16
\out[71]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out3\(7),
	devoe => ww_devoe,
	o => \out[71]~output_o\);

-- Location: IOOBUF_X67_Y19_N9
\out[72]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(0),
	devoe => ww_devoe,
	o => \out[72]~output_o\);

-- Location: IOOBUF_X67_Y16_N16
\out[73]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(1),
	devoe => ww_devoe,
	o => \out[73]~output_o\);

-- Location: IOOBUF_X54_Y0_N23
\out[74]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(2),
	devoe => ww_devoe,
	o => \out[74]~output_o\);

-- Location: IOOBUF_X52_Y0_N9
\out[75]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(3),
	devoe => ww_devoe,
	o => \out[75]~output_o\);

-- Location: IOOBUF_X18_Y0_N9
\out[76]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(4),
	devoe => ww_devoe,
	o => \out[76]~output_o\);

-- Location: IOOBUF_X11_Y0_N9
\out[77]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(5),
	devoe => ww_devoe,
	o => \out[77]~output_o\);

-- Location: IOOBUF_X11_Y0_N16
\out[78]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(6),
	devoe => ww_devoe,
	o => \out[78]~output_o\);

-- Location: IOOBUF_X18_Y0_N23
\out[79]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out2\(7),
	devoe => ww_devoe,
	o => \out[79]~output_o\);

-- Location: IOOBUF_X67_Y19_N16
\out[80]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(0),
	devoe => ww_devoe,
	o => \out[80]~output_o\);

-- Location: IOOBUF_X67_Y17_N23
\out[81]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(1),
	devoe => ww_devoe,
	o => \out[81]~output_o\);

-- Location: IOOBUF_X54_Y0_N30
\out[82]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(2),
	devoe => ww_devoe,
	o => \out[82]~output_o\);

-- Location: IOOBUF_X50_Y0_N30
\out[83]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(3),
	devoe => ww_devoe,
	o => \out[83]~output_o\);

-- Location: IOOBUF_X20_Y0_N23
\out[84]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(4),
	devoe => ww_devoe,
	o => \out[84]~output_o\);

-- Location: IOOBUF_X9_Y0_N30
\out[85]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(5),
	devoe => ww_devoe,
	o => \out[85]~output_o\);

-- Location: IOOBUF_X11_Y0_N23
\out[86]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(6),
	devoe => ww_devoe,
	o => \out[86]~output_o\);

-- Location: IOOBUF_X20_Y0_N16
\out[87]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out1\(7),
	devoe => ww_devoe,
	o => \out[87]~output_o\);

-- Location: IOOBUF_X67_Y18_N2
\out[88]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(0),
	devoe => ww_devoe,
	o => \out[88]~output_o\);

-- Location: IOOBUF_X67_Y17_N16
\out[89]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(1),
	devoe => ww_devoe,
	o => \out[89]~output_o\);

-- Location: IOOBUF_X52_Y0_N2
\out[90]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(2),
	devoe => ww_devoe,
	o => \out[90]~output_o\);

-- Location: IOOBUF_X52_Y0_N23
\out[91]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(3),
	devoe => ww_devoe,
	o => \out[91]~output_o\);

-- Location: IOOBUF_X16_Y0_N9
\out[92]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(4),
	devoe => ww_devoe,
	o => \out[92]~output_o\);

-- Location: IOOBUF_X7_Y0_N9
\out[93]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(5),
	devoe => ww_devoe,
	o => \out[93]~output_o\);

-- Location: IOOBUF_X7_Y0_N16
\out[94]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(6),
	devoe => ww_devoe,
	o => \out[94]~output_o\);

-- Location: IOOBUF_X18_Y0_N2
\out[95]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix1|out0\(7),
	devoe => ww_devoe,
	o => \out[95]~output_o\);

-- Location: IOOBUF_X0_Y4_N2
\out[96]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(0),
	devoe => ww_devoe,
	o => \out[96]~output_o\);

-- Location: IOOBUF_X1_Y0_N9
\out[97]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(1),
	devoe => ww_devoe,
	o => \out[97]~output_o\);

-- Location: IOOBUF_X27_Y0_N16
\out[98]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(2),
	devoe => ww_devoe,
	o => \out[98]~output_o\);

-- Location: IOOBUF_X34_Y0_N23
\out[99]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(3),
	devoe => ww_devoe,
	o => \out[99]~output_o\);

-- Location: IOOBUF_X32_Y43_N2
\out[100]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(4),
	devoe => ww_devoe,
	o => \out[100]~output_o\);

-- Location: IOOBUF_X34_Y0_N9
\out[101]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(5),
	devoe => ww_devoe,
	o => \out[101]~output_o\);

-- Location: IOOBUF_X43_Y0_N23
\out[102]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(6),
	devoe => ww_devoe,
	o => \out[102]~output_o\);

-- Location: IOOBUF_X22_Y0_N16
\out[103]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out3\(7),
	devoe => ww_devoe,
	o => \out[103]~output_o\);

-- Location: IOOBUF_X0_Y4_N23
\out[104]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(0),
	devoe => ww_devoe,
	o => \out[104]~output_o\);

-- Location: IOOBUF_X0_Y8_N16
\out[105]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(1),
	devoe => ww_devoe,
	o => \out[105]~output_o\);

-- Location: IOOBUF_X22_Y0_N9
\out[106]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(2),
	devoe => ww_devoe,
	o => \out[106]~output_o\);

-- Location: IOOBUF_X32_Y43_N9
\out[107]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(3),
	devoe => ww_devoe,
	o => \out[107]~output_o\);

-- Location: IOOBUF_X5_Y0_N16
\out[108]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(4),
	devoe => ww_devoe,
	o => \out[108]~output_o\);

-- Location: IOOBUF_X34_Y0_N2
\out[109]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(5),
	devoe => ww_devoe,
	o => \out[109]~output_o\);

-- Location: IOOBUF_X38_Y0_N23
\out[110]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(6),
	devoe => ww_devoe,
	o => \out[110]~output_o\);

-- Location: IOOBUF_X22_Y0_N23
\out[111]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out2\(7),
	devoe => ww_devoe,
	o => \out[111]~output_o\);

-- Location: IOOBUF_X0_Y4_N16
\out[112]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(0),
	devoe => ww_devoe,
	o => \out[112]~output_o\);

-- Location: IOOBUF_X1_Y0_N30
\out[113]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(1),
	devoe => ww_devoe,
	o => \out[113]~output_o\);

-- Location: IOOBUF_X27_Y0_N23
\out[114]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(2),
	devoe => ww_devoe,
	o => \out[114]~output_o\);

-- Location: IOOBUF_X29_Y0_N30
\out[115]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(3),
	devoe => ww_devoe,
	o => \out[115]~output_o\);

-- Location: IOOBUF_X32_Y0_N9
\out[116]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(4),
	devoe => ww_devoe,
	o => \out[116]~output_o\);

-- Location: IOOBUF_X38_Y0_N30
\out[117]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(5),
	devoe => ww_devoe,
	o => \out[117]~output_o\);

-- Location: IOOBUF_X41_Y0_N9
\out[118]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(6),
	devoe => ww_devoe,
	o => \out[118]~output_o\);

-- Location: IOOBUF_X25_Y0_N16
\out[119]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out1\(7),
	devoe => ww_devoe,
	o => \out[119]~output_o\);

-- Location: IOOBUF_X0_Y3_N2
\out[120]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(0),
	devoe => ww_devoe,
	o => \out[120]~output_o\);

-- Location: IOOBUF_X1_Y0_N2
\out[121]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(1),
	devoe => ww_devoe,
	o => \out[121]~output_o\);

-- Location: IOOBUF_X20_Y0_N9
\out[122]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(2),
	devoe => ww_devoe,
	o => \out[122]~output_o\);

-- Location: IOOBUF_X29_Y0_N16
\out[123]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(3),
	devoe => ww_devoe,
	o => \out[123]~output_o\);

-- Location: IOOBUF_X5_Y0_N9
\out[124]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(4),
	devoe => ww_devoe,
	o => \out[124]~output_o\);

-- Location: IOOBUF_X34_Y0_N16
\out[125]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(5),
	devoe => ww_devoe,
	o => \out[125]~output_o\);

-- Location: IOOBUF_X41_Y0_N30
\out[126]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(6),
	devoe => ww_devoe,
	o => \out[126]~output_o\);

-- Location: IOOBUF_X27_Y43_N23
\out[127]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mix0|out0\(7),
	devoe => ww_devoe,
	o => \out[127]~output_o\);

-- Location: IOIBUF_X43_Y43_N22
\in[24]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(24),
	o => \in[24]~input_o\);

-- Location: IOIBUF_X0_Y21_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X45_Y43_N15
\in[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(15),
	o => \in[15]~input_o\);

-- Location: FF_X50_Y42_N19
\mix3|pd2|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[15]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(0));

-- Location: IOIBUF_X50_Y43_N8
\in[16]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(16),
	o => \in[16]~input_o\);

-- Location: LCCOMB_X50_Y42_N18
\mix3|out2[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[0]~0_combout\ = \in[24]~input_o\ $ (\mix3|pd2|modul1|b\(0) $ (\in[16]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[24]~input_o\,
	datac => \mix3|pd2|modul1|b\(0),
	datad => \in[16]~input_o\,
	combout => \mix3|out2[0]~0_combout\);

-- Location: IOIBUF_X54_Y43_N15
\in[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(7),
	o => \in[7]~input_o\);

-- Location: FF_X50_Y42_N9
\mix3|pd3|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[7]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(0));

-- Location: IOIBUF_X43_Y43_N29
\in[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(8),
	o => \in[8]~input_o\);

-- Location: LCCOMB_X50_Y42_N20
\mix3|out3[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(0) = \mix3|out2[0]~0_combout\ $ (\mix3|pd3|modul1|b\(0) $ (\in[8]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|out2[0]~0_combout\,
	datab => \mix3|pd3|modul1|b\(0),
	datac => \in[8]~input_o\,
	combout => \mix3|out3\(0));

-- Location: IOIBUF_X45_Y43_N29
\in[17]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(17),
	o => \in[17]~input_o\);

-- Location: LCCOMB_X45_Y42_N24
\mix3|pd2|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd2|modul1|b~0_combout\ = \in[8]~input_o\ $ (\in[15]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[8]~input_o\,
	datad => \in[15]~input_o\,
	combout => \mix3|pd2|modul1|b~0_combout\);

-- Location: FF_X45_Y42_N25
\mix3|pd2|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd2|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(1));

-- Location: IOIBUF_X45_Y43_N22
\in[25]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(25),
	o => \in[25]~input_o\);

-- Location: LCCOMB_X45_Y42_N18
\mix3|out2[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[1]~1_combout\ = \in[17]~input_o\ $ (\mix3|pd2|modul1|b\(1) $ (\in[25]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[17]~input_o\,
	datab => \mix3|pd2|modul1|b\(1),
	datac => \in[25]~input_o\,
	combout => \mix3|out2[1]~1_combout\);

-- Location: IOIBUF_X38_Y43_N22
\in[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(9),
	o => \in[9]~input_o\);

-- Location: IOIBUF_X50_Y43_N1
\in[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(0),
	o => \in[0]~input_o\);

-- Location: LCCOMB_X50_Y42_N30
\mix3|pd3|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd3|modul1|b~0_combout\ = \in[0]~input_o\ $ (\in[7]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[0]~input_o\,
	datad => \in[7]~input_o\,
	combout => \mix3|pd3|modul1|b~0_combout\);

-- Location: FF_X50_Y42_N31
\mix3|pd3|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd3|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(1));

-- Location: LCCOMB_X45_Y42_N4
\mix3|out3[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(1) = \mix3|out2[1]~1_combout\ $ (\in[9]~input_o\ $ (\mix3|pd3|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|out2[1]~1_combout\,
	datac => \in[9]~input_o\,
	datad => \mix3|pd3|modul1|b\(1),
	combout => \mix3|out3\(1));

-- Location: IOIBUF_X45_Y43_N8
\in[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(1),
	o => \in[1]~input_o\);

-- Location: FF_X45_Y42_N31
\mix3|pd3|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(2));

-- Location: IOIBUF_X38_Y43_N29
\in[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(10),
	o => \in[10]~input_o\);

-- Location: IOIBUF_X45_Y43_N1
\in[26]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(26),
	o => \in[26]~input_o\);

-- Location: FF_X45_Y42_N1
\mix3|pd2|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[9]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(2));

-- Location: IOIBUF_X48_Y43_N22
\in[18]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(18),
	o => \in[18]~input_o\);

-- Location: LCCOMB_X45_Y42_N0
\mix3|out2[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[2]~2_combout\ = \in[26]~input_o\ $ (\mix3|pd2|modul1|b\(2) $ (\in[18]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[26]~input_o\,
	datac => \mix3|pd2|modul1|b\(2),
	datad => \in[18]~input_o\,
	combout => \mix3|out2[2]~2_combout\);

-- Location: LCCOMB_X45_Y42_N2
\mix3|out3[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(2) = \mix3|pd3|modul1|b\(2) $ (\in[10]~input_o\ $ (\mix3|out2[2]~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|pd3|modul1|b\(2),
	datac => \in[10]~input_o\,
	datad => \mix3|out2[2]~2_combout\,
	combout => \mix3|out3\(2));

-- Location: IOIBUF_X56_Y43_N8
\in[19]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(19),
	o => \in[19]~input_o\);

-- Location: LCCOMB_X45_Y42_N20
\mix3|pd2|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd2|modul1|b~1_combout\ = \in[10]~input_o\ $ (\in[15]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[10]~input_o\,
	datad => \in[15]~input_o\,
	combout => \mix3|pd2|modul1|b~1_combout\);

-- Location: FF_X45_Y42_N21
\mix3|pd2|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd2|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(3));

-- Location: IOIBUF_X63_Y43_N8
\in[27]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(27),
	o => \in[27]~input_o\);

-- Location: LCCOMB_X53_Y42_N10
\mix3|out2[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[3]~3_combout\ = \in[19]~input_o\ $ (\mix3|pd2|modul1|b\(3) $ (\in[27]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[19]~input_o\,
	datac => \mix3|pd2|modul1|b\(3),
	datad => \in[27]~input_o\,
	combout => \mix3|out2[3]~3_combout\);

-- Location: IOIBUF_X52_Y43_N22
\in[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(2),
	o => \in[2]~input_o\);

-- Location: LCCOMB_X53_Y42_N24
\mix3|pd3|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd3|modul1|b~1_combout\ = \in[2]~input_o\ $ (\in[7]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[2]~input_o\,
	datac => \in[7]~input_o\,
	combout => \mix3|pd3|modul1|b~1_combout\);

-- Location: FF_X53_Y42_N25
\mix3|pd3|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd3|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(3));

-- Location: IOIBUF_X54_Y43_N1
\in[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(11),
	o => \in[11]~input_o\);

-- Location: LCCOMB_X53_Y42_N4
\mix3|out3[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(3) = \mix3|out2[3]~3_combout\ $ (\mix3|pd3|modul1|b\(3) $ (\in[11]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|out2[3]~3_combout\,
	datab => \mix3|pd3|modul1|b\(3),
	datac => \in[11]~input_o\,
	combout => \mix3|out3\(3));

-- Location: IOIBUF_X61_Y43_N8
\in[28]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(28),
	o => \in[28]~input_o\);

-- Location: IOIBUF_X61_Y43_N1
\in[20]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(20),
	o => \in[20]~input_o\);

-- Location: LCCOMB_X53_Y42_N8
\mix3|pd2|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd2|modul1|b~2_combout\ = \in[15]~input_o\ $ (\in[11]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[15]~input_o\,
	datac => \in[11]~input_o\,
	combout => \mix3|pd2|modul1|b~2_combout\);

-- Location: FF_X53_Y42_N9
\mix3|pd2|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd2|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(4));

-- Location: LCCOMB_X53_Y42_N2
\mix3|out2[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[4]~4_combout\ = \in[28]~input_o\ $ (\in[20]~input_o\ $ (\mix3|pd2|modul1|b\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[28]~input_o\,
	datab => \in[20]~input_o\,
	datac => \mix3|pd2|modul1|b\(4),
	combout => \mix3|out2[4]~4_combout\);

-- Location: IOIBUF_X54_Y43_N8
\in[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(3),
	o => \in[3]~input_o\);

-- Location: LCCOMB_X53_Y42_N30
\mix3|pd3|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd3|modul1|b~2_combout\ = \in[3]~input_o\ $ (\in[7]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[3]~input_o\,
	datac => \in[7]~input_o\,
	combout => \mix3|pd3|modul1|b~2_combout\);

-- Location: FF_X53_Y42_N31
\mix3|pd3|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd3|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(4));

-- Location: IOIBUF_X61_Y43_N15
\in[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(12),
	o => \in[12]~input_o\);

-- Location: LCCOMB_X53_Y42_N28
\mix3|out3[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(4) = \mix3|out2[4]~4_combout\ $ (\mix3|pd3|modul1|b\(4) $ (\in[12]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|out2[4]~4_combout\,
	datac => \mix3|pd3|modul1|b\(4),
	datad => \in[12]~input_o\,
	combout => \mix3|out3\(4));

-- Location: IOIBUF_X61_Y43_N22
\in[29]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(29),
	o => \in[29]~input_o\);

-- Location: IOIBUF_X65_Y43_N8
\in[21]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(21),
	o => \in[21]~input_o\);

-- Location: FF_X61_Y42_N3
\mix3|pd2|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[12]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(5));

-- Location: LCCOMB_X61_Y42_N2
\mix3|out2[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[5]~5_combout\ = \in[29]~input_o\ $ (\in[21]~input_o\ $ (\mix3|pd2|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[29]~input_o\,
	datab => \in[21]~input_o\,
	datac => \mix3|pd2|modul1|b\(5),
	combout => \mix3|out2[5]~5_combout\);

-- Location: IOIBUF_X67_Y41_N8
\in[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(13),
	o => \in[13]~input_o\);

-- Location: IOIBUF_X59_Y43_N1
\in[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(4),
	o => \in[4]~input_o\);

-- Location: FF_X61_Y42_N1
\mix3|pd3|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[4]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(5));

-- Location: LCCOMB_X61_Y42_N12
\mix3|out3[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(5) = \mix3|out2[5]~5_combout\ $ (\in[13]~input_o\ $ (\mix3|pd3|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|out2[5]~5_combout\,
	datac => \in[13]~input_o\,
	datad => \mix3|pd3|modul1|b\(5),
	combout => \mix3|out3\(5));

-- Location: IOIBUF_X65_Y43_N15
\in[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(5),
	o => \in[5]~input_o\);

-- Location: FF_X61_Y42_N31
\mix3|pd3|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[5]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(6));

-- Location: IOIBUF_X59_Y43_N15
\in[22]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(22),
	o => \in[22]~input_o\);

-- Location: IOIBUF_X59_Y43_N8
\in[30]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(30),
	o => \in[30]~input_o\);

-- Location: FF_X61_Y42_N9
\mix3|pd2|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[13]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(6));

-- Location: LCCOMB_X61_Y42_N8
\mix3|out2[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[6]~6_combout\ = \in[22]~input_o\ $ (\in[30]~input_o\ $ (\mix3|pd2|modul1|b\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[22]~input_o\,
	datab => \in[30]~input_o\,
	datac => \mix3|pd2|modul1|b\(6),
	combout => \mix3|out2[6]~6_combout\);

-- Location: IOIBUF_X65_Y43_N1
\in[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(14),
	o => \in[14]~input_o\);

-- Location: LCCOMB_X61_Y42_N18
\mix3|out3[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(6) = \mix3|pd3|modul1|b\(6) $ (\mix3|out2[6]~6_combout\ $ (\in[14]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|pd3|modul1|b\(6),
	datac => \mix3|out2[6]~6_combout\,
	datad => \in[14]~input_o\,
	combout => \mix3|out3\(6));

-- Location: IOIBUF_X56_Y43_N22
\in[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(6),
	o => \in[6]~input_o\);

-- Location: FF_X50_Y42_N17
\mix3|pd3|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[6]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd3|modul1|b\(7));

-- Location: IOIBUF_X52_Y43_N15
\in[23]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(23),
	o => \in[23]~input_o\);

-- Location: FF_X50_Y42_N3
\mix3|pd2|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[14]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd2|modul1|b\(7));

-- Location: IOIBUF_X63_Y43_N29
\in[31]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(31),
	o => \in[31]~input_o\);

-- Location: LCCOMB_X50_Y42_N2
\mix3|out2[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2[7]~7_combout\ = \in[23]~input_o\ $ (\mix3|pd2|modul1|b\(7) $ (\in[31]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[23]~input_o\,
	datac => \mix3|pd2|modul1|b\(7),
	datad => \in[31]~input_o\,
	combout => \mix3|out2[7]~7_combout\);

-- Location: LCCOMB_X50_Y42_N28
\mix3|out3[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out3\(7) = \mix3|pd3|modul1|b\(7) $ (\in[15]~input_o\ $ (\mix3|out2[7]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|pd3|modul1|b\(7),
	datac => \in[15]~input_o\,
	datad => \mix3|out2[7]~7_combout\,
	combout => \mix3|out3\(7));

-- Location: FF_X50_Y42_N23
\mix3|pd1|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[23]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(0));

-- Location: LCCOMB_X50_Y42_N0
\mix3|out2[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(0) = \mix3|out2[0]~0_combout\ $ (\mix3|pd1|modul1|b\(0) $ (\in[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|out2[0]~0_combout\,
	datac => \mix3|pd1|modul1|b\(0),
	datad => \in[0]~input_o\,
	combout => \mix3|out2\(0));

-- Location: LCCOMB_X50_Y42_N10
\mix3|pd1|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd1|modul1|b~0_combout\ = \in[16]~input_o\ $ (\in[23]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[16]~input_o\,
	datad => \in[23]~input_o\,
	combout => \mix3|pd1|modul1|b~0_combout\);

-- Location: FF_X50_Y42_N11
\mix3|pd1|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd1|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(1));

-- Location: LCCOMB_X45_Y42_N6
\mix3|out2[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(1) = \in[1]~input_o\ $ (\mix3|out2[1]~1_combout\ $ (\mix3|pd1|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[1]~input_o\,
	datab => \mix3|out2[1]~1_combout\,
	datac => \mix3|pd1|modul1|b\(1),
	combout => \mix3|out2\(1));

-- Location: FF_X45_Y42_N17
\mix3|pd1|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[17]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(2));

-- Location: LCCOMB_X45_Y42_N26
\mix3|out2[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(2) = \mix3|out2[2]~2_combout\ $ (\mix3|pd1|modul1|b\(2) $ (\in[2]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|out2[2]~2_combout\,
	datac => \mix3|pd1|modul1|b\(2),
	datad => \in[2]~input_o\,
	combout => \mix3|out2\(2));

-- Location: LCCOMB_X53_Y42_N22
\mix3|pd1|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd1|modul1|b~1_combout\ = \in[18]~input_o\ $ (\in[23]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[18]~input_o\,
	datac => \in[23]~input_o\,
	combout => \mix3|pd1|modul1|b~1_combout\);

-- Location: FF_X53_Y42_N23
\mix3|pd1|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd1|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(3));

-- Location: LCCOMB_X53_Y42_N0
\mix3|out2[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(3) = \mix3|out2[3]~3_combout\ $ (\mix3|pd1|modul1|b\(3) $ (\in[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|out2[3]~3_combout\,
	datac => \mix3|pd1|modul1|b\(3),
	datad => \in[3]~input_o\,
	combout => \mix3|out2\(3));

-- Location: LCCOMB_X53_Y42_N18
\mix3|pd1|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd1|modul1|b~2_combout\ = \in[19]~input_o\ $ (\in[23]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[19]~input_o\,
	datac => \in[23]~input_o\,
	combout => \mix3|pd1|modul1|b~2_combout\);

-- Location: FF_X53_Y42_N19
\mix3|pd1|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd1|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(4));

-- Location: LCCOMB_X53_Y42_N12
\mix3|out2[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(4) = \mix3|pd1|modul1|b\(4) $ (\in[4]~input_o\ $ (\mix3|out2[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|pd1|modul1|b\(4),
	datac => \in[4]~input_o\,
	datad => \mix3|out2[4]~4_combout\,
	combout => \mix3|out2\(4));

-- Location: FF_X61_Y42_N21
\mix3|pd1|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[20]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(5));

-- Location: LCCOMB_X61_Y42_N22
\mix3|out2[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(5) = \mix3|pd1|modul1|b\(5) $ (\in[5]~input_o\ $ (\mix3|out2[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|pd1|modul1|b\(5),
	datac => \in[5]~input_o\,
	datad => \mix3|out2[5]~5_combout\,
	combout => \mix3|out2\(5));

-- Location: FF_X61_Y42_N25
\mix3|pd1|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[21]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(6));

-- Location: LCCOMB_X61_Y42_N26
\mix3|out2[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(6) = \mix3|pd1|modul1|b\(6) $ (\mix3|out2[6]~6_combout\ $ (\in[6]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|pd1|modul1|b\(6),
	datac => \mix3|out2[6]~6_combout\,
	datad => \in[6]~input_o\,
	combout => \mix3|out2\(6));

-- Location: FF_X50_Y42_N13
\mix3|pd1|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[22]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd1|modul1|b\(7));

-- Location: LCCOMB_X50_Y42_N14
\mix3|out2[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out2\(7) = \mix3|pd1|modul1|b\(7) $ (\mix3|out2[7]~7_combout\ $ (\in[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|pd1|modul1|b\(7),
	datab => \mix3|out2[7]~7_combout\,
	datad => \in[7]~input_o\,
	combout => \mix3|out2\(7));

-- Location: FF_X50_Y42_N25
\mix3|pd0|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[31]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(0));

-- Location: LCCOMB_X50_Y42_N24
\mix3|out0[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[0]~0_combout\ = \in[8]~input_o\ $ (\mix3|pd0|modul1|b\(0) $ (\in[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[8]~input_o\,
	datac => \mix3|pd0|modul1|b\(0),
	datad => \in[0]~input_o\,
	combout => \mix3|out0[0]~0_combout\);

-- Location: LCCOMB_X50_Y42_N22
\mix3|out1[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(0) = \in[24]~input_o\ $ (\mix3|pd1|modul1|b\(0) $ (\mix3|out0[0]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[24]~input_o\,
	datac => \mix3|pd1|modul1|b\(0),
	datad => \mix3|out0[0]~0_combout\,
	combout => \mix3|out1\(0));

-- Location: LCCOMB_X45_Y42_N12
\mix3|pd0|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd0|modul1|b~0_combout\ = \in[24]~input_o\ $ (\in[31]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[24]~input_o\,
	datac => \in[31]~input_o\,
	combout => \mix3|pd0|modul1|b~0_combout\);

-- Location: FF_X45_Y42_N13
\mix3|pd0|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd0|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(1));

-- Location: LCCOMB_X45_Y42_N22
\mix3|out0[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[1]~1_combout\ = \mix3|pd0|modul1|b\(1) $ (\in[1]~input_o\ $ (\in[9]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|pd0|modul1|b\(1),
	datac => \in[1]~input_o\,
	datad => \in[9]~input_o\,
	combout => \mix3|out0[1]~1_combout\);

-- Location: LCCOMB_X45_Y42_N8
\mix3|out1[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(1) = \in[25]~input_o\ $ (\mix3|pd1|modul1|b\(1) $ (\mix3|out0[1]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[25]~input_o\,
	datab => \mix3|pd1|modul1|b\(1),
	datac => \mix3|out0[1]~1_combout\,
	combout => \mix3|out1\(1));

-- Location: FF_X45_Y42_N11
\mix3|pd0|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[25]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(2));

-- Location: LCCOMB_X45_Y42_N10
\mix3|out0[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[2]~2_combout\ = \in[10]~input_o\ $ (\mix3|pd0|modul1|b\(2) $ (\in[2]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[10]~input_o\,
	datac => \mix3|pd0|modul1|b\(2),
	datad => \in[2]~input_o\,
	combout => \mix3|out0[2]~2_combout\);

-- Location: LCCOMB_X45_Y42_N16
\mix3|out1[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(2) = \mix3|out0[2]~2_combout\ $ (\mix3|pd1|modul1|b\(2) $ (\in[26]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|out0[2]~2_combout\,
	datac => \mix3|pd1|modul1|b\(2),
	datad => \in[26]~input_o\,
	combout => \mix3|out1\(2));

-- Location: LCCOMB_X45_Y42_N28
\mix3|pd0|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd0|modul1|b~1_combout\ = \in[31]~input_o\ $ (\in[26]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[31]~input_o\,
	datad => \in[26]~input_o\,
	combout => \mix3|pd0|modul1|b~1_combout\);

-- Location: FF_X45_Y42_N29
\mix3|pd0|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd0|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(3));

-- Location: LCCOMB_X53_Y42_N14
\mix3|out0[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[3]~3_combout\ = \mix3|pd0|modul1|b\(3) $ (\in[11]~input_o\ $ (\in[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|pd0|modul1|b\(3),
	datac => \in[11]~input_o\,
	datad => \in[3]~input_o\,
	combout => \mix3|out0[3]~3_combout\);

-- Location: LCCOMB_X53_Y42_N16
\mix3|out1[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(3) = \mix3|pd1|modul1|b\(3) $ (\mix3|out0[3]~3_combout\ $ (\in[27]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|pd1|modul1|b\(3),
	datac => \mix3|out0[3]~3_combout\,
	datad => \in[27]~input_o\,
	combout => \mix3|out1\(3));

-- Location: LCCOMB_X61_Y42_N28
\mix3|pd0|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|pd0|modul1|b~2_combout\ = \in[27]~input_o\ $ (\in[31]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[27]~input_o\,
	datad => \in[31]~input_o\,
	combout => \mix3|pd0|modul1|b~2_combout\);

-- Location: FF_X61_Y42_N29
\mix3|pd0|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix3|pd0|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(4));

-- Location: LCCOMB_X61_Y42_N14
\mix3|out0[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[4]~4_combout\ = \in[12]~input_o\ $ (\mix3|pd0|modul1|b\(4) $ (\in[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[12]~input_o\,
	datab => \mix3|pd0|modul1|b\(4),
	datac => \in[4]~input_o\,
	combout => \mix3|out0[4]~4_combout\);

-- Location: LCCOMB_X53_Y42_N26
\mix3|out1[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(4) = \in[28]~input_o\ $ (\mix3|pd1|modul1|b\(4) $ (\mix3|out0[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[28]~input_o\,
	datab => \mix3|pd1|modul1|b\(4),
	datac => \mix3|out0[4]~4_combout\,
	combout => \mix3|out1\(4));

-- Location: FF_X61_Y42_N17
\mix3|pd0|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[28]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(5));

-- Location: LCCOMB_X61_Y42_N16
\mix3|out0[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[5]~5_combout\ = \in[13]~input_o\ $ (\mix3|pd0|modul1|b\(5) $ (\in[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[13]~input_o\,
	datac => \mix3|pd0|modul1|b\(5),
	datad => \in[5]~input_o\,
	combout => \mix3|out0[5]~5_combout\);

-- Location: LCCOMB_X61_Y42_N20
\mix3|out1[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(5) = \in[29]~input_o\ $ (\mix3|pd1|modul1|b\(5) $ (\mix3|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[29]~input_o\,
	datac => \mix3|pd1|modul1|b\(5),
	datad => \mix3|out0[5]~5_combout\,
	combout => \mix3|out1\(5));

-- Location: FF_X61_Y42_N11
\mix3|pd0|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[29]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(6));

-- Location: LCCOMB_X61_Y42_N10
\mix3|out0[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[6]~6_combout\ = \in[6]~input_o\ $ (\mix3|pd0|modul1|b\(6) $ (\in[14]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[6]~input_o\,
	datac => \mix3|pd0|modul1|b\(6),
	datad => \in[14]~input_o\,
	combout => \mix3|out0[6]~6_combout\);

-- Location: LCCOMB_X61_Y42_N24
\mix3|out1[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(6) = \in[30]~input_o\ $ (\mix3|pd1|modul1|b\(6) $ (\mix3|out0[6]~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[30]~input_o\,
	datac => \mix3|pd1|modul1|b\(6),
	datad => \mix3|out0[6]~6_combout\,
	combout => \mix3|out1\(6));

-- Location: FF_X50_Y42_N27
\mix3|pd0|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[30]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix3|pd0|modul1|b\(7));

-- Location: LCCOMB_X50_Y42_N26
\mix3|out0[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0[7]~7_combout\ = \in[15]~input_o\ $ (\mix3|pd0|modul1|b\(7) $ (\in[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[15]~input_o\,
	datac => \mix3|pd0|modul1|b\(7),
	datad => \in[7]~input_o\,
	combout => \mix3|out0[7]~7_combout\);

-- Location: LCCOMB_X50_Y42_N12
\mix3|out1[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out1\(7) = \mix3|out0[7]~7_combout\ $ (\mix3|pd1|modul1|b\(7) $ (\in[31]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|out0[7]~7_combout\,
	datac => \mix3|pd1|modul1|b\(7),
	datad => \in[31]~input_o\,
	combout => \mix3|out1\(7));

-- Location: LCCOMB_X50_Y42_N8
\mix3|out0[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(0) = \mix3|out0[0]~0_combout\ $ (\mix3|pd3|modul1|b\(0) $ (\in[16]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|out0[0]~0_combout\,
	datac => \mix3|pd3|modul1|b\(0),
	datad => \in[16]~input_o\,
	combout => \mix3|out0\(0));

-- Location: LCCOMB_X45_Y42_N14
\mix3|out0[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(1) = \in[17]~input_o\ $ (\mix3|out0[1]~1_combout\ $ (\mix3|pd3|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[17]~input_o\,
	datac => \mix3|out0[1]~1_combout\,
	datad => \mix3|pd3|modul1|b\(1),
	combout => \mix3|out0\(1));

-- Location: LCCOMB_X45_Y42_N30
\mix3|out0[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(2) = \mix3|out0[2]~2_combout\ $ (\mix3|pd3|modul1|b\(2) $ (\in[18]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|out0[2]~2_combout\,
	datac => \mix3|pd3|modul1|b\(2),
	datad => \in[18]~input_o\,
	combout => \mix3|out0\(2));

-- Location: LCCOMB_X53_Y42_N20
\mix3|out0[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(3) = \in[19]~input_o\ $ (\mix3|pd3|modul1|b\(3) $ (\mix3|out0[3]~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[19]~input_o\,
	datab => \mix3|pd3|modul1|b\(3),
	datac => \mix3|out0[3]~3_combout\,
	combout => \mix3|out0\(3));

-- Location: LCCOMB_X53_Y42_N6
\mix3|out0[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(4) = \mix3|pd3|modul1|b\(4) $ (\mix3|out0[4]~4_combout\ $ (\in[20]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix3|pd3|modul1|b\(4),
	datac => \mix3|out0[4]~4_combout\,
	datad => \in[20]~input_o\,
	combout => \mix3|out0\(4));

-- Location: LCCOMB_X61_Y42_N0
\mix3|out0[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(5) = \in[21]~input_o\ $ (\mix3|pd3|modul1|b\(5) $ (\mix3|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[21]~input_o\,
	datac => \mix3|pd3|modul1|b\(5),
	datad => \mix3|out0[5]~5_combout\,
	combout => \mix3|out0\(5));

-- Location: LCCOMB_X61_Y42_N30
\mix3|out0[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(6) = \in[22]~input_o\ $ (\mix3|pd3|modul1|b\(6) $ (\mix3|out0[6]~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[22]~input_o\,
	datac => \mix3|pd3|modul1|b\(6),
	datad => \mix3|out0[6]~6_combout\,
	combout => \mix3|out0\(6));

-- Location: LCCOMB_X50_Y42_N16
\mix3|out0[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix3|out0\(7) = \mix3|out0[7]~7_combout\ $ (\mix3|pd3|modul1|b\(7) $ (\in[23]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix3|out0[7]~7_combout\,
	datac => \mix3|pd3|modul1|b\(7),
	datad => \in[23]~input_o\,
	combout => \mix3|out0\(7));

-- Location: IOIBUF_X67_Y10_N15
\in[56]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(56),
	o => \in[56]~input_o\);

-- Location: IOIBUF_X67_Y6_N15
\in[47]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(47),
	o => \in[47]~input_o\);

-- Location: FF_X66_Y7_N3
\mix2|pd2|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[47]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(0));

-- Location: IOIBUF_X67_Y7_N8
\in[48]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(48),
	o => \in[48]~input_o\);

-- Location: LCCOMB_X66_Y7_N2
\mix2|out2[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[0]~0_combout\ = \in[56]~input_o\ $ (\mix2|pd2|modul1|b\(0) $ (\in[48]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[56]~input_o\,
	datac => \mix2|pd2|modul1|b\(0),
	datad => \in[48]~input_o\,
	combout => \mix2|out2[0]~0_combout\);

-- Location: IOIBUF_X67_Y9_N8
\in[39]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(39),
	o => \in[39]~input_o\);

-- Location: FF_X66_Y7_N9
\mix2|pd3|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[39]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(0));

-- Location: IOIBUF_X67_Y10_N8
\in[40]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(40),
	o => \in[40]~input_o\);

-- Location: LCCOMB_X66_Y7_N28
\mix2|out3[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(0) = \mix2|out2[0]~0_combout\ $ (\mix2|pd3|modul1|b\(0) $ (\in[40]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|out2[0]~0_combout\,
	datac => \mix2|pd3|modul1|b\(0),
	datad => \in[40]~input_o\,
	combout => \mix2|out3\(0));

-- Location: IOIBUF_X67_Y9_N1
\in[32]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(32),
	o => \in[32]~input_o\);

-- Location: LCCOMB_X66_Y7_N6
\mix2|pd3|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd3|modul1|b~0_combout\ = \in[32]~input_o\ $ (\in[39]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[32]~input_o\,
	datac => \in[39]~input_o\,
	combout => \mix2|pd3|modul1|b~0_combout\);

-- Location: FF_X66_Y7_N7
\mix2|pd3|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd3|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(1));

-- Location: IOIBUF_X67_Y9_N15
\in[57]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(57),
	o => \in[57]~input_o\);

-- Location: IOIBUF_X67_Y5_N22
\in[49]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(49),
	o => \in[49]~input_o\);

-- Location: LCCOMB_X66_Y7_N0
\mix2|pd2|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd2|modul1|b~0_combout\ = \in[47]~input_o\ $ (\in[40]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[47]~input_o\,
	datad => \in[40]~input_o\,
	combout => \mix2|pd2|modul1|b~0_combout\);

-- Location: FF_X66_Y7_N1
\mix2|pd2|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd2|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(1));

-- Location: LCCOMB_X66_Y7_N26
\mix2|out2[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[1]~1_combout\ = \in[57]~input_o\ $ (\in[49]~input_o\ $ (\mix2|pd2|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[57]~input_o\,
	datac => \in[49]~input_o\,
	datad => \mix2|pd2|modul1|b\(1),
	combout => \mix2|out2[1]~1_combout\);

-- Location: IOIBUF_X67_Y9_N22
\in[41]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(41),
	o => \in[41]~input_o\);

-- Location: LCCOMB_X66_Y7_N20
\mix2|out3[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(1) = \mix2|pd3|modul1|b\(1) $ (\mix2|out2[1]~1_combout\ $ (\in[41]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|pd3|modul1|b\(1),
	datac => \mix2|out2[1]~1_combout\,
	datad => \in[41]~input_o\,
	combout => \mix2|out3\(1));

-- Location: IOIBUF_X67_Y3_N1
\in[58]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(58),
	o => \in[58]~input_o\);

-- Location: FF_X63_Y3_N27
\mix2|pd2|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[41]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(2));

-- Location: IOIBUF_X67_Y3_N8
\in[50]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(50),
	o => \in[50]~input_o\);

-- Location: LCCOMB_X63_Y3_N26
\mix2|out2[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[2]~2_combout\ = \in[58]~input_o\ $ (\mix2|pd2|modul1|b\(2) $ (\in[50]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[58]~input_o\,
	datac => \mix2|pd2|modul1|b\(2),
	datad => \in[50]~input_o\,
	combout => \mix2|out2[2]~2_combout\);

-- Location: IOIBUF_X59_Y0_N22
\in[42]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(42),
	o => \in[42]~input_o\);

-- Location: IOIBUF_X67_Y6_N22
\in[33]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(33),
	o => \in[33]~input_o\);

-- Location: FF_X63_Y3_N1
\mix2|pd3|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[33]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(2));

-- Location: LCCOMB_X63_Y3_N4
\mix2|out3[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(2) = \mix2|out2[2]~2_combout\ $ (\in[42]~input_o\ $ (\mix2|pd3|modul1|b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out2[2]~2_combout\,
	datab => \in[42]~input_o\,
	datad => \mix2|pd3|modul1|b\(2),
	combout => \mix2|out3\(2));

-- Location: IOIBUF_X63_Y0_N22
\in[59]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(59),
	o => \in[59]~input_o\);

-- Location: IOIBUF_X63_Y0_N29
\in[51]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(51),
	o => \in[51]~input_o\);

-- Location: LCCOMB_X63_Y3_N16
\mix2|pd2|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd2|modul1|b~1_combout\ = \in[47]~input_o\ $ (\in[42]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[47]~input_o\,
	datad => \in[42]~input_o\,
	combout => \mix2|pd2|modul1|b~1_combout\);

-- Location: FF_X63_Y3_N17
\mix2|pd2|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd2|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(3));

-- Location: LCCOMB_X63_Y3_N2
\mix2|out2[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[3]~3_combout\ = \in[59]~input_o\ $ (\in[51]~input_o\ $ (\mix2|pd2|modul1|b\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[59]~input_o\,
	datac => \in[51]~input_o\,
	datad => \mix2|pd2|modul1|b\(3),
	combout => \mix2|out2[3]~3_combout\);

-- Location: IOIBUF_X59_Y0_N15
\in[34]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(34),
	o => \in[34]~input_o\);

-- Location: LCCOMB_X63_Y3_N14
\mix2|pd3|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd3|modul1|b~1_combout\ = \in[34]~input_o\ $ (\in[39]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[34]~input_o\,
	datad => \in[39]~input_o\,
	combout => \mix2|pd3|modul1|b~1_combout\);

-- Location: FF_X63_Y3_N15
\mix2|pd3|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd3|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(3));

-- Location: IOIBUF_X67_Y2_N15
\in[43]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(43),
	o => \in[43]~input_o\);

-- Location: LCCOMB_X63_Y3_N28
\mix2|out3[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(3) = \mix2|out2[3]~3_combout\ $ (\mix2|pd3|modul1|b\(3) $ (\in[43]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|out2[3]~3_combout\,
	datac => \mix2|pd3|modul1|b\(3),
	datad => \in[43]~input_o\,
	combout => \mix2|out3\(3));

-- Location: LCCOMB_X63_Y4_N10
\mix2|pd2|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd2|modul1|b~2_combout\ = \in[47]~input_o\ $ (\in[43]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[47]~input_o\,
	datac => \in[43]~input_o\,
	combout => \mix2|pd2|modul1|b~2_combout\);

-- Location: FF_X63_Y4_N11
\mix2|pd2|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd2|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(4));

-- Location: IOIBUF_X63_Y0_N15
\in[52]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(52),
	o => \in[52]~input_o\);

-- Location: IOIBUF_X67_Y11_N15
\in[60]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(60),
	o => \in[60]~input_o\);

-- Location: LCCOMB_X63_Y4_N12
\mix2|out2[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[4]~4_combout\ = \mix2|pd2|modul1|b\(4) $ (\in[52]~input_o\ $ (\in[60]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|pd2|modul1|b\(4),
	datab => \in[52]~input_o\,
	datad => \in[60]~input_o\,
	combout => \mix2|out2[4]~4_combout\);

-- Location: IOIBUF_X61_Y0_N29
\in[44]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(44),
	o => \in[44]~input_o\);

-- Location: IOIBUF_X63_Y0_N1
\in[35]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(35),
	o => \in[35]~input_o\);

-- Location: LCCOMB_X63_Y4_N8
\mix2|pd3|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd3|modul1|b~2_combout\ = \in[35]~input_o\ $ (\in[39]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[35]~input_o\,
	datad => \in[39]~input_o\,
	combout => \mix2|pd3|modul1|b~2_combout\);

-- Location: FF_X63_Y4_N9
\mix2|pd3|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd3|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(4));

-- Location: LCCOMB_X63_Y4_N14
\mix2|out3[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(4) = \mix2|out2[4]~4_combout\ $ (\in[44]~input_o\ $ (\mix2|pd3|modul1|b\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out2[4]~4_combout\,
	datab => \in[44]~input_o\,
	datac => \mix2|pd3|modul1|b\(4),
	combout => \mix2|out3\(4));

-- Location: IOIBUF_X67_Y4_N22
\in[36]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(36),
	o => \in[36]~input_o\);

-- Location: FF_X64_Y4_N9
\mix2|pd3|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[36]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(5));

-- Location: IOIBUF_X67_Y2_N1
\in[45]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(45),
	o => \in[45]~input_o\);

-- Location: IOIBUF_X65_Y0_N8
\in[61]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(61),
	o => \in[61]~input_o\);

-- Location: FF_X64_Y4_N19
\mix2|pd2|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[44]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(5));

-- Location: IOIBUF_X65_Y0_N1
\in[53]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(53),
	o => \in[53]~input_o\);

-- Location: LCCOMB_X64_Y4_N18
\mix2|out2[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[5]~5_combout\ = \in[61]~input_o\ $ (\mix2|pd2|modul1|b\(5) $ (\in[53]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[61]~input_o\,
	datac => \mix2|pd2|modul1|b\(5),
	datad => \in[53]~input_o\,
	combout => \mix2|out2[5]~5_combout\);

-- Location: LCCOMB_X64_Y4_N20
\mix2|out3[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(5) = \mix2|pd3|modul1|b\(5) $ (\in[45]~input_o\ $ (\mix2|out2[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|pd3|modul1|b\(5),
	datac => \in[45]~input_o\,
	datad => \mix2|out2[5]~5_combout\,
	combout => \mix2|out3\(5));

-- Location: IOIBUF_X65_Y0_N15
\in[37]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(37),
	o => \in[37]~input_o\);

-- Location: FF_X64_Y4_N7
\mix2|pd3|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[37]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(6));

-- Location: IOIBUF_X67_Y2_N22
\in[54]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(54),
	o => \in[54]~input_o\);

-- Location: FF_X64_Y4_N17
\mix2|pd2|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[45]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(6));

-- Location: IOIBUF_X67_Y5_N15
\in[62]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(62),
	o => \in[62]~input_o\);

-- Location: LCCOMB_X64_Y4_N16
\mix2|out2[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[6]~6_combout\ = \in[54]~input_o\ $ (\mix2|pd2|modul1|b\(6) $ (\in[62]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[54]~input_o\,
	datac => \mix2|pd2|modul1|b\(6),
	datad => \in[62]~input_o\,
	combout => \mix2|out2[6]~6_combout\);

-- Location: IOIBUF_X67_Y4_N15
\in[46]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(46),
	o => \in[46]~input_o\);

-- Location: LCCOMB_X64_Y4_N26
\mix2|out3[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(6) = \mix2|pd3|modul1|b\(6) $ (\mix2|out2[6]~6_combout\ $ (\in[46]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|pd3|modul1|b\(6),
	datab => \mix2|out2[6]~6_combout\,
	datac => \in[46]~input_o\,
	combout => \mix2|out3\(6));

-- Location: IOIBUF_X67_Y5_N1
\in[55]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(55),
	o => \in[55]~input_o\);

-- Location: FF_X63_Y4_N3
\mix2|pd2|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[46]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd2|modul1|b\(7));

-- Location: IOIBUF_X67_Y5_N8
\in[63]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(63),
	o => \in[63]~input_o\);

-- Location: LCCOMB_X63_Y4_N2
\mix2|out2[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2[7]~7_combout\ = \in[55]~input_o\ $ (\mix2|pd2|modul1|b\(7) $ (\in[63]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[55]~input_o\,
	datac => \mix2|pd2|modul1|b\(7),
	datad => \in[63]~input_o\,
	combout => \mix2|out2[7]~7_combout\);

-- Location: IOIBUF_X61_Y0_N22
\in[38]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(38),
	o => \in[38]~input_o\);

-- Location: FF_X63_Y4_N17
\mix2|pd3|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[38]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd3|modul1|b\(7));

-- Location: LCCOMB_X63_Y4_N4
\mix2|out3[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out3\(7) = \mix2|out2[7]~7_combout\ $ (\in[47]~input_o\ $ (\mix2|pd3|modul1|b\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|out2[7]~7_combout\,
	datac => \in[47]~input_o\,
	datad => \mix2|pd3|modul1|b\(7),
	combout => \mix2|out3\(7));

-- Location: FF_X66_Y7_N23
\mix2|pd1|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[55]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(0));

-- Location: LCCOMB_X66_Y7_N24
\mix2|out2[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(0) = \mix2|out2[0]~0_combout\ $ (\mix2|pd1|modul1|b\(0) $ (\in[32]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|out2[0]~0_combout\,
	datac => \mix2|pd1|modul1|b\(0),
	datad => \in[32]~input_o\,
	combout => \mix2|out2\(0));

-- Location: LCCOMB_X66_Y7_N18
\mix2|pd1|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd1|modul1|b~0_combout\ = \in[55]~input_o\ $ (\in[48]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[55]~input_o\,
	datad => \in[48]~input_o\,
	combout => \mix2|pd1|modul1|b~0_combout\);

-- Location: FF_X66_Y7_N19
\mix2|pd1|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd1|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(1));

-- Location: LCCOMB_X66_Y7_N12
\mix2|out2[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(1) = \mix2|pd1|modul1|b\(1) $ (\mix2|out2[1]~1_combout\ $ (\in[33]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|pd1|modul1|b\(1),
	datac => \mix2|out2[1]~1_combout\,
	datad => \in[33]~input_o\,
	combout => \mix2|out2\(1));

-- Location: FF_X63_Y3_N31
\mix2|pd1|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[49]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(2));

-- Location: LCCOMB_X63_Y3_N24
\mix2|out2[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(2) = \mix2|out2[2]~2_combout\ $ (\in[34]~input_o\ $ (\mix2|pd1|modul1|b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out2[2]~2_combout\,
	datab => \in[34]~input_o\,
	datac => \mix2|pd1|modul1|b\(2),
	combout => \mix2|out2\(2));

-- Location: LCCOMB_X63_Y3_N18
\mix2|pd1|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd1|modul1|b~1_combout\ = \in[50]~input_o\ $ (\in[55]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[50]~input_o\,
	datad => \in[55]~input_o\,
	combout => \mix2|pd1|modul1|b~1_combout\);

-- Location: FF_X63_Y3_N19
\mix2|pd1|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd1|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(3));

-- Location: LCCOMB_X63_Y3_N12
\mix2|out2[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(3) = \mix2|pd1|modul1|b\(3) $ (\in[35]~input_o\ $ (\mix2|out2[3]~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|pd1|modul1|b\(3),
	datac => \in[35]~input_o\,
	datad => \mix2|out2[3]~3_combout\,
	combout => \mix2|out2\(3));

-- Location: LCCOMB_X63_Y4_N6
\mix2|pd1|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd1|modul1|b~2_combout\ = \in[55]~input_o\ $ (\in[51]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[55]~input_o\,
	datad => \in[51]~input_o\,
	combout => \mix2|pd1|modul1|b~2_combout\);

-- Location: FF_X63_Y4_N7
\mix2|pd1|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd1|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(4));

-- Location: LCCOMB_X63_Y4_N24
\mix2|out2[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(4) = \mix2|pd1|modul1|b\(4) $ (\in[36]~input_o\ $ (\mix2|out2[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|pd1|modul1|b\(4),
	datab => \in[36]~input_o\,
	datad => \mix2|out2[4]~4_combout\,
	combout => \mix2|out2\(4));

-- Location: FF_X64_Y4_N13
\mix2|pd1|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[52]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(5));

-- Location: LCCOMB_X64_Y4_N22
\mix2|out2[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(5) = \mix2|pd1|modul1|b\(5) $ (\in[37]~input_o\ $ (\mix2|out2[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|pd1|modul1|b\(5),
	datac => \in[37]~input_o\,
	datad => \mix2|out2[5]~5_combout\,
	combout => \mix2|out2\(5));

-- Location: FF_X64_Y4_N25
\mix2|pd1|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[53]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(6));

-- Location: LCCOMB_X64_Y4_N10
\mix2|out2[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(6) = \mix2|out2[6]~6_combout\ $ (\in[38]~input_o\ $ (\mix2|pd1|modul1|b\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|out2[6]~6_combout\,
	datac => \in[38]~input_o\,
	datad => \mix2|pd1|modul1|b\(6),
	combout => \mix2|out2\(6));

-- Location: FF_X63_Y4_N19
\mix2|pd1|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[54]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd1|modul1|b\(7));

-- Location: LCCOMB_X63_Y4_N20
\mix2|out2[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out2\(7) = \in[39]~input_o\ $ (\mix2|pd1|modul1|b\(7) $ (\mix2|out2[7]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[39]~input_o\,
	datab => \mix2|pd1|modul1|b\(7),
	datad => \mix2|out2[7]~7_combout\,
	combout => \mix2|out2\(7));

-- Location: FF_X66_Y7_N31
\mix2|pd0|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[63]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(0));

-- Location: LCCOMB_X66_Y7_N30
\mix2|out0[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[0]~0_combout\ = \in[32]~input_o\ $ (\mix2|pd0|modul1|b\(0) $ (\in[40]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[32]~input_o\,
	datac => \mix2|pd0|modul1|b\(0),
	datad => \in[40]~input_o\,
	combout => \mix2|out0[0]~0_combout\);

-- Location: LCCOMB_X66_Y7_N22
\mix2|out1[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(0) = \in[56]~input_o\ $ (\mix2|pd1|modul1|b\(0) $ (\mix2|out0[0]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[56]~input_o\,
	datac => \mix2|pd1|modul1|b\(0),
	datad => \mix2|out0[0]~0_combout\,
	combout => \mix2|out1\(0));

-- Location: LCCOMB_X66_Y7_N16
\mix2|pd0|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd0|modul1|b~0_combout\ = \in[56]~input_o\ $ (\in[63]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[56]~input_o\,
	datac => \in[63]~input_o\,
	combout => \mix2|pd0|modul1|b~0_combout\);

-- Location: FF_X66_Y7_N17
\mix2|pd0|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd0|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(1));

-- Location: LCCOMB_X66_Y7_N10
\mix2|out0[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[1]~1_combout\ = \in[33]~input_o\ $ (\mix2|pd0|modul1|b\(1) $ (\in[41]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[33]~input_o\,
	datab => \mix2|pd0|modul1|b\(1),
	datad => \in[41]~input_o\,
	combout => \mix2|out0[1]~1_combout\);

-- Location: LCCOMB_X66_Y7_N4
\mix2|out1[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(1) = \mix2|out0[1]~1_combout\ $ (\mix2|pd1|modul1|b\(1) $ (\in[57]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[1]~1_combout\,
	datab => \mix2|pd1|modul1|b\(1),
	datac => \in[57]~input_o\,
	combout => \mix2|out1\(1));

-- Location: FF_X63_Y3_N23
\mix2|pd0|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[57]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(2));

-- Location: LCCOMB_X63_Y3_N22
\mix2|out0[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[2]~2_combout\ = \in[34]~input_o\ $ (\mix2|pd0|modul1|b\(2) $ (\in[42]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[34]~input_o\,
	datac => \mix2|pd0|modul1|b\(2),
	datad => \in[42]~input_o\,
	combout => \mix2|out0[2]~2_combout\);

-- Location: LCCOMB_X63_Y3_N30
\mix2|out1[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(2) = \mix2|out0[2]~2_combout\ $ (\mix2|pd1|modul1|b\(2) $ (\in[58]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[2]~2_combout\,
	datac => \mix2|pd1|modul1|b\(2),
	datad => \in[58]~input_o\,
	combout => \mix2|out1\(2));

-- Location: LCCOMB_X63_Y3_N8
\mix2|pd0|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd0|modul1|b~1_combout\ = \in[63]~input_o\ $ (\in[58]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[63]~input_o\,
	datad => \in[58]~input_o\,
	combout => \mix2|pd0|modul1|b~1_combout\);

-- Location: FF_X63_Y3_N9
\mix2|pd0|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd0|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(3));

-- Location: LCCOMB_X63_Y3_N10
\mix2|out0[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[3]~3_combout\ = \mix2|pd0|modul1|b\(3) $ (\in[35]~input_o\ $ (\in[43]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|pd0|modul1|b\(3),
	datac => \in[35]~input_o\,
	datad => \in[43]~input_o\,
	combout => \mix2|out0[3]~3_combout\);

-- Location: LCCOMB_X63_Y3_N20
\mix2|out1[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(3) = \mix2|out0[3]~3_combout\ $ (\in[59]~input_o\ $ (\mix2|pd1|modul1|b\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[3]~3_combout\,
	datab => \in[59]~input_o\,
	datad => \mix2|pd1|modul1|b\(3),
	combout => \mix2|out1\(3));

-- Location: LCCOMB_X63_Y4_N30
\mix2|pd0|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|pd0|modul1|b~2_combout\ = \in[59]~input_o\ $ (\in[63]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[59]~input_o\,
	datad => \in[63]~input_o\,
	combout => \mix2|pd0|modul1|b~2_combout\);

-- Location: FF_X63_Y4_N31
\mix2|pd0|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix2|pd0|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(4));

-- Location: LCCOMB_X63_Y4_N0
\mix2|out0[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[4]~4_combout\ = \in[44]~input_o\ $ (\mix2|pd0|modul1|b\(4) $ (\in[36]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[44]~input_o\,
	datac => \mix2|pd0|modul1|b\(4),
	datad => \in[36]~input_o\,
	combout => \mix2|out0[4]~4_combout\);

-- Location: LCCOMB_X63_Y4_N26
\mix2|out1[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(4) = \mix2|pd1|modul1|b\(4) $ (\mix2|out0[4]~4_combout\ $ (\in[60]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|pd1|modul1|b\(4),
	datab => \mix2|out0[4]~4_combout\,
	datad => \in[60]~input_o\,
	combout => \mix2|out1\(4));

-- Location: FF_X64_Y4_N29
\mix2|pd0|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[60]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(5));

-- Location: LCCOMB_X64_Y4_N28
\mix2|out0[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[5]~5_combout\ = \in[37]~input_o\ $ (\in[45]~input_o\ $ (\mix2|pd0|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[37]~input_o\,
	datab => \in[45]~input_o\,
	datac => \mix2|pd0|modul1|b\(5),
	combout => \mix2|out0[5]~5_combout\);

-- Location: LCCOMB_X64_Y4_N12
\mix2|out1[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(5) = \in[61]~input_o\ $ (\mix2|pd1|modul1|b\(5) $ (\mix2|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[61]~input_o\,
	datac => \mix2|pd1|modul1|b\(5),
	datad => \mix2|out0[5]~5_combout\,
	combout => \mix2|out1\(5));

-- Location: FF_X64_Y4_N31
\mix2|pd0|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[61]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(6));

-- Location: LCCOMB_X64_Y4_N30
\mix2|out0[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[6]~6_combout\ = \in[46]~input_o\ $ (\mix2|pd0|modul1|b\(6) $ (\in[38]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[46]~input_o\,
	datac => \mix2|pd0|modul1|b\(6),
	datad => \in[38]~input_o\,
	combout => \mix2|out0[6]~6_combout\);

-- Location: LCCOMB_X64_Y4_N24
\mix2|out1[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(6) = \mix2|out0[6]~6_combout\ $ (\mix2|pd1|modul1|b\(6) $ (\in[62]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[6]~6_combout\,
	datac => \mix2|pd1|modul1|b\(6),
	datad => \in[62]~input_o\,
	combout => \mix2|out1\(6));

-- Location: FF_X63_Y4_N29
\mix2|pd0|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[62]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix2|pd0|modul1|b\(7));

-- Location: LCCOMB_X63_Y4_N28
\mix2|out0[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0[7]~7_combout\ = \in[47]~input_o\ $ (\mix2|pd0|modul1|b\(7) $ (\in[39]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[47]~input_o\,
	datac => \mix2|pd0|modul1|b\(7),
	datad => \in[39]~input_o\,
	combout => \mix2|out0[7]~7_combout\);

-- Location: LCCOMB_X63_Y4_N18
\mix2|out1[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out1\(7) = \mix2|out0[7]~7_combout\ $ (\mix2|pd1|modul1|b\(7) $ (\in[63]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix2|out0[7]~7_combout\,
	datac => \mix2|pd1|modul1|b\(7),
	datad => \in[63]~input_o\,
	combout => \mix2|out1\(7));

-- Location: LCCOMB_X66_Y7_N8
\mix2|out0[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(0) = \mix2|out0[0]~0_combout\ $ (\mix2|pd3|modul1|b\(0) $ (\in[48]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[0]~0_combout\,
	datac => \mix2|pd3|modul1|b\(0),
	datad => \in[48]~input_o\,
	combout => \mix2|out0\(0));

-- Location: LCCOMB_X66_Y7_N14
\mix2|out0[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(1) = \mix2|out0[1]~1_combout\ $ (\in[49]~input_o\ $ (\mix2|pd3|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[1]~1_combout\,
	datac => \in[49]~input_o\,
	datad => \mix2|pd3|modul1|b\(1),
	combout => \mix2|out0\(1));

-- Location: LCCOMB_X63_Y3_N0
\mix2|out0[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(2) = \mix2|out0[2]~2_combout\ $ (\mix2|pd3|modul1|b\(2) $ (\in[50]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[2]~2_combout\,
	datac => \mix2|pd3|modul1|b\(2),
	datad => \in[50]~input_o\,
	combout => \mix2|out0\(2));

-- Location: LCCOMB_X63_Y3_N6
\mix2|out0[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(3) = \mix2|out0[3]~3_combout\ $ (\mix2|pd3|modul1|b\(3) $ (\in[51]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[3]~3_combout\,
	datab => \mix2|pd3|modul1|b\(3),
	datac => \in[51]~input_o\,
	combout => \mix2|out0\(3));

-- Location: LCCOMB_X63_Y4_N22
\mix2|out0[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(4) = \in[52]~input_o\ $ (\mix2|pd3|modul1|b\(4) $ (\mix2|out0[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[52]~input_o\,
	datac => \mix2|pd3|modul1|b\(4),
	datad => \mix2|out0[4]~4_combout\,
	combout => \mix2|out0\(4));

-- Location: LCCOMB_X64_Y4_N8
\mix2|out0[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(5) = \in[53]~input_o\ $ (\mix2|pd3|modul1|b\(5) $ (\mix2|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[53]~input_o\,
	datac => \mix2|pd3|modul1|b\(5),
	datad => \mix2|out0[5]~5_combout\,
	combout => \mix2|out0\(5));

-- Location: LCCOMB_X64_Y4_N6
\mix2|out0[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(6) = \mix2|out0[6]~6_combout\ $ (\mix2|pd3|modul1|b\(6) $ (\in[54]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix2|out0[6]~6_combout\,
	datac => \mix2|pd3|modul1|b\(6),
	datad => \in[54]~input_o\,
	combout => \mix2|out0\(6));

-- Location: LCCOMB_X63_Y4_N16
\mix2|out0[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix2|out0\(7) = \in[55]~input_o\ $ (\mix2|out0[7]~7_combout\ $ (\mix2|pd3|modul1|b\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[55]~input_o\,
	datab => \mix2|out0[7]~7_combout\,
	datac => \mix2|pd3|modul1|b\(7),
	combout => \mix2|out0\(7));

-- Location: IOIBUF_X67_Y22_N15
\in[80]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(80),
	o => \in[80]~input_o\);

-- Location: IOIBUF_X67_Y14_N8
\in[79]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(79),
	o => \in[79]~input_o\);

-- Location: FF_X66_Y18_N27
\mix1|pd2|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[79]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(0));

-- Location: IOIBUF_X67_Y18_N8
\in[88]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(88),
	o => \in[88]~input_o\);

-- Location: LCCOMB_X66_Y18_N26
\mix1|out2[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[0]~0_combout\ = \in[80]~input_o\ $ (\mix1|pd2|modul1|b\(0) $ (\in[88]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[80]~input_o\,
	datac => \mix1|pd2|modul1|b\(0),
	datad => \in[88]~input_o\,
	combout => \mix1|out2[0]~0_combout\);

-- Location: IOIBUF_X67_Y13_N1
\in[71]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(71),
	o => \in[71]~input_o\);

-- Location: FF_X66_Y18_N9
\mix1|pd3|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[71]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(0));

-- Location: IOIBUF_X67_Y22_N22
\in[72]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(72),
	o => \in[72]~input_o\);

-- Location: LCCOMB_X66_Y18_N28
\mix1|out3[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(0) = \mix1|out2[0]~0_combout\ $ (\mix1|pd3|modul1|b\(0) $ (\in[72]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out2[0]~0_combout\,
	datab => \mix1|pd3|modul1|b\(0),
	datac => \in[72]~input_o\,
	combout => \mix1|out3\(0));

-- Location: IOIBUF_X67_Y19_N22
\in[64]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(64),
	o => \in[64]~input_o\);

-- Location: LCCOMB_X66_Y18_N6
\mix1|pd3|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd3|modul1|b~0_combout\ = \in[71]~input_o\ $ (\in[64]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[71]~input_o\,
	datad => \in[64]~input_o\,
	combout => \mix1|pd3|modul1|b~0_combout\);

-- Location: FF_X66_Y18_N7
\mix1|pd3|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd3|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(1));

-- Location: IOIBUF_X67_Y14_N22
\in[89]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(89),
	o => \in[89]~input_o\);

-- Location: IOIBUF_X67_Y16_N1
\in[81]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(81),
	o => \in[81]~input_o\);

-- Location: LCCOMB_X66_Y18_N16
\mix1|pd2|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd2|modul1|b~0_combout\ = \in[79]~input_o\ $ (\in[72]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[79]~input_o\,
	datac => \in[72]~input_o\,
	combout => \mix1|pd2|modul1|b~0_combout\);

-- Location: FF_X66_Y18_N17
\mix1|pd2|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd2|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(1));

-- Location: LCCOMB_X66_Y18_N2
\mix1|out2[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[1]~1_combout\ = \in[89]~input_o\ $ (\in[81]~input_o\ $ (\mix1|pd2|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[89]~input_o\,
	datac => \in[81]~input_o\,
	datad => \mix1|pd2|modul1|b\(1),
	combout => \mix1|out2[1]~1_combout\);

-- Location: IOIBUF_X67_Y15_N8
\in[73]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(73),
	o => \in[73]~input_o\);

-- Location: LCCOMB_X66_Y18_N12
\mix1|out3[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(1) = \mix1|pd3|modul1|b\(1) $ (\mix1|out2[1]~1_combout\ $ (\in[73]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|pd3|modul1|b\(1),
	datab => \mix1|out2[1]~1_combout\,
	datad => \in[73]~input_o\,
	combout => \mix1|out3\(1));

-- Location: IOIBUF_X36_Y0_N1
\in[82]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(82),
	o => \in[82]~input_o\);

-- Location: IOIBUF_X54_Y0_N1
\in[90]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(90),
	o => \in[90]~input_o\);

-- Location: FF_X53_Y3_N27
\mix1|pd2|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[73]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(2));

-- Location: LCCOMB_X53_Y3_N26
\mix1|out2[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[2]~2_combout\ = \in[82]~input_o\ $ (\in[90]~input_o\ $ (\mix1|pd2|modul1|b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[82]~input_o\,
	datab => \in[90]~input_o\,
	datac => \mix1|pd2|modul1|b\(2),
	combout => \mix1|out2[2]~2_combout\);

-- Location: IOIBUF_X67_Y15_N22
\in[65]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(65),
	o => \in[65]~input_o\);

-- Location: FF_X53_Y3_N9
\mix1|pd3|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[65]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(2));

-- Location: IOIBUF_X36_Y0_N8
\in[74]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(74),
	o => \in[74]~input_o\);

-- Location: LCCOMB_X53_Y3_N20
\mix1|out3[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(2) = \mix1|out2[2]~2_combout\ $ (\mix1|pd3|modul1|b\(2) $ (\in[74]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out2[2]~2_combout\,
	datab => \mix1|pd3|modul1|b\(2),
	datac => \in[74]~input_o\,
	combout => \mix1|out3\(2));

-- Location: IOIBUF_X45_Y0_N22
\in[91]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(91),
	o => \in[91]~input_o\);

-- Location: IOIBUF_X50_Y0_N8
\in[83]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(83),
	o => \in[83]~input_o\);

-- Location: LCCOMB_X53_Y3_N16
\mix1|pd2|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd2|modul1|b~1_combout\ = \in[74]~input_o\ $ (\in[79]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[74]~input_o\,
	datac => \in[79]~input_o\,
	combout => \mix1|pd2|modul1|b~1_combout\);

-- Location: FF_X53_Y3_N17
\mix1|pd2|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd2|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(3));

-- Location: LCCOMB_X53_Y3_N10
\mix1|out2[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[3]~3_combout\ = \in[91]~input_o\ $ (\in[83]~input_o\ $ (\mix1|pd2|modul1|b\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[91]~input_o\,
	datac => \in[83]~input_o\,
	datad => \mix1|pd2|modul1|b\(3),
	combout => \mix1|out2[3]~3_combout\);

-- Location: IOIBUF_X43_Y0_N29
\in[75]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(75),
	o => \in[75]~input_o\);

-- Location: IOIBUF_X52_Y0_N29
\in[66]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(66),
	o => \in[66]~input_o\);

-- Location: LCCOMB_X53_Y3_N6
\mix1|pd3|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd3|modul1|b~1_combout\ = \in[66]~input_o\ $ (\in[71]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[66]~input_o\,
	datac => \in[71]~input_o\,
	combout => \mix1|pd3|modul1|b~1_combout\);

-- Location: FF_X53_Y3_N7
\mix1|pd3|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd3|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(3));

-- Location: LCCOMB_X53_Y3_N4
\mix1|out3[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(3) = \mix1|out2[3]~3_combout\ $ (\in[75]~input_o\ $ (\mix1|pd3|modul1|b\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out2[3]~3_combout\,
	datab => \in[75]~input_o\,
	datad => \mix1|pd3|modul1|b\(3),
	combout => \mix1|out3\(3));

-- Location: IOIBUF_X14_Y0_N29
\in[92]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(92),
	o => \in[92]~input_o\);

-- Location: IOIBUF_X14_Y0_N22
\in[84]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(84),
	o => \in[84]~input_o\);

-- Location: LCCOMB_X20_Y1_N2
\mix1|pd2|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd2|modul1|b~2_combout\ = \in[75]~input_o\ $ (\in[79]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[75]~input_o\,
	datad => \in[79]~input_o\,
	combout => \mix1|pd2|modul1|b~2_combout\);

-- Location: FF_X20_Y1_N3
\mix1|pd2|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd2|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(4));

-- Location: LCCOMB_X20_Y1_N4
\mix1|out2[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[4]~4_combout\ = \in[92]~input_o\ $ (\in[84]~input_o\ $ (\mix1|pd2|modul1|b\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[92]~input_o\,
	datac => \in[84]~input_o\,
	datad => \mix1|pd2|modul1|b\(4),
	combout => \mix1|out2[4]~4_combout\);

-- Location: IOIBUF_X45_Y0_N1
\in[67]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(67),
	o => \in[67]~input_o\);

-- Location: LCCOMB_X20_Y1_N8
\mix1|pd3|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd3|modul1|b~2_combout\ = \in[67]~input_o\ $ (\in[71]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[67]~input_o\,
	datad => \in[71]~input_o\,
	combout => \mix1|pd3|modul1|b~2_combout\);

-- Location: FF_X20_Y1_N9
\mix1|pd3|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd3|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(4));

-- Location: IOIBUF_X16_Y0_N1
\in[76]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(76),
	o => \in[76]~input_o\);

-- Location: LCCOMB_X20_Y1_N14
\mix1|out3[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(4) = \mix1|out2[4]~4_combout\ $ (\mix1|pd3|modul1|b\(4) $ (\in[76]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|out2[4]~4_combout\,
	datac => \mix1|pd3|modul1|b\(4),
	datad => \in[76]~input_o\,
	combout => \mix1|out3\(4));

-- Location: IOIBUF_X14_Y0_N8
\in[68]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(68),
	o => \in[68]~input_o\);

-- Location: FF_X12_Y1_N9
\mix1|pd3|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[68]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(5));

-- Location: IOIBUF_X7_Y0_N29
\in[77]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(77),
	o => \in[77]~input_o\);

-- Location: IOIBUF_X7_Y0_N22
\in[85]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(85),
	o => \in[85]~input_o\);

-- Location: IOIBUF_X9_Y0_N8
\in[93]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(93),
	o => \in[93]~input_o\);

-- Location: FF_X12_Y1_N19
\mix1|pd2|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[76]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(5));

-- Location: LCCOMB_X12_Y1_N18
\mix1|out2[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[5]~5_combout\ = \in[85]~input_o\ $ (\in[93]~input_o\ $ (\mix1|pd2|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[85]~input_o\,
	datab => \in[93]~input_o\,
	datac => \mix1|pd2|modul1|b\(5),
	combout => \mix1|out2[5]~5_combout\);

-- Location: LCCOMB_X12_Y1_N20
\mix1|out3[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(5) = \mix1|pd3|modul1|b\(5) $ (\in[77]~input_o\ $ (\mix1|out2[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|pd3|modul1|b\(5),
	datac => \in[77]~input_o\,
	datad => \mix1|out2[5]~5_combout\,
	combout => \mix1|out3\(5));

-- Location: IOIBUF_X14_Y0_N15
\in[94]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(94),
	o => \in[94]~input_o\);

-- Location: FF_X12_Y1_N25
\mix1|pd2|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[77]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(6));

-- Location: IOIBUF_X16_Y0_N22
\in[86]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(86),
	o => \in[86]~input_o\);

-- Location: LCCOMB_X12_Y1_N24
\mix1|out2[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[6]~6_combout\ = \in[94]~input_o\ $ (\mix1|pd2|modul1|b\(6) $ (\in[86]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[94]~input_o\,
	datac => \mix1|pd2|modul1|b\(6),
	datad => \in[86]~input_o\,
	combout => \mix1|out2[6]~6_combout\);

-- Location: IOIBUF_X11_Y0_N29
\in[78]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(78),
	o => \in[78]~input_o\);

-- Location: IOIBUF_X5_Y0_N22
\in[69]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(69),
	o => \in[69]~input_o\);

-- Location: FF_X12_Y1_N7
\mix1|pd3|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[69]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(6));

-- Location: LCCOMB_X12_Y1_N26
\mix1|out3[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(6) = \mix1|out2[6]~6_combout\ $ (\in[78]~input_o\ $ (\mix1|pd3|modul1|b\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|out2[6]~6_combout\,
	datac => \in[78]~input_o\,
	datad => \mix1|pd3|modul1|b\(6),
	combout => \mix1|out3\(6));

-- Location: IOIBUF_X67_Y13_N22
\in[95]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(95),
	o => \in[95]~input_o\);

-- Location: FF_X20_Y1_N11
\mix1|pd2|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[78]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd2|modul1|b\(7));

-- Location: IOIBUF_X67_Y15_N15
\in[87]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(87),
	o => \in[87]~input_o\);

-- Location: LCCOMB_X20_Y1_N10
\mix1|out2[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2[7]~7_combout\ = \in[95]~input_o\ $ (\mix1|pd2|modul1|b\(7) $ (\in[87]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[95]~input_o\,
	datac => \mix1|pd2|modul1|b\(7),
	datad => \in[87]~input_o\,
	combout => \mix1|out2[7]~7_combout\);

-- Location: IOIBUF_X3_Y0_N1
\in[70]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(70),
	o => \in[70]~input_o\);

-- Location: FF_X20_Y1_N17
\mix1|pd3|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[70]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd3|modul1|b\(7));

-- Location: LCCOMB_X20_Y1_N28
\mix1|out3[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out3\(7) = \mix1|out2[7]~7_combout\ $ (\mix1|pd3|modul1|b\(7) $ (\in[79]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out2[7]~7_combout\,
	datab => \mix1|pd3|modul1|b\(7),
	datad => \in[79]~input_o\,
	combout => \mix1|out3\(7));

-- Location: FF_X66_Y18_N15
\mix1|pd1|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[87]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(0));

-- Location: LCCOMB_X66_Y18_N0
\mix1|out2[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(0) = \mix1|out2[0]~0_combout\ $ (\mix1|pd1|modul1|b\(0) $ (\in[64]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out2[0]~0_combout\,
	datac => \mix1|pd1|modul1|b\(0),
	datad => \in[64]~input_o\,
	combout => \mix1|out2\(0));

-- Location: LCCOMB_X66_Y18_N18
\mix1|pd1|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd1|modul1|b~0_combout\ = \in[80]~input_o\ $ (\in[87]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[80]~input_o\,
	datac => \in[87]~input_o\,
	combout => \mix1|pd1|modul1|b~0_combout\);

-- Location: FF_X66_Y18_N19
\mix1|pd1|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd1|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(1));

-- Location: LCCOMB_X66_Y18_N20
\mix1|out2[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(1) = \mix1|pd1|modul1|b\(1) $ (\in[65]~input_o\ $ (\mix1|out2[1]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|pd1|modul1|b\(1),
	datac => \in[65]~input_o\,
	datad => \mix1|out2[1]~1_combout\,
	combout => \mix1|out2\(1));

-- Location: FF_X53_Y3_N31
\mix1|pd1|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[81]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(2));

-- Location: LCCOMB_X53_Y3_N24
\mix1|out2[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(2) = \mix1|out2[2]~2_combout\ $ (\in[66]~input_o\ $ (\mix1|pd1|modul1|b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out2[2]~2_combout\,
	datab => \in[66]~input_o\,
	datac => \mix1|pd1|modul1|b\(2),
	combout => \mix1|out2\(2));

-- Location: LCCOMB_X53_Y3_N18
\mix1|pd1|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd1|modul1|b~1_combout\ = \in[82]~input_o\ $ (\in[87]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[82]~input_o\,
	datad => \in[87]~input_o\,
	combout => \mix1|pd1|modul1|b~1_combout\);

-- Location: FF_X53_Y3_N19
\mix1|pd1|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd1|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(3));

-- Location: LCCOMB_X53_Y3_N12
\mix1|out2[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(3) = \mix1|pd1|modul1|b\(3) $ (\in[67]~input_o\ $ (\mix1|out2[3]~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|pd1|modul1|b\(3),
	datac => \in[67]~input_o\,
	datad => \mix1|out2[3]~3_combout\,
	combout => \mix1|out2\(3));

-- Location: LCCOMB_X20_Y1_N30
\mix1|pd1|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd1|modul1|b~2_combout\ = \in[83]~input_o\ $ (\in[87]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[83]~input_o\,
	datad => \in[87]~input_o\,
	combout => \mix1|pd1|modul1|b~2_combout\);

-- Location: FF_X20_Y1_N31
\mix1|pd1|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd1|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(4));

-- Location: LCCOMB_X20_Y1_N24
\mix1|out2[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(4) = \mix1|pd1|modul1|b\(4) $ (\mix1|out2[4]~4_combout\ $ (\in[68]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|pd1|modul1|b\(4),
	datab => \mix1|out2[4]~4_combout\,
	datac => \in[68]~input_o\,
	combout => \mix1|out2\(4));

-- Location: FF_X12_Y1_N13
\mix1|pd1|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[84]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(5));

-- Location: LCCOMB_X12_Y1_N30
\mix1|out2[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(5) = \mix1|out2[5]~5_combout\ $ (\in[69]~input_o\ $ (\mix1|pd1|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|out2[5]~5_combout\,
	datac => \in[69]~input_o\,
	datad => \mix1|pd1|modul1|b\(5),
	combout => \mix1|out2\(5));

-- Location: FF_X12_Y1_N17
\mix1|pd1|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[85]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(6));

-- Location: LCCOMB_X12_Y1_N10
\mix1|out2[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(6) = \mix1|out2[6]~6_combout\ $ (\in[70]~input_o\ $ (\mix1|pd1|modul1|b\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|out2[6]~6_combout\,
	datac => \in[70]~input_o\,
	datad => \mix1|pd1|modul1|b\(6),
	combout => \mix1|out2\(6));

-- Location: FF_X20_Y1_N19
\mix1|pd1|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[86]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd1|modul1|b\(7));

-- Location: LCCOMB_X20_Y1_N20
\mix1|out2[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out2\(7) = \mix1|out2[7]~7_combout\ $ (\mix1|pd1|modul1|b\(7) $ (\in[71]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out2[7]~7_combout\,
	datab => \mix1|pd1|modul1|b\(7),
	datad => \in[71]~input_o\,
	combout => \mix1|out2\(7));

-- Location: FF_X66_Y18_N23
\mix1|pd0|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[95]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(0));

-- Location: LCCOMB_X66_Y18_N22
\mix1|out0[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[0]~0_combout\ = \in[64]~input_o\ $ (\in[72]~input_o\ $ (\mix1|pd0|modul1|b\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[64]~input_o\,
	datab => \in[72]~input_o\,
	datac => \mix1|pd0|modul1|b\(0),
	combout => \mix1|out0[0]~0_combout\);

-- Location: LCCOMB_X66_Y18_N14
\mix1|out1[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(0) = \mix1|out0[0]~0_combout\ $ (\mix1|pd1|modul1|b\(0) $ (\in[88]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out0[0]~0_combout\,
	datac => \mix1|pd1|modul1|b\(0),
	datad => \in[88]~input_o\,
	combout => \mix1|out1\(0));

-- Location: LCCOMB_X66_Y18_N24
\mix1|pd0|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd0|modul1|b~0_combout\ = \in[95]~input_o\ $ (\in[88]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[95]~input_o\,
	datad => \in[88]~input_o\,
	combout => \mix1|pd0|modul1|b~0_combout\);

-- Location: FF_X66_Y18_N25
\mix1|pd0|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd0|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(1));

-- Location: LCCOMB_X66_Y18_N10
\mix1|out0[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[1]~1_combout\ = \in[73]~input_o\ $ (\mix1|pd0|modul1|b\(1) $ (\in[65]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[73]~input_o\,
	datab => \mix1|pd0|modul1|b\(1),
	datac => \in[65]~input_o\,
	combout => \mix1|out0[1]~1_combout\);

-- Location: LCCOMB_X66_Y18_N4
\mix1|out1[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(1) = \mix1|out0[1]~1_combout\ $ (\mix1|pd1|modul1|b\(1) $ (\in[89]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out0[1]~1_combout\,
	datab => \mix1|pd1|modul1|b\(1),
	datac => \in[89]~input_o\,
	combout => \mix1|out1\(1));

-- Location: FF_X53_Y3_N15
\mix1|pd0|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[89]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(2));

-- Location: LCCOMB_X53_Y3_N14
\mix1|out0[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[2]~2_combout\ = \in[74]~input_o\ $ (\mix1|pd0|modul1|b\(2) $ (\in[66]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[74]~input_o\,
	datac => \mix1|pd0|modul1|b\(2),
	datad => \in[66]~input_o\,
	combout => \mix1|out0[2]~2_combout\);

-- Location: LCCOMB_X53_Y3_N30
\mix1|out1[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(2) = \in[90]~input_o\ $ (\mix1|pd1|modul1|b\(2) $ (\mix1|out0[2]~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[90]~input_o\,
	datac => \mix1|pd1|modul1|b\(2),
	datad => \mix1|out0[2]~2_combout\,
	combout => \mix1|out1\(2));

-- Location: LCCOMB_X53_Y3_N0
\mix1|pd0|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd0|modul1|b~1_combout\ = \in[90]~input_o\ $ (\in[95]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[90]~input_o\,
	datad => \in[95]~input_o\,
	combout => \mix1|pd0|modul1|b~1_combout\);

-- Location: FF_X53_Y3_N1
\mix1|pd0|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd0|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(3));

-- Location: LCCOMB_X53_Y3_N2
\mix1|out0[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[3]~3_combout\ = \in[75]~input_o\ $ (\in[67]~input_o\ $ (\mix1|pd0|modul1|b\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[75]~input_o\,
	datac => \in[67]~input_o\,
	datad => \mix1|pd0|modul1|b\(3),
	combout => \mix1|out0[3]~3_combout\);

-- Location: LCCOMB_X53_Y3_N28
\mix1|out1[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(3) = \in[91]~input_o\ $ (\mix1|pd1|modul1|b\(3) $ (\mix1|out0[3]~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[91]~input_o\,
	datab => \mix1|pd1|modul1|b\(3),
	datad => \mix1|out0[3]~3_combout\,
	combout => \mix1|out1\(3));

-- Location: LCCOMB_X20_Y1_N6
\mix1|pd0|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|pd0|modul1|b~2_combout\ = \in[91]~input_o\ $ (\in[95]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[91]~input_o\,
	datad => \in[95]~input_o\,
	combout => \mix1|pd0|modul1|b~2_combout\);

-- Location: FF_X20_Y1_N7
\mix1|pd0|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix1|pd0|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(4));

-- Location: LCCOMB_X20_Y1_N0
\mix1|out0[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[4]~4_combout\ = \in[68]~input_o\ $ (\in[76]~input_o\ $ (\mix1|pd0|modul1|b\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[68]~input_o\,
	datab => \in[76]~input_o\,
	datad => \mix1|pd0|modul1|b\(4),
	combout => \mix1|out0[4]~4_combout\);

-- Location: LCCOMB_X20_Y1_N26
\mix1|out1[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(4) = \in[92]~input_o\ $ (\mix1|pd1|modul1|b\(4) $ (\mix1|out0[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[92]~input_o\,
	datac => \mix1|pd1|modul1|b\(4),
	datad => \mix1|out0[4]~4_combout\,
	combout => \mix1|out1\(4));

-- Location: FF_X12_Y1_N29
\mix1|pd0|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[92]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(5));

-- Location: LCCOMB_X12_Y1_N28
\mix1|out0[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[5]~5_combout\ = \in[77]~input_o\ $ (\mix1|pd0|modul1|b\(5) $ (\in[69]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[77]~input_o\,
	datac => \mix1|pd0|modul1|b\(5),
	datad => \in[69]~input_o\,
	combout => \mix1|out0[5]~5_combout\);

-- Location: LCCOMB_X12_Y1_N12
\mix1|out1[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(5) = \in[93]~input_o\ $ (\mix1|pd1|modul1|b\(5) $ (\mix1|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[93]~input_o\,
	datac => \mix1|pd1|modul1|b\(5),
	datad => \mix1|out0[5]~5_combout\,
	combout => \mix1|out1\(5));

-- Location: FF_X12_Y1_N23
\mix1|pd0|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[93]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(6));

-- Location: LCCOMB_X12_Y1_N22
\mix1|out0[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[6]~6_combout\ = \in[70]~input_o\ $ (\in[78]~input_o\ $ (\mix1|pd0|modul1|b\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[70]~input_o\,
	datab => \in[78]~input_o\,
	datac => \mix1|pd0|modul1|b\(6),
	combout => \mix1|out0[6]~6_combout\);

-- Location: LCCOMB_X12_Y1_N16
\mix1|out1[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(6) = \mix1|out0[6]~6_combout\ $ (\mix1|pd1|modul1|b\(6) $ (\in[94]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out0[6]~6_combout\,
	datac => \mix1|pd1|modul1|b\(6),
	datad => \in[94]~input_o\,
	combout => \mix1|out1\(6));

-- Location: FF_X20_Y1_N13
\mix1|pd0|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[94]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix1|pd0|modul1|b\(7));

-- Location: LCCOMB_X20_Y1_N12
\mix1|out0[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0[7]~7_combout\ = \in[71]~input_o\ $ (\mix1|pd0|modul1|b\(7) $ (\in[79]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[71]~input_o\,
	datac => \mix1|pd0|modul1|b\(7),
	datad => \in[79]~input_o\,
	combout => \mix1|out0[7]~7_combout\);

-- Location: LCCOMB_X20_Y1_N18
\mix1|out1[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out1\(7) = \mix1|out0[7]~7_combout\ $ (\mix1|pd1|modul1|b\(7) $ (\in[95]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out0[7]~7_combout\,
	datac => \mix1|pd1|modul1|b\(7),
	datad => \in[95]~input_o\,
	combout => \mix1|out1\(7));

-- Location: LCCOMB_X66_Y18_N8
\mix1|out0[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(0) = \in[80]~input_o\ $ (\mix1|pd3|modul1|b\(0) $ (\mix1|out0[0]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[80]~input_o\,
	datac => \mix1|pd3|modul1|b\(0),
	datad => \mix1|out0[0]~0_combout\,
	combout => \mix1|out0\(0));

-- Location: LCCOMB_X66_Y18_N30
\mix1|out0[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(1) = \mix1|out0[1]~1_combout\ $ (\in[81]~input_o\ $ (\mix1|pd3|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out0[1]~1_combout\,
	datac => \in[81]~input_o\,
	datad => \mix1|pd3|modul1|b\(1),
	combout => \mix1|out0\(1));

-- Location: LCCOMB_X53_Y3_N8
\mix1|out0[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(2) = \in[82]~input_o\ $ (\mix1|out0[2]~2_combout\ $ (\mix1|pd3|modul1|b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[82]~input_o\,
	datab => \mix1|out0[2]~2_combout\,
	datac => \mix1|pd3|modul1|b\(2),
	combout => \mix1|out0\(2));

-- Location: LCCOMB_X53_Y3_N22
\mix1|out0[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(3) = \mix1|pd3|modul1|b\(3) $ (\in[83]~input_o\ $ (\mix1|out0[3]~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|pd3|modul1|b\(3),
	datac => \in[83]~input_o\,
	datad => \mix1|out0[3]~3_combout\,
	combout => \mix1|out0\(3));

-- Location: LCCOMB_X20_Y1_N22
\mix1|out0[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(4) = \mix1|pd3|modul1|b\(4) $ (\in[84]~input_o\ $ (\mix1|out0[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix1|pd3|modul1|b\(4),
	datac => \in[84]~input_o\,
	datad => \mix1|out0[4]~4_combout\,
	combout => \mix1|out0\(4));

-- Location: LCCOMB_X12_Y1_N8
\mix1|out0[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(5) = \in[85]~input_o\ $ (\mix1|pd3|modul1|b\(5) $ (\mix1|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[85]~input_o\,
	datac => \mix1|pd3|modul1|b\(5),
	datad => \mix1|out0[5]~5_combout\,
	combout => \mix1|out0\(5));

-- Location: LCCOMB_X12_Y1_N6
\mix1|out0[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(6) = \mix1|out0[6]~6_combout\ $ (\mix1|pd3|modul1|b\(6) $ (\in[86]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out0[6]~6_combout\,
	datac => \mix1|pd3|modul1|b\(6),
	datad => \in[86]~input_o\,
	combout => \mix1|out0\(6));

-- Location: LCCOMB_X20_Y1_N16
\mix1|out0[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix1|out0\(7) = \mix1|out0[7]~7_combout\ $ (\mix1|pd3|modul1|b\(7) $ (\in[87]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix1|out0[7]~7_combout\,
	datac => \mix1|pd3|modul1|b\(7),
	datad => \in[87]~input_o\,
	combout => \mix1|out0\(7));

-- Location: IOIBUF_X1_Y0_N22
\in[112]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(112),
	o => \in[112]~input_o\);

-- Location: IOIBUF_X3_Y0_N22
\in[111]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(111),
	o => \in[111]~input_o\);

-- Location: FF_X2_Y1_N3
\mix0|pd2|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[111]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(0));

-- Location: IOIBUF_X3_Y0_N8
\in[120]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(120),
	o => \in[120]~input_o\);

-- Location: LCCOMB_X2_Y1_N2
\mix0|out2[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[0]~0_combout\ = \in[112]~input_o\ $ (\mix0|pd2|modul1|b\(0) $ (\in[120]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[112]~input_o\,
	datac => \mix0|pd2|modul1|b\(0),
	datad => \in[120]~input_o\,
	combout => \mix0|out2[0]~0_combout\);

-- Location: IOIBUF_X0_Y5_N22
\in[104]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(104),
	o => \in[104]~input_o\);

-- Location: IOIBUF_X3_Y0_N29
\in[103]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(103),
	o => \in[103]~input_o\);

-- Location: FF_X2_Y1_N17
\mix0|pd3|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[103]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(0));

-- Location: LCCOMB_X2_Y1_N12
\mix0|out3[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(0) = \mix0|out2[0]~0_combout\ $ (\in[104]~input_o\ $ (\mix0|pd3|modul1|b\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix0|out2[0]~0_combout\,
	datac => \in[104]~input_o\,
	datad => \mix0|pd3|modul1|b\(0),
	combout => \mix0|out3\(0));

-- Location: IOIBUF_X0_Y2_N1
\in[105]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(105),
	o => \in[105]~input_o\);

-- Location: IOIBUF_X0_Y2_N22
\in[113]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(113),
	o => \in[113]~input_o\);

-- Location: LCCOMB_X2_Y1_N8
\mix0|pd2|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd2|modul1|b~0_combout\ = \in[111]~input_o\ $ (\in[104]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[111]~input_o\,
	datac => \in[104]~input_o\,
	combout => \mix0|pd2|modul1|b~0_combout\);

-- Location: FF_X2_Y1_N9
\mix0|pd2|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd2|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(1));

-- Location: IOIBUF_X3_Y0_N15
\in[121]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(121),
	o => \in[121]~input_o\);

-- Location: LCCOMB_X2_Y1_N18
\mix0|out2[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[1]~1_combout\ = \in[113]~input_o\ $ (\mix0|pd2|modul1|b\(1) $ (\in[121]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[113]~input_o\,
	datac => \mix0|pd2|modul1|b\(1),
	datad => \in[121]~input_o\,
	combout => \mix0|out2[1]~1_combout\);

-- Location: IOIBUF_X0_Y5_N15
\in[96]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(96),
	o => \in[96]~input_o\);

-- Location: LCCOMB_X2_Y1_N22
\mix0|pd3|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd3|modul1|b~0_combout\ = \in[103]~input_o\ $ (\in[96]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[103]~input_o\,
	datac => \in[96]~input_o\,
	combout => \mix0|pd3|modul1|b~0_combout\);

-- Location: FF_X2_Y1_N23
\mix0|pd3|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd3|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(1));

-- Location: LCCOMB_X2_Y1_N28
\mix0|out3[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(1) = \in[105]~input_o\ $ (\mix0|out2[1]~1_combout\ $ (\mix0|pd3|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[105]~input_o\,
	datab => \mix0|out2[1]~1_combout\,
	datac => \mix0|pd3|modul1|b\(1),
	combout => \mix0|out3\(1));

-- Location: IOIBUF_X0_Y6_N8
\in[122]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(122),
	o => \in[122]~input_o\);

-- Location: FF_X27_Y1_N11
\mix0|pd2|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[105]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(2));

-- Location: IOIBUF_X27_Y0_N8
\in[114]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(114),
	o => \in[114]~input_o\);

-- Location: LCCOMB_X27_Y1_N10
\mix0|out2[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[2]~2_combout\ = \in[122]~input_o\ $ (\mix0|pd2|modul1|b\(2) $ (\in[114]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[122]~input_o\,
	datac => \mix0|pd2|modul1|b\(2),
	datad => \in[114]~input_o\,
	combout => \mix0|out2[2]~2_combout\);

-- Location: IOIBUF_X22_Y0_N1
\in[106]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(106),
	o => \in[106]~input_o\);

-- Location: IOIBUF_X0_Y2_N15
\in[97]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(97),
	o => \in[97]~input_o\);

-- Location: FF_X27_Y1_N25
\mix0|pd3|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[97]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(2));

-- Location: LCCOMB_X27_Y1_N28
\mix0|out3[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(2) = \mix0|out2[2]~2_combout\ $ (\in[106]~input_o\ $ (\mix0|pd3|modul1|b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out2[2]~2_combout\,
	datab => \in[106]~input_o\,
	datad => \mix0|pd3|modul1|b\(2),
	combout => \mix0|out3\(2));

-- Location: IOIBUF_X32_Y0_N1
\in[107]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(107),
	o => \in[107]~input_o\);

-- Location: IOIBUF_X25_Y0_N22
\in[98]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(98),
	o => \in[98]~input_o\);

-- Location: LCCOMB_X32_Y1_N24
\mix0|pd3|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd3|modul1|b~1_combout\ = \in[98]~input_o\ $ (\in[103]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[98]~input_o\,
	datac => \in[103]~input_o\,
	combout => \mix0|pd3|modul1|b~1_combout\);

-- Location: FF_X32_Y1_N25
\mix0|pd3|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd3|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(3));

-- Location: LCCOMB_X27_Y1_N14
\mix0|pd2|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd2|modul1|b~1_combout\ = \in[111]~input_o\ $ (\in[106]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[111]~input_o\,
	datad => \in[106]~input_o\,
	combout => \mix0|pd2|modul1|b~1_combout\);

-- Location: FF_X27_Y1_N15
\mix0|pd2|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd2|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(3));

-- Location: IOIBUF_X32_Y0_N15
\in[115]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(115),
	o => \in[115]~input_o\);

-- Location: IOIBUF_X43_Y0_N1
\in[123]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(123),
	o => \in[123]~input_o\);

-- Location: LCCOMB_X32_Y1_N26
\mix0|out2[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[3]~3_combout\ = \mix0|pd2|modul1|b\(3) $ (\in[115]~input_o\ $ (\in[123]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd2|modul1|b\(3),
	datab => \in[115]~input_o\,
	datad => \in[123]~input_o\,
	combout => \mix0|out2[3]~3_combout\);

-- Location: LCCOMB_X32_Y1_N28
\mix0|out3[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(3) = \in[107]~input_o\ $ (\mix0|pd3|modul1|b\(3) $ (\mix0|out2[3]~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[107]~input_o\,
	datab => \mix0|pd3|modul1|b\(3),
	datac => \mix0|out2[3]~3_combout\,
	combout => \mix0|out3\(3));

-- Location: IOIBUF_X29_Y0_N22
\in[99]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(99),
	o => \in[99]~input_o\);

-- Location: LCCOMB_X32_Y1_N30
\mix0|pd3|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd3|modul1|b~2_combout\ = \in[103]~input_o\ $ (\in[99]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[103]~input_o\,
	datad => \in[99]~input_o\,
	combout => \mix0|pd3|modul1|b~2_combout\);

-- Location: FF_X32_Y1_N31
\mix0|pd3|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd3|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(4));

-- Location: IOIBUF_X41_Y0_N15
\in[108]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(108),
	o => \in[108]~input_o\);

-- Location: LCCOMB_X32_Y1_N16
\mix0|pd2|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd2|modul1|b~2_combout\ = \in[111]~input_o\ $ (\in[107]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[111]~input_o\,
	datad => \in[107]~input_o\,
	combout => \mix0|pd2|modul1|b~2_combout\);

-- Location: FF_X32_Y1_N17
\mix0|pd2|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd2|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(4));

-- Location: IOIBUF_X41_Y0_N22
\in[116]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(116),
	o => \in[116]~input_o\);

-- Location: IOIBUF_X38_Y0_N15
\in[124]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(124),
	o => \in[124]~input_o\);

-- Location: LCCOMB_X32_Y1_N10
\mix0|out2[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[4]~4_combout\ = \mix0|pd2|modul1|b\(4) $ (\in[116]~input_o\ $ (\in[124]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix0|pd2|modul1|b\(4),
	datac => \in[116]~input_o\,
	datad => \in[124]~input_o\,
	combout => \mix0|out2[4]~4_combout\);

-- Location: LCCOMB_X32_Y1_N20
\mix0|out3[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(4) = \mix0|pd3|modul1|b\(4) $ (\in[108]~input_o\ $ (\mix0|out2[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd3|modul1|b\(4),
	datac => \in[108]~input_o\,
	datad => \mix0|out2[4]~4_combout\,
	combout => \mix0|out3\(4));

-- Location: IOIBUF_X38_Y0_N1
\in[117]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(117),
	o => \in[117]~input_o\);

-- Location: IOIBUF_X38_Y0_N8
\in[125]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(125),
	o => \in[125]~input_o\);

-- Location: FF_X35_Y1_N3
\mix0|pd2|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[108]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(5));

-- Location: LCCOMB_X35_Y1_N2
\mix0|out2[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[5]~5_combout\ = \in[117]~input_o\ $ (\in[125]~input_o\ $ (\mix0|pd2|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[117]~input_o\,
	datab => \in[125]~input_o\,
	datac => \mix0|pd2|modul1|b\(5),
	combout => \mix0|out2[5]~5_combout\);

-- Location: IOIBUF_X43_Y0_N15
\in[109]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(109),
	o => \in[109]~input_o\);

-- Location: IOIBUF_X41_Y0_N1
\in[100]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(100),
	o => \in[100]~input_o\);

-- Location: FF_X35_Y1_N25
\mix0|pd3|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[100]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(5));

-- Location: LCCOMB_X35_Y1_N20
\mix0|out3[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(5) = \mix0|out2[5]~5_combout\ $ (\in[109]~input_o\ $ (\mix0|pd3|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix0|out2[5]~5_combout\,
	datac => \in[109]~input_o\,
	datad => \mix0|pd3|modul1|b\(5),
	combout => \mix0|out3\(5));

-- Location: IOIBUF_X43_Y0_N8
\in[101]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(101),
	o => \in[101]~input_o\);

-- Location: FF_X35_Y1_N7
\mix0|pd3|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[101]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(6));

-- Location: IOIBUF_X34_Y0_N29
\in[110]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(110),
	o => \in[110]~input_o\);

-- Location: IOIBUF_X29_Y0_N1
\in[118]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(118),
	o => \in[118]~input_o\);

-- Location: FF_X35_Y1_N1
\mix0|pd2|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[109]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(6));

-- Location: IOIBUF_X29_Y0_N8
\in[126]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(126),
	o => \in[126]~input_o\);

-- Location: LCCOMB_X35_Y1_N0
\mix0|out2[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[6]~6_combout\ = \in[118]~input_o\ $ (\mix0|pd2|modul1|b\(6) $ (\in[126]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[118]~input_o\,
	datac => \mix0|pd2|modul1|b\(6),
	datad => \in[126]~input_o\,
	combout => \mix0|out2[6]~6_combout\);

-- Location: LCCOMB_X35_Y1_N18
\mix0|out3[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(6) = \mix0|pd3|modul1|b\(6) $ (\in[110]~input_o\ $ (\mix0|out2[6]~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd3|modul1|b\(6),
	datab => \in[110]~input_o\,
	datad => \mix0|out2[6]~6_combout\,
	combout => \mix0|out3\(6));

-- Location: IOIBUF_X0_Y2_N8
\in[127]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(127),
	o => \in[127]~input_o\);

-- Location: FF_X27_Y1_N27
\mix0|pd2|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[110]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd2|modul1|b\(7));

-- Location: IOIBUF_X5_Y0_N29
\in[119]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(119),
	o => \in[119]~input_o\);

-- Location: LCCOMB_X27_Y1_N26
\mix0|out2[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2[7]~7_combout\ = \in[127]~input_o\ $ (\mix0|pd2|modul1|b\(7) $ (\in[119]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[127]~input_o\,
	datac => \mix0|pd2|modul1|b\(7),
	datad => \in[119]~input_o\,
	combout => \mix0|out2[7]~7_combout\);

-- Location: IOIBUF_X27_Y0_N1
\in[102]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_in\(102),
	o => \in[102]~input_o\);

-- Location: FF_X27_Y1_N1
\mix0|pd3|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[102]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd3|modul1|b\(7));

-- Location: LCCOMB_X27_Y1_N20
\mix0|out3[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out3\(7) = \in[111]~input_o\ $ (\mix0|out2[7]~7_combout\ $ (\mix0|pd3|modul1|b\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[111]~input_o\,
	datac => \mix0|out2[7]~7_combout\,
	datad => \mix0|pd3|modul1|b\(7),
	combout => \mix0|out3\(7));

-- Location: FF_X2_Y1_N15
\mix0|pd1|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[119]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(0));

-- Location: LCCOMB_X2_Y1_N0
\mix0|out2[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(0) = \mix0|pd1|modul1|b\(0) $ (\in[96]~input_o\ $ (\mix0|out2[0]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mix0|pd1|modul1|b\(0),
	datac => \in[96]~input_o\,
	datad => \mix0|out2[0]~0_combout\,
	combout => \mix0|out2\(0));

-- Location: LCCOMB_X2_Y1_N10
\mix0|pd1|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd1|modul1|b~0_combout\ = \in[112]~input_o\ $ (\in[119]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[112]~input_o\,
	datac => \in[119]~input_o\,
	combout => \mix0|pd1|modul1|b~0_combout\);

-- Location: FF_X2_Y1_N11
\mix0|pd1|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd1|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(1));

-- Location: LCCOMB_X2_Y1_N20
\mix0|out2[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(1) = \mix0|pd1|modul1|b\(1) $ (\in[97]~input_o\ $ (\mix0|out2[1]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd1|modul1|b\(1),
	datab => \in[97]~input_o\,
	datad => \mix0|out2[1]~1_combout\,
	combout => \mix0|out2\(1));

-- Location: FF_X27_Y1_N31
\mix0|pd1|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[113]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(2));

-- Location: LCCOMB_X27_Y1_N8
\mix0|out2[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(2) = \in[98]~input_o\ $ (\mix0|pd1|modul1|b\(2) $ (\mix0|out2[2]~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[98]~input_o\,
	datac => \mix0|pd1|modul1|b\(2),
	datad => \mix0|out2[2]~2_combout\,
	combout => \mix0|out2\(2));

-- Location: LCCOMB_X32_Y1_N14
\mix0|pd1|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd1|modul1|b~1_combout\ = \in[119]~input_o\ $ (\in[114]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[119]~input_o\,
	datad => \in[114]~input_o\,
	combout => \mix0|pd1|modul1|b~1_combout\);

-- Location: FF_X32_Y1_N15
\mix0|pd1|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd1|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(3));

-- Location: LCCOMB_X32_Y1_N8
\mix0|out2[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(3) = \mix0|out2[3]~3_combout\ $ (\in[99]~input_o\ $ (\mix0|pd1|modul1|b\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out2[3]~3_combout\,
	datab => \in[99]~input_o\,
	datac => \mix0|pd1|modul1|b\(3),
	combout => \mix0|out2\(3));

-- Location: LCCOMB_X32_Y1_N2
\mix0|pd1|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd1|modul1|b~2_combout\ = \in[119]~input_o\ $ (\in[115]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[119]~input_o\,
	datad => \in[115]~input_o\,
	combout => \mix0|pd1|modul1|b~2_combout\);

-- Location: FF_X32_Y1_N3
\mix0|pd1|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd1|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(4));

-- Location: LCCOMB_X32_Y1_N12
\mix0|out2[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(4) = \mix0|out2[4]~4_combout\ $ (\in[100]~input_o\ $ (\mix0|pd1|modul1|b\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out2[4]~4_combout\,
	datac => \in[100]~input_o\,
	datad => \mix0|pd1|modul1|b\(4),
	combout => \mix0|out2\(4));

-- Location: FF_X35_Y1_N29
\mix0|pd1|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[116]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(5));

-- Location: LCCOMB_X35_Y1_N30
\mix0|out2[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(5) = \in[101]~input_o\ $ (\mix0|pd1|modul1|b\(5) $ (\mix0|out2[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[101]~input_o\,
	datab => \mix0|pd1|modul1|b\(5),
	datad => \mix0|out2[5]~5_combout\,
	combout => \mix0|out2\(5));

-- Location: FF_X35_Y1_N9
\mix0|pd1|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[117]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(6));

-- Location: LCCOMB_X35_Y1_N26
\mix0|out2[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(6) = \in[102]~input_o\ $ (\mix0|pd1|modul1|b\(6) $ (\mix0|out2[6]~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[102]~input_o\,
	datab => \mix0|pd1|modul1|b\(6),
	datad => \mix0|out2[6]~6_combout\,
	combout => \mix0|out2\(6));

-- Location: FF_X27_Y1_N3
\mix0|pd1|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[118]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd1|modul1|b\(7));

-- Location: LCCOMB_X27_Y1_N4
\mix0|out2[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out2\(7) = \mix0|out2[7]~7_combout\ $ (\in[103]~input_o\ $ (\mix0|pd1|modul1|b\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out2[7]~7_combout\,
	datac => \in[103]~input_o\,
	datad => \mix0|pd1|modul1|b\(7),
	combout => \mix0|out2\(7));

-- Location: FF_X2_Y1_N7
\mix0|pd0|modul1|b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[127]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(0));

-- Location: LCCOMB_X2_Y1_N6
\mix0|out0[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[0]~0_combout\ = \in[104]~input_o\ $ (\in[96]~input_o\ $ (\mix0|pd0|modul1|b\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[104]~input_o\,
	datab => \in[96]~input_o\,
	datac => \mix0|pd0|modul1|b\(0),
	combout => \mix0|out0[0]~0_combout\);

-- Location: LCCOMB_X2_Y1_N14
\mix0|out1[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(0) = \mix0|out0[0]~0_combout\ $ (\mix0|pd1|modul1|b\(0) $ (\in[120]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out0[0]~0_combout\,
	datac => \mix0|pd1|modul1|b\(0),
	datad => \in[120]~input_o\,
	combout => \mix0|out1\(0));

-- Location: LCCOMB_X2_Y1_N24
\mix0|pd0|modul1|b~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd0|modul1|b~0_combout\ = \in[127]~input_o\ $ (\in[120]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in[127]~input_o\,
	datad => \in[120]~input_o\,
	combout => \mix0|pd0|modul1|b~0_combout\);

-- Location: FF_X2_Y1_N25
\mix0|pd0|modul1|b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd0|modul1|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(1));

-- Location: LCCOMB_X2_Y1_N26
\mix0|out0[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[1]~1_combout\ = \in[105]~input_o\ $ (\in[97]~input_o\ $ (\mix0|pd0|modul1|b\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[105]~input_o\,
	datab => \in[97]~input_o\,
	datad => \mix0|pd0|modul1|b\(1),
	combout => \mix0|out0[1]~1_combout\);

-- Location: LCCOMB_X2_Y1_N4
\mix0|out1[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(1) = \mix0|pd1|modul1|b\(1) $ (\mix0|out0[1]~1_combout\ $ (\in[121]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd1|modul1|b\(1),
	datac => \mix0|out0[1]~1_combout\,
	datad => \in[121]~input_o\,
	combout => \mix0|out1\(1));

-- Location: FF_X27_Y1_N23
\mix0|pd0|modul1|b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[121]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(2));

-- Location: LCCOMB_X27_Y1_N22
\mix0|out0[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[2]~2_combout\ = \in[106]~input_o\ $ (\mix0|pd0|modul1|b\(2) $ (\in[98]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[106]~input_o\,
	datac => \mix0|pd0|modul1|b\(2),
	datad => \in[98]~input_o\,
	combout => \mix0|out0[2]~2_combout\);

-- Location: LCCOMB_X27_Y1_N30
\mix0|out1[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(2) = \mix0|out0[2]~2_combout\ $ (\in[122]~input_o\ $ (\mix0|pd1|modul1|b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out0[2]~2_combout\,
	datab => \in[122]~input_o\,
	datac => \mix0|pd1|modul1|b\(2),
	combout => \mix0|out1\(2));

-- Location: LCCOMB_X27_Y1_N16
\mix0|pd0|modul1|b~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd0|modul1|b~1_combout\ = \in[127]~input_o\ $ (\in[122]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[127]~input_o\,
	datad => \in[122]~input_o\,
	combout => \mix0|pd0|modul1|b~1_combout\);

-- Location: FF_X27_Y1_N17
\mix0|pd0|modul1|b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd0|modul1|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(3));

-- Location: LCCOMB_X32_Y1_N6
\mix0|out0[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[3]~3_combout\ = \mix0|pd0|modul1|b\(3) $ (\in[99]~input_o\ $ (\in[107]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd0|modul1|b\(3),
	datab => \in[99]~input_o\,
	datad => \in[107]~input_o\,
	combout => \mix0|out0[3]~3_combout\);

-- Location: LCCOMB_X32_Y1_N0
\mix0|out1[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(3) = \mix0|out0[3]~3_combout\ $ (\mix0|pd1|modul1|b\(3) $ (\in[123]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out0[3]~3_combout\,
	datac => \mix0|pd1|modul1|b\(3),
	datad => \in[123]~input_o\,
	combout => \mix0|out1\(3));

-- Location: LCCOMB_X35_Y1_N12
\mix0|pd0|modul1|b~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|pd0|modul1|b~2_combout\ = \in[127]~input_o\ $ (\in[123]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[127]~input_o\,
	datad => \in[123]~input_o\,
	combout => \mix0|pd0|modul1|b~2_combout\);

-- Location: FF_X35_Y1_N13
\mix0|pd0|modul1|b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \mix0|pd0|modul1|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(4));

-- Location: LCCOMB_X35_Y1_N14
\mix0|out0[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[4]~4_combout\ = \mix0|pd0|modul1|b\(4) $ (\in[100]~input_o\ $ (\in[108]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd0|modul1|b\(4),
	datab => \in[100]~input_o\,
	datac => \in[108]~input_o\,
	combout => \mix0|out0[4]~4_combout\);

-- Location: LCCOMB_X32_Y1_N18
\mix0|out1[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(4) = \mix0|out0[4]~4_combout\ $ (\mix0|pd1|modul1|b\(4) $ (\in[124]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out0[4]~4_combout\,
	datab => \mix0|pd1|modul1|b\(4),
	datad => \in[124]~input_o\,
	combout => \mix0|out1\(4));

-- Location: FF_X35_Y1_N17
\mix0|pd0|modul1|b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[124]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(5));

-- Location: LCCOMB_X35_Y1_N16
\mix0|out0[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[5]~5_combout\ = \in[101]~input_o\ $ (\in[109]~input_o\ $ (\mix0|pd0|modul1|b\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[101]~input_o\,
	datab => \in[109]~input_o\,
	datac => \mix0|pd0|modul1|b\(5),
	combout => \mix0|out0[5]~5_combout\);

-- Location: LCCOMB_X35_Y1_N28
\mix0|out1[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(5) = \in[125]~input_o\ $ (\mix0|pd1|modul1|b\(5) $ (\mix0|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[125]~input_o\,
	datac => \mix0|pd1|modul1|b\(5),
	datad => \mix0|out0[5]~5_combout\,
	combout => \mix0|out1\(5));

-- Location: FF_X35_Y1_N11
\mix0|pd0|modul1|b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[125]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(6));

-- Location: LCCOMB_X35_Y1_N10
\mix0|out0[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[6]~6_combout\ = \in[102]~input_o\ $ (\mix0|pd0|modul1|b\(6) $ (\in[110]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[102]~input_o\,
	datac => \mix0|pd0|modul1|b\(6),
	datad => \in[110]~input_o\,
	combout => \mix0|out0[6]~6_combout\);

-- Location: LCCOMB_X35_Y1_N8
\mix0|out1[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(6) = \in[126]~input_o\ $ (\mix0|pd1|modul1|b\(6) $ (\mix0|out0[6]~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[126]~input_o\,
	datac => \mix0|pd1|modul1|b\(6),
	datad => \mix0|out0[6]~6_combout\,
	combout => \mix0|out1\(6));

-- Location: FF_X27_Y1_N19
\mix0|pd0|modul1|b[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \in[126]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \mix0|pd0|modul1|b\(7));

-- Location: LCCOMB_X27_Y1_N18
\mix0|out0[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0[7]~7_combout\ = \in[103]~input_o\ $ (\mix0|pd0|modul1|b\(7) $ (\in[111]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[103]~input_o\,
	datac => \mix0|pd0|modul1|b\(7),
	datad => \in[111]~input_o\,
	combout => \mix0|out0[7]~7_combout\);

-- Location: LCCOMB_X27_Y1_N2
\mix0|out1[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out1\(7) = \in[127]~input_o\ $ (\mix0|pd1|modul1|b\(7) $ (\mix0|out0[7]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in[127]~input_o\,
	datac => \mix0|pd1|modul1|b\(7),
	datad => \mix0|out0[7]~7_combout\,
	combout => \mix0|out1\(7));

-- Location: LCCOMB_X2_Y1_N16
\mix0|out0[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(0) = \in[112]~input_o\ $ (\mix0|pd3|modul1|b\(0) $ (\mix0|out0[0]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[112]~input_o\,
	datac => \mix0|pd3|modul1|b\(0),
	datad => \mix0|out0[0]~0_combout\,
	combout => \mix0|out0\(0));

-- Location: LCCOMB_X2_Y1_N30
\mix0|out0[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(1) = \mix0|pd3|modul1|b\(1) $ (\in[113]~input_o\ $ (\mix0|out0[1]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd3|modul1|b\(1),
	datab => \in[113]~input_o\,
	datac => \mix0|out0[1]~1_combout\,
	combout => \mix0|out0\(1));

-- Location: LCCOMB_X27_Y1_N24
\mix0|out0[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(2) = \mix0|out0[2]~2_combout\ $ (\mix0|pd3|modul1|b\(2) $ (\in[114]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out0[2]~2_combout\,
	datac => \mix0|pd3|modul1|b\(2),
	datad => \in[114]~input_o\,
	combout => \mix0|out0\(2));

-- Location: LCCOMB_X32_Y1_N4
\mix0|out0[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(3) = \mix0|out0[3]~3_combout\ $ (\in[115]~input_o\ $ (\mix0|pd3|modul1|b\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out0[3]~3_combout\,
	datab => \in[115]~input_o\,
	datad => \mix0|pd3|modul1|b\(3),
	combout => \mix0|out0\(3));

-- Location: LCCOMB_X32_Y1_N22
\mix0|out0[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(4) = \mix0|pd3|modul1|b\(4) $ (\in[116]~input_o\ $ (\mix0|out0[4]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|pd3|modul1|b\(4),
	datac => \in[116]~input_o\,
	datad => \mix0|out0[4]~4_combout\,
	combout => \mix0|out0\(4));

-- Location: LCCOMB_X35_Y1_N24
\mix0|out0[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(5) = \in[117]~input_o\ $ (\mix0|pd3|modul1|b\(5) $ (\mix0|out0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[117]~input_o\,
	datac => \mix0|pd3|modul1|b\(5),
	datad => \mix0|out0[5]~5_combout\,
	combout => \mix0|out0\(5));

-- Location: LCCOMB_X35_Y1_N6
\mix0|out0[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(6) = \mix0|out0[6]~6_combout\ $ (\mix0|pd3|modul1|b\(6) $ (\in[118]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mix0|out0[6]~6_combout\,
	datac => \mix0|pd3|modul1|b\(6),
	datad => \in[118]~input_o\,
	combout => \mix0|out0\(6));

-- Location: LCCOMB_X27_Y1_N0
\mix0|out0[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \mix0|out0\(7) = \in[119]~input_o\ $ (\mix0|pd3|modul1|b\(7) $ (\mix0|out0[7]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \in[119]~input_o\,
	datac => \mix0|pd3|modul1|b\(7),
	datad => \mix0|out0[7]~7_combout\,
	combout => \mix0|out0\(7));

-- Location: IOIBUF_X67_Y35_N1
\enable~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_enable,
	o => \enable~input_o\);

\ww_out\(0) <= \out[0]~output_o\;

\ww_out\(1) <= \out[1]~output_o\;

\ww_out\(2) <= \out[2]~output_o\;

\ww_out\(3) <= \out[3]~output_o\;

\ww_out\(4) <= \out[4]~output_o\;

\ww_out\(5) <= \out[5]~output_o\;

\ww_out\(6) <= \out[6]~output_o\;

\ww_out\(7) <= \out[7]~output_o\;

\ww_out\(8) <= \out[8]~output_o\;

\ww_out\(9) <= \out[9]~output_o\;

\ww_out\(10) <= \out[10]~output_o\;

\ww_out\(11) <= \out[11]~output_o\;

\ww_out\(12) <= \out[12]~output_o\;

\ww_out\(13) <= \out[13]~output_o\;

\ww_out\(14) <= \out[14]~output_o\;

\ww_out\(15) <= \out[15]~output_o\;

\ww_out\(16) <= \out[16]~output_o\;

\ww_out\(17) <= \out[17]~output_o\;

\ww_out\(18) <= \out[18]~output_o\;

\ww_out\(19) <= \out[19]~output_o\;

\ww_out\(20) <= \out[20]~output_o\;

\ww_out\(21) <= \out[21]~output_o\;

\ww_out\(22) <= \out[22]~output_o\;

\ww_out\(23) <= \out[23]~output_o\;

\ww_out\(24) <= \out[24]~output_o\;

\ww_out\(25) <= \out[25]~output_o\;

\ww_out\(26) <= \out[26]~output_o\;

\ww_out\(27) <= \out[27]~output_o\;

\ww_out\(28) <= \out[28]~output_o\;

\ww_out\(29) <= \out[29]~output_o\;

\ww_out\(30) <= \out[30]~output_o\;

\ww_out\(31) <= \out[31]~output_o\;

\ww_out\(32) <= \out[32]~output_o\;

\ww_out\(33) <= \out[33]~output_o\;

\ww_out\(34) <= \out[34]~output_o\;

\ww_out\(35) <= \out[35]~output_o\;

\ww_out\(36) <= \out[36]~output_o\;

\ww_out\(37) <= \out[37]~output_o\;

\ww_out\(38) <= \out[38]~output_o\;

\ww_out\(39) <= \out[39]~output_o\;

\ww_out\(40) <= \out[40]~output_o\;

\ww_out\(41) <= \out[41]~output_o\;

\ww_out\(42) <= \out[42]~output_o\;

\ww_out\(43) <= \out[43]~output_o\;

\ww_out\(44) <= \out[44]~output_o\;

\ww_out\(45) <= \out[45]~output_o\;

\ww_out\(46) <= \out[46]~output_o\;

\ww_out\(47) <= \out[47]~output_o\;

\ww_out\(48) <= \out[48]~output_o\;

\ww_out\(49) <= \out[49]~output_o\;

\ww_out\(50) <= \out[50]~output_o\;

\ww_out\(51) <= \out[51]~output_o\;

\ww_out\(52) <= \out[52]~output_o\;

\ww_out\(53) <= \out[53]~output_o\;

\ww_out\(54) <= \out[54]~output_o\;

\ww_out\(55) <= \out[55]~output_o\;

\ww_out\(56) <= \out[56]~output_o\;

\ww_out\(57) <= \out[57]~output_o\;

\ww_out\(58) <= \out[58]~output_o\;

\ww_out\(59) <= \out[59]~output_o\;

\ww_out\(60) <= \out[60]~output_o\;

\ww_out\(61) <= \out[61]~output_o\;

\ww_out\(62) <= \out[62]~output_o\;

\ww_out\(63) <= \out[63]~output_o\;

\ww_out\(64) <= \out[64]~output_o\;

\ww_out\(65) <= \out[65]~output_o\;

\ww_out\(66) <= \out[66]~output_o\;

\ww_out\(67) <= \out[67]~output_o\;

\ww_out\(68) <= \out[68]~output_o\;

\ww_out\(69) <= \out[69]~output_o\;

\ww_out\(70) <= \out[70]~output_o\;

\ww_out\(71) <= \out[71]~output_o\;

\ww_out\(72) <= \out[72]~output_o\;

\ww_out\(73) <= \out[73]~output_o\;

\ww_out\(74) <= \out[74]~output_o\;

\ww_out\(75) <= \out[75]~output_o\;

\ww_out\(76) <= \out[76]~output_o\;

\ww_out\(77) <= \out[77]~output_o\;

\ww_out\(78) <= \out[78]~output_o\;

\ww_out\(79) <= \out[79]~output_o\;

\ww_out\(80) <= \out[80]~output_o\;

\ww_out\(81) <= \out[81]~output_o\;

\ww_out\(82) <= \out[82]~output_o\;

\ww_out\(83) <= \out[83]~output_o\;

\ww_out\(84) <= \out[84]~output_o\;

\ww_out\(85) <= \out[85]~output_o\;

\ww_out\(86) <= \out[86]~output_o\;

\ww_out\(87) <= \out[87]~output_o\;

\ww_out\(88) <= \out[88]~output_o\;

\ww_out\(89) <= \out[89]~output_o\;

\ww_out\(90) <= \out[90]~output_o\;

\ww_out\(91) <= \out[91]~output_o\;

\ww_out\(92) <= \out[92]~output_o\;

\ww_out\(93) <= \out[93]~output_o\;

\ww_out\(94) <= \out[94]~output_o\;

\ww_out\(95) <= \out[95]~output_o\;

\ww_out\(96) <= \out[96]~output_o\;

\ww_out\(97) <= \out[97]~output_o\;

\ww_out\(98) <= \out[98]~output_o\;

\ww_out\(99) <= \out[99]~output_o\;

\ww_out\(100) <= \out[100]~output_o\;

\ww_out\(101) <= \out[101]~output_o\;

\ww_out\(102) <= \out[102]~output_o\;

\ww_out\(103) <= \out[103]~output_o\;

\ww_out\(104) <= \out[104]~output_o\;

\ww_out\(105) <= \out[105]~output_o\;

\ww_out\(106) <= \out[106]~output_o\;

\ww_out\(107) <= \out[107]~output_o\;

\ww_out\(108) <= \out[108]~output_o\;

\ww_out\(109) <= \out[109]~output_o\;

\ww_out\(110) <= \out[110]~output_o\;

\ww_out\(111) <= \out[111]~output_o\;

\ww_out\(112) <= \out[112]~output_o\;

\ww_out\(113) <= \out[113]~output_o\;

\ww_out\(114) <= \out[114]~output_o\;

\ww_out\(115) <= \out[115]~output_o\;

\ww_out\(116) <= \out[116]~output_o\;

\ww_out\(117) <= \out[117]~output_o\;

\ww_out\(118) <= \out[118]~output_o\;

\ww_out\(119) <= \out[119]~output_o\;

\ww_out\(120) <= \out[120]~output_o\;

\ww_out\(121) <= \out[121]~output_o\;

\ww_out\(122) <= \out[122]~output_o\;

\ww_out\(123) <= \out[123]~output_o\;

\ww_out\(124) <= \out[124]~output_o\;

\ww_out\(125) <= \out[125]~output_o\;

\ww_out\(126) <= \out[126]~output_o\;

\ww_out\(127) <= \out[127]~output_o\;
END structure;


