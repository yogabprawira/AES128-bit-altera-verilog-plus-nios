module addroundkey(in, key, out, enable, clk);

input [127:0] in, key;
output reg [127:0] out;
input clk, enable;

always @(posedge clk)
begin
	if(enable)	out <= in ^ key;
end
endmodule
