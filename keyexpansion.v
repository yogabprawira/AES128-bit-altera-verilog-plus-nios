module keyexpansion(
inkey,
outkey,
sel, 
clk
);

input wire [127:0] inkey;
output reg [127:0] outkey;
input clk;
input [3:0] sel;
reg [127:0] inkeyreg;

wire [7:0] a, b, c, d;

subbytes subbytesA(inkeyreg[111:104], a, 1, clk);
subbytes subbytesB(inkeyreg[119:112], b, 1, clk);
subbytes subbytesC(inkeyreg[127:120], c, 1, clk);
subbytes subbytesD(inkeyreg[103:96], d, 1, clk);

always @(posedge clk)
begin
	if(sel==0) outkey <= inkey;
	else begin
		inkeyreg <= inkey;
		case(sel)
			1 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h01;
			2 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h02;
			3 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h04;
			4 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h08;
			5 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h10;
			6 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h20;
			7 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h40;
			8 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h80;
			9 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h1b;
			10 : outkey[7:0] = a ^ inkeyreg[7:0] ^ 8'h36;
		endcase
		outkey[15:8] = b ^ inkeyreg[15:8];
		outkey[23:16] = c ^ inkeyreg[23:16];
		outkey[31:24] = d ^ inkeyreg[31:24];
		outkey[39:32] = outkey[7:0] ^ inkeyreg[39:32];
		outkey[47:40] = outkey[15:8] ^ inkeyreg[47:40];
		outkey[55:48] = outkey[23:16] ^ inkeyreg[55:48];
		outkey[63:56] = outkey[31:24] ^ inkeyreg[63:56];
		outkey[71:64] = outkey[39:32] ^ inkeyreg[71:64];
		outkey[79:72] = outkey[47:40] ^ inkeyreg[79:72];
		outkey[87:80] = outkey[55:48] ^ inkeyreg[87:80];
		outkey[95:88] = outkey[63:56] ^ inkeyreg[95:88];
		outkey[103:96] = outkey[71:64] ^ inkeyreg[103:96];
		outkey[111:104] = outkey[79:72] ^ inkeyreg[111:104];
		outkey[119:112] = outkey[87:80] ^ inkeyreg[119:112];
		outkey[127:120] = outkey[95:88] ^ inkeyreg[127:120];
	end
end
endmodule