module subbytes(
inputSubBytes,
outputSubBytes,
enable,
clk
);

output reg [7:0] outputSubBytes;
input [7:0] inputSubBytes;
input clk, enable;

always @(posedge clk)
begin
	if(enable)
	case (inputSubBytes)
		8'h00 : outputSubBytes <= 8'h63;
		8'h01 : outputSubBytes <= 8'h7c;
		8'h02 : outputSubBytes <= 8'h77;
		8'h03 : outputSubBytes <= 8'h7b;
		8'h04 : outputSubBytes <= 8'hf2;
		8'h05 : outputSubBytes <= 8'h6b;
		8'h06 : outputSubBytes <= 8'h6f;
		8'h07 : outputSubBytes <= 8'hc5;
		8'h08 : outputSubBytes <= 8'h30;
		8'h09 : outputSubBytes <= 8'h01;
		8'h0a : outputSubBytes <= 8'h67;
		8'h0b : outputSubBytes <= 8'h2b;
		8'h0c : outputSubBytes <= 8'hfe;
		8'h0d : outputSubBytes <= 8'hd7;
		8'h0e : outputSubBytes <= 8'hab;
		8'h0f : outputSubBytes <= 8'h76;
		
		8'h10 : outputSubBytes <= 8'hca;
		8'h11 : outputSubBytes <= 8'h82;
		8'h12 : outputSubBytes <= 8'hc9;
		8'h13 : outputSubBytes <= 8'h7d;
		8'h14 : outputSubBytes <= 8'hfa;
		8'h15 : outputSubBytes <= 8'h59;
		8'h16 : outputSubBytes <= 8'h47;
		8'h17 : outputSubBytes <= 8'hf0;
		8'h18 : outputSubBytes <= 8'had;
		8'h19 : outputSubBytes <= 8'hd4;
		8'h1a : outputSubBytes <= 8'ha2;
		8'h1b : outputSubBytes <= 8'haf;
		8'h1c : outputSubBytes <= 8'h9c;
		8'h1d : outputSubBytes <= 8'ha4;
		8'h1e : outputSubBytes <= 8'h72;
		8'h1f : outputSubBytes <= 8'hc0;
		
		8'h20 : outputSubBytes <= 8'hb7;
		8'h21 : outputSubBytes <= 8'hfd;
		8'h22 : outputSubBytes <= 8'h93;
		8'h23 : outputSubBytes <= 8'h26;
		8'h24 : outputSubBytes <= 8'h36;
		8'h25 : outputSubBytes <= 8'h3f;
		8'h26 : outputSubBytes <= 8'hf7;
		8'h27 : outputSubBytes <= 8'hcc;
		8'h28 : outputSubBytes <= 8'h34;
		8'h29 : outputSubBytes <= 8'ha5;
		8'h2a : outputSubBytes <= 8'he5;
		8'h2b : outputSubBytes <= 8'hf1;
		8'h2c : outputSubBytes <= 8'h71;
		8'h2d : outputSubBytes <= 8'hd8;
		8'h2e : outputSubBytes <= 8'h31;
		8'h2f : outputSubBytes <= 8'h15;
		
		8'h30 : outputSubBytes <= 8'h04;
		8'h31 : outputSubBytes <= 8'hc7;
		8'h32 : outputSubBytes <= 8'h23;
		8'h33 : outputSubBytes <= 8'hc3;
		8'h34 : outputSubBytes <= 8'h18;
		8'h35 : outputSubBytes <= 8'h96;
		8'h36 : outputSubBytes <= 8'h05;
		8'h37 : outputSubBytes <= 8'h9a;
		8'h38 : outputSubBytes <= 8'h07;
		8'h39 : outputSubBytes <= 8'h12;
		8'h3a : outputSubBytes <= 8'h80;
		8'h3b : outputSubBytes <= 8'he2;
		8'h3c : outputSubBytes <= 8'heb;
		8'h3d : outputSubBytes <= 8'h27;
		8'h3e : outputSubBytes <= 8'hb2;
		8'h3f : outputSubBytes <= 8'h75;
		
		8'h40 : outputSubBytes <= 8'h09;
		8'h41 : outputSubBytes <= 8'h83;
		8'h42 : outputSubBytes <= 8'h2c;
		8'h43 : outputSubBytes <= 8'h1a;
		8'h44 : outputSubBytes <= 8'h1b;
		8'h45 : outputSubBytes <= 8'h6e;
		8'h46 : outputSubBytes <= 8'h5a;
		8'h47 : outputSubBytes <= 8'ha0;
		8'h48 : outputSubBytes <= 8'h52;
		8'h49 : outputSubBytes <= 8'h3b;
		8'h4a : outputSubBytes <= 8'hd6;
		8'h4b : outputSubBytes <= 8'hb3;
		8'h4c : outputSubBytes <= 8'h29;
		8'h4d : outputSubBytes <= 8'he3;
		8'h4e : outputSubBytes <= 8'h2f;
		8'h4f : outputSubBytes <= 8'h84;
		
		8'h50 : outputSubBytes <= 8'h53;
		8'h51 : outputSubBytes <= 8'hd1;
		8'h52 : outputSubBytes <= 8'h00;
		8'h53 : outputSubBytes <= 8'hed;
		8'h54 : outputSubBytes <= 8'h20;
		8'h55 : outputSubBytes <= 8'hfc;
		8'h56 : outputSubBytes <= 8'hb1;
		8'h57 : outputSubBytes <= 8'h5b;
		8'h58 : outputSubBytes <= 8'h6a;
		8'h59 : outputSubBytes <= 8'hcb;
		8'h5a : outputSubBytes <= 8'hbe;
		8'h5b : outputSubBytes <= 8'h39;
		8'h5c : outputSubBytes <= 8'h4a;
		8'h5d : outputSubBytes <= 8'h4c;
		8'h5e : outputSubBytes <= 8'h58;
		8'h5f : outputSubBytes <= 8'hcf;
		
		8'h60 : outputSubBytes <= 8'hd0;
		8'h61 : outputSubBytes <= 8'hef;
		8'h62 : outputSubBytes <= 8'haa;
		8'h63 : outputSubBytes <= 8'hfb;
		8'h64 : outputSubBytes <= 8'h43;
		8'h65 : outputSubBytes <= 8'h4d;
		8'h66 : outputSubBytes <= 8'h33;
		8'h67 : outputSubBytes <= 8'h85;
		8'h68 : outputSubBytes <= 8'h45;
		8'h69 : outputSubBytes <= 8'hf9;
		8'h6a : outputSubBytes <= 8'h02;
		8'h6b : outputSubBytes <= 8'h7f;
		8'h6c : outputSubBytes <= 8'h50;
		8'h6d : outputSubBytes <= 8'h3c;
		8'h6e : outputSubBytes <= 8'h9f;
		8'h6f : outputSubBytes <= 8'ha8;
		
		8'h70 : outputSubBytes <= 8'h51;
		8'h71 : outputSubBytes <= 8'ha3;
		8'h72 : outputSubBytes <= 8'h40;
		8'h73 : outputSubBytes <= 8'h8f;
		8'h74 : outputSubBytes <= 8'h92;
		8'h75 : outputSubBytes <= 8'h9d;
		8'h76 : outputSubBytes <= 8'h38;
		8'h77 : outputSubBytes <= 8'hf5;
		8'h78 : outputSubBytes <= 8'hbc;
		8'h79 : outputSubBytes <= 8'hb6;
		8'h7a : outputSubBytes <= 8'hda;
		8'h7b : outputSubBytes <= 8'h21;
		8'h7c : outputSubBytes <= 8'h10;
		8'h7d : outputSubBytes <= 8'hff;
		8'h7e : outputSubBytes <= 8'hf3;
		8'h7f : outputSubBytes <= 8'hd2;
		
		8'h80 : outputSubBytes <= 8'hcd;
		8'h81 : outputSubBytes <= 8'h0c;
		8'h82 : outputSubBytes <= 8'h13;
		8'h83 : outputSubBytes <= 8'hec;
		8'h84 : outputSubBytes <= 8'h5f;
		8'h85 : outputSubBytes <= 8'h97;
		8'h86 : outputSubBytes <= 8'h44;
		8'h87 : outputSubBytes <= 8'h17;
		8'h88 : outputSubBytes <= 8'hc4;
		8'h89 : outputSubBytes <= 8'ha7;
		8'h8a : outputSubBytes <= 8'h7e;
		8'h8b : outputSubBytes <= 8'h3d;
		8'h8c : outputSubBytes <= 8'h64;
		8'h8d : outputSubBytes <= 8'h5d;
		8'h8e : outputSubBytes <= 8'h19;
		8'h8f : outputSubBytes <= 8'h73;
		
		8'h90 : outputSubBytes <= 8'h60;
		8'h91 : outputSubBytes <= 8'h81;
		8'h92 : outputSubBytes <= 8'h4f;
		8'h93 : outputSubBytes <= 8'hdc;
		8'h94 : outputSubBytes <= 8'h22;
		8'h95 : outputSubBytes <= 8'h2a;
		8'h96 : outputSubBytes <= 8'h90;
		8'h97 : outputSubBytes <= 8'h88;
		8'h98 : outputSubBytes <= 8'h46;
		8'h99 : outputSubBytes <= 8'hee;
		8'h9a : outputSubBytes <= 8'hb8;
		8'h9b : outputSubBytes <= 8'h14;
		8'h9c : outputSubBytes <= 8'hde;
		8'h9d : outputSubBytes <= 8'h5e;
		8'h9e : outputSubBytes <= 8'h0b;
		8'h9f : outputSubBytes <= 8'hdb;
		
		8'ha0 : outputSubBytes <= 8'he0;
		8'ha1 : outputSubBytes <= 8'h32;
		8'ha2 : outputSubBytes <= 8'h3a;
		8'ha3 : outputSubBytes <= 8'h0a;
		8'ha4 : outputSubBytes <= 8'h49;
		8'ha5 : outputSubBytes <= 8'h06;
		8'ha6 : outputSubBytes <= 8'h24;
		8'ha7 : outputSubBytes <= 8'h5c;
		8'ha8 : outputSubBytes <= 8'hc2;
		8'ha9 : outputSubBytes <= 8'hd3;
		8'haa : outputSubBytes <= 8'hac;
		8'hab : outputSubBytes <= 8'h62;
		8'hac : outputSubBytes <= 8'h91;
		8'had : outputSubBytes <= 8'h95;
		8'hae : outputSubBytes <= 8'he4;
		8'haf : outputSubBytes <= 8'h79;
		
		8'hb0 : outputSubBytes <= 8'he7;
		8'hb1 : outputSubBytes <= 8'hc8;
		8'hb2 : outputSubBytes <= 8'h37;
		8'hb3 : outputSubBytes <= 8'h6d;
		8'hb4 : outputSubBytes <= 8'h8d;
		8'hb5 : outputSubBytes <= 8'hd5;
		8'hb6 : outputSubBytes <= 8'h4e;
		8'hb7 : outputSubBytes <= 8'ha9;
		8'hb8 : outputSubBytes <= 8'h6c;
		8'hb9 : outputSubBytes <= 8'h56;
		8'hba : outputSubBytes <= 8'hf4;
		8'hbb : outputSubBytes <= 8'hea;
		8'hbc : outputSubBytes <= 8'h65;
		8'hbd : outputSubBytes <= 8'h7a;
		8'hbe : outputSubBytes <= 8'hae;
		8'hbf : outputSubBytes <= 8'h08;
		
		8'hc0 : outputSubBytes <= 8'hba;
		8'hc1 : outputSubBytes <= 8'h78;
		8'hc2 : outputSubBytes <= 8'h25;
		8'hc3 : outputSubBytes <= 8'h2e;
		8'hc4 : outputSubBytes <= 8'h1c;
		8'hc5 : outputSubBytes <= 8'ha6;
		8'hc6 : outputSubBytes <= 8'hb4;
		8'hc7 : outputSubBytes <= 8'hc6;
		8'hc8 : outputSubBytes <= 8'he8;
		8'hc9 : outputSubBytes <= 8'hdd;
		8'hca : outputSubBytes <= 8'h74;
		8'hcb : outputSubBytes <= 8'h1f;
		8'hcc : outputSubBytes <= 8'h4b;
		8'hcd : outputSubBytes <= 8'hbd;
		8'hce : outputSubBytes <= 8'h8b;
		8'hcf : outputSubBytes <= 8'h8a;
		
		8'hd0 : outputSubBytes <= 8'h70;
		8'hd1 : outputSubBytes <= 8'h3e;
		8'hd2 : outputSubBytes <= 8'hb5;
		8'hd3 : outputSubBytes <= 8'h66;
		8'hd4 : outputSubBytes <= 8'h48;
		8'hd5 : outputSubBytes <= 8'h03;
		8'hd6 : outputSubBytes <= 8'hf6;
		8'hd7 : outputSubBytes <= 8'h0e;
		8'hd8 : outputSubBytes <= 8'h61;
		8'hd9 : outputSubBytes <= 8'h35;
		8'hda : outputSubBytes <= 8'h57;
		8'hdb : outputSubBytes <= 8'hb9;
		8'hdc : outputSubBytes <= 8'h86;
		8'hdd : outputSubBytes <= 8'hc1;
		8'hde : outputSubBytes <= 8'h1d;
		8'hdf : outputSubBytes <= 8'h9e;
		
		8'he0 : outputSubBytes <= 8'he1;
		8'he1 : outputSubBytes <= 8'hf8;
		8'he2 : outputSubBytes <= 8'h98;
		8'he3 : outputSubBytes <= 8'h11;
		8'he4 : outputSubBytes <= 8'h69;
		8'he5 : outputSubBytes <= 8'hd9;
		8'he6 : outputSubBytes <= 8'h8e;
		8'he7 : outputSubBytes <= 8'h94;
		8'he8 : outputSubBytes <= 8'h9b;
		8'he9 : outputSubBytes <= 8'h1e;
		8'hea : outputSubBytes <= 8'h87;
		8'heb : outputSubBytes <= 8'he9;
		8'hec : outputSubBytes <= 8'hce;
		8'hed : outputSubBytes <= 8'h55;
		8'hee : outputSubBytes <= 8'h28;
		8'hef : outputSubBytes <= 8'hdf;
		
		8'hf0 : outputSubBytes <= 8'h8c;
		8'hf1 : outputSubBytes <= 8'ha1;
		8'hf2 : outputSubBytes <= 8'h89;
		8'hf3 : outputSubBytes <= 8'h0d;
		8'hf4 : outputSubBytes <= 8'hbf;
		8'hf5 : outputSubBytes <= 8'he6;
		8'hf6 : outputSubBytes <= 8'h42;
		8'hf7 : outputSubBytes <= 8'h68;
		8'hf8 : outputSubBytes <= 8'h41;
		8'hf9 : outputSubBytes <= 8'h99;
		8'hfa : outputSubBytes <= 8'h2d;
		8'hfb : outputSubBytes <= 8'h0f;
		8'hfc : outputSubBytes <= 8'hb0;
		8'hfd : outputSubBytes <= 8'h54;
		8'hfe : outputSubBytes <= 8'hbb;
		8'hff : outputSubBytes <= 8'h16;
	endcase
end
endmodule
