module controller_fsm(
clk, 
reset,
writedone, 
mux0sel,
muxdemux1demux0sel,
en_ark,
en_msb,
en_sr,
en_mc,
en_shiftregout,
sel_shiftregout,
keysc_sel,
keysc_en,
man_reset
);

input clk, reset, writedone;
output reg 
mux0sel = 0, 
muxdemux1demux0sel = 0,  
en_ark = 0, 
en_msb = 0, 
en_sr = 0, 
en_mc = 0, 
en_shiftregout = 0,
man_reset = 0,
keysc_en = 0;
output reg [3:0] keysc_sel = 0, sel_shiftregout = 0;
reg [31:0] i;
reg [4:0] j;

(* syn_encoding = "safe" *) reg [3:0] state;
parameter 
IDLE = 0,
SRI = 1,
ARK = 2,
MSB = 3,
SR = 4,
MC = 5,
SRO = 6;

always @(state)
begin
	case(state)
		IDLE : begin
				en_ark <= 0;
				keysc_en <= 0;
				en_msb <= 0;
				en_sr <= 0;
				en_mc <= 0;
				en_shiftregout <= 0;
				sel_shiftregout <= 0;
				i <= 0;
		end
		SRI : begin
				en_ark <= 0;
				keysc_en <= 0;
				en_msb <= 0;
				en_sr <= 0;
				en_mc <= 0;
				en_shiftregout <= 0;
				sel_shiftregout <= 0;
				i <= 0;
		end
		ARK : begin
				en_ark <= 1;
				keysc_en <= 0;
				en_msb <= 0;
				en_sr <= 0;
				en_mc <= 0;
				en_shiftregout <= 0;
				sel_shiftregout <= 0;
				i <= 0;
		end
		MSB : begin
				en_ark <= 0;
				keysc_en <= 0;
				en_msb <= 1;
				en_sr <= 0;
				en_mc <= 0;
				en_shiftregout <= 0;
				sel_shiftregout <= 0;
				i <= 0;
		end
		SR : begin
				en_ark <= 0;
				keysc_en <= 0;
				en_msb <= 0;
				en_sr <= 1;
				en_mc <= 0;
				en_shiftregout <= 0;
				sel_shiftregout <= 0;
				i <= 0;
		end
		MC : begin
				en_ark <= 0;
				keysc_en <= 0;
				en_msb <= 0;
				en_sr <= 0;
				en_mc <= 1;
				en_shiftregout <= 0;
				sel_shiftregout <= 0;
				i <= 0;
				keysc_sel <= keysc_sel+1;
		end
		SRO : begin
				en_ark <= 0;
				keysc_en <= 0;
				en_msb <= 0;
				en_sr <= 0;
				en_mc <= 0;
				en_shiftregout <= 1;
				for (j=0;j<15;j=j+1) sel_shiftregout <= j;
				i <= 0;
		end
	endcase
end

always @(clk)
begin
	if(reset) state <= IDLE;
	else if(writedone) i <= i+1;
end

endmodule
