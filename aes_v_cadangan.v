module aes_v_cadangan(in, key, 
out_reg, outmc_reg, outmsb_reg, outmux0_reg, outsr_reg, outark_reg, 
outkey, mux0sel, demux0sel, mux1sel, demux1sel, keys_en,
iter, count, enable, done, reset, clk);

input [127:0] in, key;
output reg [127:0] out_reg;
input clk, reset;
output reg [127:0] 
outmux0_reg,
outark_reg,
outmsb_reg,
outsr_reg,
outmc_reg
;
output reg [3:0] count = 0;
output reg [5:0] iter;
output reg mux0sel, demux0sel, mux1sel, demux1sel, enable, keys_en, done;
wire [127:0] 
outmux1,
outmux0,
outark,
outdemux0,
outmsb,
outsr,
outmc,
outdemux1_0,
outdemux1_1,
outkey,
out
;

output outkey;

keyschedule ks(key, outkey, count, keys_en, clk);
multiplexer mux0(in, outmux1, outmux0, mux0sel, clk);
addroundkey ark(outmux0_reg, outkey, outark, enable, clk); 
demultiplexer demux0(outark_reg, outdemux0, out, demux0sel, clk);
multisubbytes msb(outdemux0, outmsb, enable, clk);
shiftrows sr(outmsb_reg, outsr, enable, clk);
demultiplexer demux1(outsr_reg, outdemux1_0, outdemux1_1, demux1sel, clk);
mixcolumn mc(outdemux1_0, outmc, enable, clk);
multiplexer mux1(outmc_reg, outdemux1_1, outmux1, mux1sel, clk);

always @(posedge clk)
begin
	outmux0_reg <= outmux0;
	outark_reg <= outark;
	outmsb_reg <= outmsb;
	outmc_reg <= outmc;
	if(reset) begin
		enable <= 0;
		iter <= 0;
		count <= 0;
	end
	else begin
		enable <= 1;
		if(count>10) count <= 0;
		else begin
			if(iter>7) begin
				iter <= 0;
				count <= count+1;
			end
			else begin
				iter <= iter+1;
			end
		end
	end
end

always @(count)
begin
	if(count==0) begin
		mux0sel <= 0;
		demux0sel <= 0;
		mux1sel <= 0;
		demux1sel <= 0;
		done <= 0;
	end
	if(count>0) begin 
		mux0sel <= 1;
	end
	if(count==10) begin
		mux1sel <= 1;
		demux1sel <= 1;
		if(iter>6) begin 
			demux0sel <= 1;
			done <= 1;
			out_reg <= out;
		end
	end
end

always @(iter)
begin
	if(iter == 4) begin
		keys_en <= 1;
	end
	else begin 
		keys_en <= 0;
	end
	if(iter == 3) outsr_reg <= outsr;
end

endmodule
