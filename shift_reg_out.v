module shift_reg_out (
in,
out,
sel,
enable,
clk
);

input [127:0] in;
output reg [7:0] out;
input clk, enable;
input [3:0] sel;

always @(posedge clk)
begin
	if(enable)
	case(sel)
		0 : out <= in[7:0];
		1 : out <= in[15:0];
		2 : out <= in[23:16];
		3 : out <= in[31:24];
		4 : out <= in[39:32];
		5 : out <= in[47:40];
		6 : out <= in[55:48];
		7 : out <= in[63:56];
		8 : out <= in[71:64];
		9 : out <= in[79:72];
		10 : out <= in[87:80];
		11 : out <= in[95:88];
		12 : out <= in[103:96];
		13 : out <= in[111:104];
		14 : out <= in[119:112];
		15 : out <= in[127:120];
	endcase
end
endmodule
