module shift_register_out (in, out, read, clk);

input [127:0] in;
reg [127:0] regin;
output reg [31:0] out;
input clk, read;

always @(posedge clk)
begin
	if(read) regin = in;
	out = regin[31:0];
	regin = regin>>32;
end

endmodule
